class Item {
	constructor(node, list) {
		if (!(node instanceof Node)) {
			throw `item must be a Node, got: ${node}`;
		}
		if (!(list instanceof List)) {
			throw `list must be a List, got: ${list}`;
		}
		this.node = node;
		this.list = list;
	}

	hide() {
		this.node.hidden = true;
	}

	show() {
		this.node.hidden = false;
	}
}

class List {
	constructor(container, list) {
		this.container = container;
		this.list = [];
		this.filters = {};
		// TODO: figure out how to do better error handling here
		if (list) {
			for (let node of list) {
				if (!(node instanceof Node)) {
					throw `node must be a Node, got: ${node}`;
				}
				this.list.push(new Item(node, this));
			}
		}
	}

	get length() {
		return this.list.length;
	}
	
	get(idx) {
		return this.list[idx];
	}

	slice(start, end) {
		return this.list(start, end);
	}
	
	push(node) {
		if (!(node instanceof Node)) {
			throw `node must be a Node, got: ${node}`;
		}
		return this.list.push(new Item(node, this));
	}

	appendChild(node) {
		this.push(node);
		this.container.appendChild(node);
	}

	addEventListener(type, listener, options) {
		this.container.addEventListener(type, listener, options);
	}
	
	show(...items) {
		for (let item of items) {
			item.show();
		}
	}
	
	hide(...items) {
		for (let item of items) {
			item.hide();
		}
	}
	
	key_sort(key_fn, reverse = false, nulls_last = true) {
		this.cmp_sort((item1, item2) => {
			let value1 = key_fn(item1);
			let value2 = key_fn(item2);
			if (value1 == value2) {
				return 0;
			}
			if (nulls_last) {
				if (null == value1) {
					return 1;
				}
				if (null == value2) {
					return -1;
				}
			}
			if (reverse) {
			  [value1, value2] = [value2, value1];  
			}
			if (value1 < value2) {
				return -1;
			} else {
				return 1;
			}
		});
	}

	cmp_sort(cmp_fn) {
		this.list.sort(cmp_fn);
		this.container.append(...(this.list.map((item) => { return item.node; })));
	}
	
	add_filter(filter_key, filter_fn, filter_op, filter_group = 'default') {
		if (!this.filters[filter_group]) {
			this.filters[filter_group] = {};
		}
		this.filters[filter_group][filter_key] = filter_fn;
		this.filter(filter_op);
	}

	remove_filter(filter_key, filter_op, filter_group = 'default') {
		if (this.filters[filter_group] && this.filters[filter_group][filter_key]) {
			delete this.filters[filter_group][filter_key];
		}
		this.filter(filter_op);
	}

	filter(filter_op) {
		const filter_groups = Object.getOwnPropertyNames(this.filters);
		for (let item of this.list) {
			let show = null;
			for (let filter_group of filter_groups) {
				let show_group = null;
				for (let filter_name of Object.getOwnPropertyNames(this.filters[filter_group])) {
					show_group = filter_op(show_group, this.filters[filter_group][filter_name](item.node));
				}
				if (show == null) {
					show = show_group;
				} else {
					show = show && show_group;
				}
			}
			if (show || show == null) {
				item.show();
			} else {
				item.hide();
			}
		}
	}

	[Symbol.iterator]() {
		let i = 0;

		return {
			next: () => {
				if (i < this.list.length) {
					return {value: this.list[i++]};
				}
				return {done: true};
			}
		}
	}
}