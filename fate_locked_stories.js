(() => {
	const FILTER_SECTIONS = [
		[
			'Main Factions',
			new Set([
				'Bohemians',
				'Constables',
				'Criminals',
				'Hell',
				'Revolutionaries',
				'Rubberies',
				'Society',
				'The Church',
				'The Docks',
				'The Great Game',
				'The Tomb-Colonies',
				'Urchins',
			])
		],
		[
			'Other Factions',
			new Set([
				'Benthic',
				'Calendar Council',
				'Cats',
				'Clay Men',
				'Drownies',
				'Fingerkings',
				'Masters',
				'New Sequence',
				'Rats',
				'Royal Family',
				'Snuffers',
				'Starved Men',
				'The Admiralty',
				'The Glass',
				'The Khanate',
				'The Palace',
				'Tigers',
				'Zailors',
			])
		],
		[
			'Individuals',
			new Set([
				'Bishop of Southwark',
				"Bishop of St Fiacre's",
				'Mr Mirrors',
				'Mr Spices',
				'Mr Stones',
				'Mr Wines',
				'Ophidian Gentleman',
				'Storm',
				'The Bazaar',
				'The Bloody-Handed Queen',
				'The Boatman',
				'The Duchess',
				'The Gracious Widow',
			])
		],
		[
			'Fallen Cities',
			new Set([
				'Fifth City',
				'First City',
				'Fourth City',
				'Second City',
				'Sixth City',
				'Third City',
			])
		],
		[
			'Topics',
			new Set([
				'Animescence',
				'Discordance',
				'Elder Continent',
				'Mushrooms',
				'Neathbow',
				'Palaeontology',
				'Parabola',
				'Polythreme',
				'Red Science',
				'Shapeling Arts',
				'Sorrow-Spiders',
				'The Correspondence',
				'The Surface',
				'The University',
				'Unterzee',
			])
		],
		[
			'Themes',
			new Set([
				'Arts',
				'Class Division',
				'Food',
				'Law',
				'Sports',
				'Theology',
			])
		],
		[
			'Lore',
			new Set([
				'Good Intro',
				'Deep Lore',
				'Sunless Sea',
			])
		],
		[
			'Story Type',
			new Set([
				'Exceptional Story',
				'Festive Tale',
			])
		],
	];

	const FAILBETTER_URL_PREFIX = 'https://community.failbettergames.com/t/';
	const REDDIT_URL_PREFIX = 'https://www.reddit.com/r/fallenlondon/comments/';
	const DEFAULT_AUTHOR = 'Failbetter Games Staff';
	let SURVEY_YEAR = 2024;

	const TAG_TYPES = {
		official: 'official',
		extra: 'extra',
		spoiler: 'spoiler',
	};

	const MONTHS = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December',
	];

	const PERMALINK = document.getElementById('permalink');
	const CARDS = new List(document.getElementById('cards'));

	const dashify = (str) => {
		return str.replace(/ /g, '-');
	};

	const formatReleaseDate = (datestring) => {
		const year = datestring.substr(0, 4);
		const month = MONTHS[datestring.substr(5, 2) - 1];
		const date = datestring.substr(8, 2);
		return `${date} ${month} ${year}`.trim();
	};

	const buildSeasonLink = (season) => {
		const season_link = document.createElement('a');
		season_link.classList.add('season-tag');
		season_link.tabIndex = 0;
		season_link.setAttribute('data-value', season);
		season_link.innerText = `Season of ${season}`;
		return season_link;
	};

	const buildAuthorLink = (author, author_list) => {
		author_list[author] = true;
		const author_link = document.createElement('a');
		author_link.tabIndex = 0;
		author_link.classList.add('author-tag');
		author_link.setAttribute('data-value', author);
		author_link.innerText = author;
		return author_link;
	};

	const buildTagLink = (tag, tag_list, type) => {
		const story_tag = document.createElement('a');
		story_tag.setAttribute('data-value', tag);
		story_tag.classList.add('tag');
		story_tag.tabIndex = 0;
		story_tag.innerHTML = ` ${tag.replace(/ /g, '&nbsp;')}`;
		tag_list[tag] = tag_list[tag] || {};

		if (type === TAG_TYPES.official) {
			tag_list[tag].official = true;
			story_tag.classList.add('official-tag');
		} else if (type === TAG_TYPES.extra) {
			tag_list[tag].extra = true;
			story_tag.classList.add('extra-tag');
		} else if (type === TAG_TYPES.spoiler) {
			tag_list[tag].spoiler = true;
			story_tag.classList.add('extra-tag', 'spoiler-tag');
		} else {
			throw `Unknown tag type: ${type}`;
		}

		return story_tag;
	};

	const calculateStoryScore = (score_data) => {
		const total = score_data.exceptional + .5 * score_data.delicious -.5 * score_data.okay - score_data.dislike;
		const votes = score_data.exceptional + score_data.delicious + score_data.liked + score_data.okay + score_data.dislike;
		score_data.score = total/votes;
		return score_data.score;
	};

	const calculateRewardScore = (score_data) => {
		let score;
		if (score_data.percent_only) {
			score = score_data.worth_it;
		} else {
			score = 100 * score_data.worth_it / (score_data.worth_it + score_data.not_worth_it);
		}
		score_data.score = score;
		return score;
	};

	const buildRankings = (story_data, year) => {
		const scores = document.createElement('div');
		scores.classList.add('story-scores');
		if (story_data.story_rankings && story_data.story_rankings[year]) {
			const score_data = story_data.story_rankings[year];
			const story_score = document.createElement('div');
			story_score.tabIndex = 0;
			story_score.classList.add('story-score');
			let raw_score = calculateStoryScore(score_data);
			if (raw_score > .5) {
				story_score.classList.add('exceptional-score');
			} else if (raw_score > 0) {
				story_score.classList.add('delicious-score');
			} else if (raw_score < -.5) {
				story_score.classList.add('dislike-score');
			} else if (raw_score < 0) {
				story_score.classList.add('okay-score');
			} else {
				// at the moment this is more of a fallback than an
				// expected option, but consider changing the ranges
				// to fix that
				story_score.classList.add('liked-score');
			}
			story_score.innerText = raw_score.toFixed(2);
			const tooltip_content = document.createDocumentFragment();
			let total = 0;
			for (let rating of ['exceptional', 'delicious', 'liked it', 'okay', "didn't like it", 'total']) {
				const rating_line = document.createElement('div');
				const rating_title = document.createElement('span');
				rating_title.innerText = rating;
				if (rating == 'liked it') {
					rating = 'liked';
				} else if (rating == "didn't like it") {
					rating = 'dislike';
				}
				rating_title.classList.add(`${rating}-title`);
				rating_title.style.textTransform = 'capitalize';
				let score;
				if (rating == 'total') {
					score = total;
				} else {
					score = Number.parseInt(score_data[rating]);
					total += score;
				}
				rating_line.append(rating_title, `: ${score}`);
				tooltip_content.appendChild(rating_line);
			}

			new Tooltip(tooltip_content, story_score);
			scores.appendChild(story_score);
		}
		if (story_data.reward_rankings && story_data.reward_rankings[year]) {
			const reward_data = story_data.reward_rankings[year]
			const reward_score = document.createElement('div');
			reward_score.classList.add('reward-score');
			reward_score.tabIndex = 0;
			let raw_score = calculateRewardScore(reward_data);
			if (raw_score > 200/3) {
				reward_score.classList.add('exceptional-score');
			} else if (raw_score > 50) {
				reward_score.classList.add('delicious-score');
			} else if (raw_score > 100/3) {
				reward_score.classList.add('okay-score');
			} else {
				reward_score.classList.add('dislike-score');
			}
			reward_score.innerText = `${raw_score.toFixed(2)}%`;
			const tooltip_content = document.createDocumentFragment();
			let total = 0;
			for (let rating of [['Worth It', 'worth_it'], ['Not Worth It', 'not_worth_it'], ['Total', 'total']]) {
				const rating_line = document.createElement('div');
				const rating_title = document.createElement('span');
				rating_title.innerText = rating[0];
				rating_title.classList.add(`${rating[1]}-title`);
				let score;
				if (rating[1] == 'total') {
					score = total;
				} else {
					if (reward_data.percent_only) {
						score = Number.parseFloat(reward_data[rating[1]]);
					} else {
						score = Number.parseInt(reward_data[rating[1]]);
					}
					total += score;
				}
				rating_line.append(rating_title, `: ${score}${reward_data.percent_only ? '%' : ''}`);
				tooltip_content.appendChild(rating_line);
			}
			new Tooltip(tooltip_content, reward_score);
			scores.appendChild(reward_score);
		}
		if (!scores.hasChildNodes()) {
			const no_score = document.createElement('div');
			no_score.tabIndex = 0;
			no_score.classList.add('no-score');
			no_score.innerText = 'Unranked';
			scores.appendChild(no_score);
		}
		return scores;
	}

	const buildFateCosts = (story_data) => {
		const story_costs = document.createElement('div');
		story_costs.classList.add('story-fate-costs');
		const story_unlock_cost = document.createElement('div');
		story_unlock_cost.classList.add('story-unlock-cost', 'story-cost');
		story_unlock_cost.tabIndex = 0;
		const unlock_label = document.createElement('div');
		unlock_label.classList.add('unlock-label', 'cost-label');
		unlock_label.innerText = 'Unlock Cost';
		story_unlock_cost.append(unlock_label, story_data.unlock_fate, ' Fate');
		story_costs.appendChild(story_unlock_cost);

		if (story_data.reset_fate != null) {
			const separator = document.createElement('div');
			separator.classList.add('story-cost-separator');
			story_costs.appendChild(separator);
			const story_reset_cost = document.createElement('div');
			story_reset_cost.classList.add('story-reset-cost', 'story-cost');
			story_reset_cost.tabIndex = 0;
			const reset_label = document.createElement('div');
			reset_label.classList.add('reset-label', 'cost-label');
			reset_label.innerText = 'Reset Cost';
			story_reset_cost.append(reset_label, story_data.reset_fate, ' Fate');
			story_costs.appendChild(story_reset_cost);
		}
		return story_costs;
	};

	const buildByline = (story_data, author_list) => {
		const byline = document.createElement('span');
		byline.classList.add('story-author');
		byline.innerText = 'by ';
		if (story_data.authors && story_data.authors.length) {
			const author_links = [];
			for (let author of story_data.authors) {
				author_links.push(buildAuthorLink(author, author_list));
			}
			if (author_links.length == 1) {
				byline.appendChild(author_links[0])
			} else if (author_links.length == 2) {
				byline.append(author_links[0], ' and ', author_links[1]);
			} else {
				for (let i = 0; i < author_links.length - 1; ++i) {
					byline.append(author_links[i], ', ');
				}
				byline.append('and ', author_links[author_links.length - 1]);
			}
		} else {
			let author = DEFAULT_AUTHOR;
			if (story_data.author) {
				author = story_data.author;
			}
			byline.appendChild(buildAuthorLink(author, author_list));
		}
		return byline;
	};

	const buildTags = (story_data, tag_list) => {
		const tags = document.createElement('span');
		tags.classList.add('story-tags');
		if (story_data.official_tags && story_data.official_tags.length) {
			const official_tags = document.createElement('span');
			official_tags.classList.add('tags', 'official-tags');
			for (let tag of story_data.official_tags) {
				official_tags.appendChild(buildTagLink(tag, tag_list, TAG_TYPES.official));
			}
			tags.appendChild(official_tags);
		}
		let extra_tags;
		if (story_data.extra_tags && story_data.extra_tags.length) {
			extra_tags = document.createElement('span');
			extra_tags.classList.add('tags', 'extra-tags');
			for (let tag of story_data.extra_tags) {
				extra_tags.appendChild(buildTagLink(tag, tag_list, TAG_TYPES.extra));
			}
		}
		if (story_data.spoiler_tags && story_data.spoiler_tags.length) {
			const spoiler_tags = document.createElement('span');
			spoiler_tags.classList.add('spoiler-tags');
			for (let tag of story_data.spoiler_tags) {
				spoiler_tags.appendChild(buildTagLink(tag, tag_list, TAG_TYPES.spoiler));
			}
			if (extra_tags) {
				extra_tags.appendChild(spoiler_tags);
			} else {
				spoiler_tags.classList.add('tags', 'extra-tags');
				extra_tags = spoiler_tags;
			}
		}
		if (extra_tags) {
			tags.appendChild(extra_tags);
		}
		return tags;
	};

	const buildImgLink = (href, src, alt) => {
		const link = document.createElement('a');
		link.setAttribute('rel', 'noreferrer');
		link.setAttribute('href', href);
		link.setAttribute('target', '_blank');
		const icon = document.createElement('img');
		icon.setAttribute('src', src);
		icon.setAttribute('alt', alt);
		icon.setAttribute('title', alt);
		link.appendChild(icon);
		return link;
	};

	const buildThreadLinks = (story_data) => {
		const threads = document.createElement('span');
		threads.classList.add('threads');
		const thread_header = document.createElement('span');
		thread_header.classList.add('thread-header');
		thread_header.innerText = 'Threads: '
		threads.appendChild(thread_header);
		if (story_data.threads.failbetter) {
			threads.appendChild(
				buildImgLink(
					`${FAILBETTER_URL_PREFIX}${story_data.threads.failbetter}`,
					'images/failbetter_sq_bn_icon.png',
					'Failbetter Thread'));
		}
		if (story_data.threads.reddit) {
			threads.appendChild(
				buildImgLink(
					`${REDDIT_URL_PREFIX}${story_data.threads.reddit}`,
					'images/reddit_icon.png',
					'Reddit Thread'));
		}
		return threads;
	};

	const buildStoryHeader = (story_data, author_list) => {
		const header = document.createElement('div');
		header.classList.add('story-header');
		const headline = document.createElement('div');
		headline.classList.add('story-headline');
		const story_title = document.createElement('span');
		story_title.classList.add('story-title');
		story_title.innerText = story_data.title;
		headline.appendChild(story_title);
		headline.appendChild(buildByline(story_data, author_list));
		header.appendChild(headline);
		if (story_data.release_date) {
			const story_release_date = document.createElement('p');
			story_release_date.classList.add('story-release-date');
			story_release_date.innerText = `released ${formatReleaseDate(story_data.release_date)}`;
			if (story_data.season) {
				const story_season = document.createElement('span');
				story_season.classList.add('story-season');
				story_season.append(' (in the ', buildSeasonLink(story_data.season), ')');
				story_release_date.appendChild(story_season);
			}
			header.appendChild(story_release_date);
		} else if (story_data.release_date_est) {
			const story_release_date = document.createElement('p');
			story_release_date.classList.add('story-release-date')
			story_release_date.innerText = `released ${formatReleaseDate(story_data.release_date_est)}`;
			const est_text = document.createElement('span');
			est_text.classList.add('estimated-text');
			est_text.innerHTML = '<sup>\u2020</sup> (estimated)'
			const est_note = document.createElement('div');
			est_note.classList.add('estimated-note');
			est_note.innerText = 'Release date estimated using online mentions.';
			new Tooltip(est_note, est_text)
			story_release_date.appendChild(est_text);
			header.appendChild(story_release_date);
		}
		return header;
	};

	const buildTagLine = (story_data, tag_list) => {
		const tag_line = document.createElement('div');
		tag_line.classList.add('tag-line');
		if (story_data.threads && Object.getOwnPropertyNames(story_data.threads).length) {
			tag_line.appendChild(buildThreadLinks(story_data));
		}
		const spacer = document.createElement('span');
		spacer.classList.add('spacer');
		tag_line.appendChild(spacer);
		tag_line.appendChild(buildTags(story_data, tag_list));
		return tag_line;
	};

	const buildStoryDetails = (story_data, tag_list, author_list) => {
		const story_details = document.createElement('div');
		story_details.classList.add('story-details');

		story_details.appendChild(buildStoryHeader(story_data, author_list));

		const story_desc = document.createElement('div');
		story_desc.classList.add('story-description');
		if (story_data.description) {
			story_desc.innerHTML = story_data.description;
		}
		story_details.appendChild(story_desc);
		story_details.appendChild(buildTagLine(story_data, tag_list));
		return story_details;
	};

	const buildStoryCard = (story_data, tag_list, author_list) => {
		const story_card = document.createElement('div');
		story_card.setAttribute('data-id', story_data.id);
		story_card.classList.add('story-card');
		story_card.appendChild(buildRankings(story_data, SURVEY_YEAR));
		story_card.appendChild(buildStoryDetails(story_data, tag_list, author_list));
		story_card.appendChild(buildFateCosts(story_data));
		return story_card;
	};

	const createCards = (tag_list, author_list) => {
		for (let story_data of STORIES) {
			const story_card = buildStoryCard(story_data, tag_list, author_list);
			CARDS.appendChild(story_card);
		}
		CARDS.addEventListener('click', (e) => {
			if (e.target.matches('a.author-tag,a.season-tag,a.tag')) {
				checkLinkedCheckbox(e);
			}
		});
	};

	const checkLinkedCheckbox = (e) => {
		const tag = e.target.getAttribute('data-value');
		const checkbox_id = `filter-${dashify(tag)}`;
		const checkbox = document.getElementById(checkbox_id);
		if (!checkbox.checked) {
			checkbox.click();
		}
	};

	const syncInputs = (e) => {
		const target = e.target;
		const input_type = target.type.toLowerCase();
		let sync_target;
		if (target.id.endsWith('-collapsible')) {
			const other_id = target.id.replace(/-collapsible$/, '');
			sync_target = document.getElementById(other_id);
		} else {
			sync_target = document.getElementById(target.id + '-collapsible');
		}
		if (sync_target == null) {
			return;
		}
		if (input_type == 'radio' || input_type == 'checkbox') {
			if (target.checked != sync_target.checked) {
				sync_target.click();
			}
		} else if (input_type.startsWith('select')) {
			if (sync_target.selectedIndex != target.selectedIndex) {
				sync_target.selectedIndex = target.selectedIndex;
				sync_target.dispatchEvent(new Event('change'));
			}
		}
	};

	const clearFilters = () => {
		const checkbox_selector = '.author-filter input:checked,.season-filter input:checked,.tag-filter input:checked';
		const checked_boxes = document.querySelectorAll(checkbox_selector);
		for (let box of checked_boxes) {
			if (box.checked) {
				box.click();
			}
		}
	};

	const getFilterOp = () => {
		if (document.getElementById('any-filter').checked) {
			return (value1, value2) => {
				if (value1 == null) { return !!value2; }
				return value1 || value2;
			};
		} else if (document.getElementById('all-filter').checked) {
			return (value1, value2) => {
				if (value1 == null) { return !!value2; }
				return value1 && value2;
			};
		} else if (document.getElementById('none-filter').checked) {
			return (value1, value2) => {
				if (value1 == null) { return !value2; }
				return value1 && !value2;
			};
		} else {
			throw 'Somehow neither radio button is checked';
		}
	};

	const filterCards = () => {
		CARDS.filter(getFilterOp());
	};

	const filterSeason = (e) => {
		const season = e.target.value;
		const filter_name = `season ${season}`;
		const filter_fn = (node) => {
			const id = Number.parseInt(node.getAttribute('data-id'));
			return SEASONS[season].stories.includes(id);
		};
		if (e.target.checked) {
			CARDS.add_filter(filter_name, filter_fn, getFilterOp(), 'season');
		} else {
			CARDS.remove_filter(filter_name, getFilterOp(), 'season');
		}
	};

	const filterAuthor = (e) => {
		const author = e.target.value;
		const filter_name = `author ${author}`;
		const filter_fn = (node) => {
			const id = Number.parseInt(node.getAttribute('data-id'));
			const story_data = STORIES[id];
			if (story_data.authors && story_data.authors.length) {
				return story_data.authors.includes(author);
			} else if (story_data.author) {
				return story_data.author == author;
			} else {
				return author == DEFAULT_AUTHOR;
			}
		};
		if (e.target.checked) {
			CARDS.add_filter(filter_name, filter_fn, getFilterOp(), 'author');
		} else {
			CARDS.remove_filter(filter_name, getFilterOp(), 'author');
		}
	};

	const filterTag = (e) => {
		const tag = e.target.value;
		const filter_name = `tag ${tag}`;
		const filter_fn = (node) => {
			const id = Number.parseInt(node.getAttribute('data-id'));
			const story_data = STORIES[id];
			const show_extra = document.getElementById('show-extra').checked;
			const show_spoilers = show_extra && document.getElementById('show-spoilers').checked;
			const official_tags = story_data.official_tags || [];
			const extra_tags = story_data.extra_tags || [];
			const spoiler_tags = story_data.spoiler_tags || [];
			return official_tags.includes(tag) ||
				   show_extra && extra_tags.includes(tag) ||
				   show_spoilers && spoiler_tags.includes(tag);
		};
		if (e.target.checked) {
			CARDS.add_filter(filter_name, filter_fn, getFilterOp());
		} else {
			CARDS.remove_filter(filter_name, getFilterOp());
		}
	};

	const setupSidebarTagFilters = (tag_list) => {
		const filters_lists = {
			'Main Factions': document.getElementById('main-factions-tag-filters'),
			'Other Factions': document.getElementById('other-factions-tag-filters'),
			'Individuals': document.getElementById('individuals-tag-filters'),
			'Topics': document.getElementById('topics-tag-filters'),
			'Fallen Cities': document.getElementById('fallen-cities-tag-filters'),
			'Themes': document.getElementById('themes-tag-filters'),
			'Lore': document.getElementById('lore-tag-filters'),
			'Story Type': document.getElementById('story-type-tag-filters'),
			'Other': document.getElementById('other-tag-filters'),
		};
		for (let tag of Object.getOwnPropertyNames(tag_list).sort()) {
			const new_filter = document.createElement('li');
			new_filter.classList.add('tag-filter');
			const filter_checkbox = document.createElement('input');
			filter_checkbox.id = `filter-${dashify(tag)}`;
			filter_checkbox.type = 'checkbox';
			const filter_label = document.createElement('label');
			filter_label.classList.add('filter-label');
			filter_label.setAttribute('for', filter_checkbox.id);
			filter_label.innerText = tag;
			filter_checkbox.value = tag;
			filter_checkbox.addEventListener('change', filterTag);
			if (tag_list[tag].official) {
				new_filter.classList.add('official-filter');
			} else if (tag_list[tag].extra) {
				new_filter.classList.add('extra-filter');
			} else if (tag_list[tag].spoiler) {
				new_filter.classList.add('spoiler-filter');
			}
			new_filter.appendChild(filter_checkbox);
			new_filter.appendChild(filter_label);
			let found = false;
			for (let section of FILTER_SECTIONS) {
				if (section[1].has(tag)) {
					filters_lists[section[0]].appendChild(new_filter);
					found = true;
					break;
				}
			}
			if (!found) {
				filters_lists['Other'].appendChild(new_filter);
				document.getElementById('other-tag-container').hidden = false;
				document.getElementById('other-tag-container-collapsible').hidden = false;
			}
		}
	};

	const setupAuthorFilters = (author_list) => {
		const author_filter_list = document.getElementById('author-filters');
		for (let author of Object.getOwnPropertyNames(author_list).sort()) {
			const new_filter = document.createElement('li');
			new_filter.classList.add('author-filter');
			const filter_checkbox = document.createElement('input');
			filter_checkbox.id = `filter-${dashify(author)}`;
			filter_checkbox.type = 'checkbox';
			const filter_label = document.createElement('label');
			filter_label.classList.add('filter-label');
			filter_label.setAttribute('for', filter_checkbox.id);
			filter_label.innerText = author;
			filter_checkbox.value = author;
			filter_checkbox.addEventListener('change', filterAuthor);
			new_filter.appendChild(filter_checkbox);
			new_filter.appendChild(filter_label);
			author_filter_list.appendChild(new_filter);
		}
	};

	const setupSeasonFilters = () => {
		const season_filters = document.getElementById('season-filters');
		for (let season of Object.getOwnPropertyNames(SEASONS)) {
			const season_el = document.createElement('li');
			season_el.classList.add('season-filter');
			const filter_checkbox = document.createElement('input');
			filter_checkbox.id = `filter-${dashify(season)}`;
			filter_checkbox.type = 'checkbox';
			const filter_label = document.createElement('label');
			filter_label.classList.add('filter-label');
			filter_label.setAttribute('for', filter_checkbox.id);
			filter_label.innerText = season;
			filter_checkbox.value = season;
			filter_checkbox.addEventListener('change', filterSeason);
			season_el.appendChild(filter_checkbox);
			season_el.appendChild(filter_label);
			season_filters.appendChild(season_el);
		};
	};

	const setupLeftSidebar = (tag_list) => {
		const show_spoilers = document.getElementById('show-spoilers');
		show_spoilers.addEventListener('change', () => {
			const style = document.getElementById('style-hide-spoilers');
			if (show_spoilers.checked) {
				style.innerText = '';
			} else {
				style.innerText = '.spoiler-tags,.spoiler-filter { display: none; }';
			}
			filterCards();
		});
		const show_extra = document.getElementById('show-extra');
		show_extra.addEventListener('change', () => {
			const style = document.getElementById('style-show-extra');
			if (show_extra.checked) {
				style.innerText = '';
				show_spoilers.disabled = false;
			} else {
				style.innerText = '.extra-tags,.extra-filter,.spoiler-filter { display: none; }';
				show_spoilers.disabled = true;
			}
			filterCards();
		});
		document.getElementById('all-filter').addEventListener('change', filterCards);
		document.getElementById('any-filter').addEventListener('change', filterCards);
		document.getElementById('none-filter').addEventListener('change', filterCards);
		document.getElementById('clear-filters').addEventListener('click', clearFilters);
		setupSidebarTagFilters(tag_list);
	};

	const setRankingsData = () => {
		const year = document.getElementById('rankings-select').value;
		if (year == SURVEY_YEAR) {
			return;
		}
		for (let card of CARDS) {
			let id = Number.parseInt(card.node.getAttribute('data-id'));
			card.node.querySelector('div.story-scores').replaceWith(buildRankings(STORIES[id], year));
		}
		SURVEY_YEAR = year;
		switch (document.getElementById('sort-select').value) {
			case 'story-score':
			case 'reward-score':
				setSort();
				break;
			default:
		}
	}

	const setSort = () => {
		let reverse, key;
		if (document.getElementById('sort-desc').checked) {
			reverse = true;
		} else if (document.getElementById('sort-asc').checked) {
			reverse = false;
		} else {
			throw 'Somehow neither radio button is checked.';
		}
		switch (document.getElementById('sort-select').value) {
			case 'release':
				key = releaseDateKey;
				break;
			case 'abc':
				key = getStringKey('title');
				break;
			case 'cost':
				key = getStringKey('unlock_fate');
				break;
			case 'story-score':
				key = storyScoreKey;
				break;
			case 'reward-score':
				key = rewardScoreKey;
				break;
			default:
				throw 'Somehow no option is selected.';
		}
		CARDS.key_sort(key, reverse);
	};

	const getStringKey = (str) => {
		return (item) => {
			return STORIES[item.node.getAttribute('data-id')][str];
		};
	};

	const storyScoreKey = (item) => {
		const score_data = STORIES[item.node.getAttribute('data-id')].story_rankings;
		if (score_data && score_data[SURVEY_YEAR]) {
			return -score_data[SURVEY_YEAR].score;
		} else {
			return null;
		}
	};

	const rewardScoreKey = (item) => {
		const score_data = STORIES[item.node.getAttribute('data-id')].reward_rankings;
		if (score_data && score_data[SURVEY_YEAR]) {
			return -score_data[SURVEY_YEAR].score;
		} else {
			return null;
		}
	};

	const releaseDateKey = (item) => {
		return Number.parseInt(item.node.getAttribute('data-id'));
	};

	const setupRightSidebar = (author_list) => {
		const sort_asc = document.getElementById('sort-asc');
		const sort_desc = document.getElementById('sort-desc');
		const sort_select = document.getElementById('sort-select');
		const rankings_select = document.getElementById('rankings-select');
		sort_asc.addEventListener('change', setSort);
		sort_desc.addEventListener('change', setSort);
		sort_select.addEventListener('change', setSort);
		rankings_select.addEventListener('change', setRankingsData);
		setupSeasonFilters();
		setupAuthorFilters(author_list);
	};

	const setupCollapsibleSidebars = () => {
		const suffix = '-collapsible';
		const show_extra = document.getElementById(`show-extra${suffix}`);
		const show_spoilers = document.getElementById(`show-spoilers${suffix}`);
		show_extra.addEventListener('change', () => {
			if (show_extra.checked) {
				show_spoilers.disabled = false;
			} else {
				show_spoilers.disabled = true;
			}
		});
		document.getElementById(`clear-filters${suffix}`).addEventListener('click', clearFilters);
		for (let main_ul of document.querySelectorAll('ul.sidebar-list[id$="-filters"]')) {
			const collapsible_ul = document.getElementById(main_ul.id + suffix);
			for (let main_li of main_ul.childNodes) {
				const main_cb = main_li.childNodes[0];
				const main_label = main_li.childNodes[1];
				const new_cb = document.createElement('input');
				new_cb.type = 'checkbox';
				new_cb.value = main_cb.value;
				new_cb.id = main_cb.id + suffix;
				const new_label = document.createElement('label');
				new_label.classList.add('filter-label')
				new_label.setAttribute('for', new_cb.id);
				new_label.innerText = main_label.innerText;
				const new_li = document.createElement('li');
				new_li.className = main_li.className;
				new_li.append(new_cb, new_label);
				collapsible_ul.appendChild(new_li);
			}
		}
		const left_collapsible_sidebar = document.getElementById('left-sidebar-collapsible');
		const right_collapsible_sidebar = document.getElementById('right-sidebar');
		const sidebar_overlay = document.getElementById('sidebar-overlay');
		document.getElementById('left-sidebar-toggle-button').addEventListener('click', () => {
			left_collapsible_sidebar.classList.add('sidebar-open');
			right_collapsible_sidebar.classList.add('sidebar-open');
			sidebar_overlay.classList.add('overlay-open');
			left_collapsible_sidebar.focus()
		});
		document.getElementById('right-sidebar-toggle-button').addEventListener('click', () => {
			left_collapsible_sidebar.classList.add('sidebar-open');
			right_collapsible_sidebar.classList.add('sidebar-open');
			sidebar_overlay.classList.add('overlay-open');
			right_collapsible_sidebar.focus();
		});
		sidebar_overlay.addEventListener('click', () => {
			left_collapsible_sidebar.classList.remove('sidebar-open');
			right_collapsible_sidebar.classList.remove('sidebar-open');
			sidebar_overlay.classList.remove('overlay-open');
		});
		document.body.addEventListener('keydown', (e) => {
			if (e.keyCode == 27) {
				left_collapsible_sidebar.classList.remove('sidebar-open');
				right_collapsible_sidebar.classList.remove('sidebar-open');
				sidebar_overlay.classList.remove('overlay-open');
			}
		});
	};

	const getParamsFromUrl = () => {
		const params = new URLSearchParams(window.location.search);
		for (let param of params.entries()) {
			const name = param[0].replace(/ /g, '-');
			const input = document.querySelector(`#filter-${name},#${name}-select,#root input[type="radio"][value="${name}"],input#${name}[value="${name}"]`);
			if (input.tagName.toLowerCase() == 'input') {
				input.checked = param[1] == 'true';
			} else if (input.options) {
				const num = parseInt(param[1]);
				if (0 <= num && num < input.options.length) {
					input.selectedIndex = num;
				}
			}
			input.dispatchEvent(new Event('change'));
		}
	};

	const updatePermalink = (e) => {
		const input = e.target;
		const tagName = input.tagName.toLowerCase();
		if (tagName != 'input' && tagName != 'select') {
			return;
		}
		const url = new URL(PERMALINK.href);
		const params = url.searchParams;
		if (tagName == 'select') {
			if (input.selectedIndex > 0) {
				params.set(input.name, input.selectedIndex);
			} else {
				params.delete(input.name);
			}
		} else if (input.type == 'radio' && input.checked) {
			for (let radio of document.querySelectorAll(`[name="${input.name}"]`)) {
				params.delete(radio.value);
			}
			if (!input.defaultChecked) {
				params.set(input.value, 'true');
			}
		} else if (input.checked != input.defaultChecked) {
			params.set(input.value, input.checked);
		} else {
			params.delete(input.value);
		}
		PERMALINK.href = url.toString();
	};

	const setupPage = () => {
		PERMALINK.href = window.location;
		const tag_list = {};
		const author_list = {};
		createCards(tag_list, author_list);
		setupLeftSidebar(tag_list);
		setupRightSidebar(author_list);
		setupCollapsibleSidebars();
		for (let input of document.querySelectorAll('#root input,#root select')) {
			input.addEventListener('change', syncInputs);
			input.addEventListener('change', updatePermalink);
		}
		for (let input of document.querySelectorAll('#left-sidebar-collapsible input,#left-sidebar-collapsible select')) {
			input.addEventListener('change', syncInputs);
		}
		getParamsFromUrl();
		PERMALINK.addEventListener('click', (e) => {
			window.navigator.clipboard.writeText(PERMALINK.href).then(() => {
				const tt = new Tooltip("Link copied to clipboard");
				tt.build();
				setTimeout(() => {
					tt.hide();
				}, 1000);
			});
			e.preventDefault();
		});
	};

	setupPage();
})();
