const STORIES = [
	{
		id: 0,
		title: "The Rubbery Murders",
		release_date: "2009/10",
		description: "Who has been poisoning deep amber traders? Is there really such a thing as a Rubbery Murderer? [This is a story with a dozen unique storylets suitable for Watchful at least 20.]",
		story_rankings: {
			2023: {
				exceptional: 0,
				delicious: 1,
				liked: 6,
				okay: 12,
				dislike: 19
			},
			2022: {
				exceptional: 0,
				delicious: 2,
				liked: 5,
				okay: 11,
				dislike: 29
			},
			2021: {
				exceptional: 0,
				delicious: 1,
				liked: 12,
				okay: 12,
				dislike: 21
			},
			2020: {
				exceptional: 1,
				delicious: 7,
				liked: 4,
				okay: 10,
				dislike: 14
			},
			2019: {
				exceptional: 0,
				delicious: 0,
				liked: 2,
				okay: 10,
				dislike: 10
			},
			2018: {
				exceptional: 1,
				delicious: 1,
				liked: 3,
				okay: 7,
				dislike: 9
			}
		},
		official_tags: ["Rubberies"],
		extra_tags: ["Permanent Reward"],
		lore_tags: [],
		unlock_fate: 25
	}, {
		id: 1,
		title: "A Long-Lost Daughter",
		release_date: "2009/10",
		description: "Your own daughter? Is it possible? [This is a story of a dozen unique storylets suitable for players with main Qualities around 30 and up.]",
		story_rankings: {
			2024: {
				exceptional: 0,
				delicious: 8,
				liked: 13,
				okay: 15,
				dislike: 13
			},
			2023: {
				exceptional: 2,
				delicious: 9,
				liked: 22,
				okay: 20,
				dislike: 12
			},
			2022: {
				exceptional: 4,
				delicious: 16,
				liked: 25,
				okay: 33,
				dislike: 20
			},
			2021: {
				exceptional: 5,
				delicious: 14,
				liked: 26,
				okay: 31,
				dislike: 20
			},
			2020: {
				exceptional: 3,
				delicious: 13,
				liked: 25,
				okay: 24,
				dislike: 3
			},
			2019: {
				exceptional: 0,
				delicious: 4,
				liked: 18,
				okay: 15,
				dislike: 2
			},
			2018: {
				exceptional: 3,
				delicious: 8,
				liked: 10,
				okay: 12,
				dislike: 2
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 18,
				not_worth_it: 35
			},
			2023: {
				worth_it: 26,
				not_worth_it: 45
			},
			2022: {
				worth_it: 36,
				not_worth_it: 61
			},
			2021: {
				worth_it: 27,
				not_worth_it: 70
			},
			2020: {
				worth_it: 14,
				not_worth_it: 60
			},
			2019: {
				worth_it: 13,
				not_worth_it: 30
			},
			2018: {
				percent_only: true,
				worth_it: 28,
				not_worth_it: 72
			}
		},
		official_tags: ["Criminals"],
		extra_tags: ["Permanent Reward"],
		lore_tags: [],
		unlock_fate: 25,
		reset_fate: 25
	}, {
		id: 2,
		title: "A Trade in Souls",
		release_date: "2010/02",
		description: "Delve into the wicked toils of the soul trade! [Those with Shadowy 20 or more can unlock a dozen unique storylets in this story.]",
		author: "Alexis Kennedy",
		story_rankings: {
			2024: {
				exceptional: 4,
				delicious: 18,
				liked: 50,
				okay: 46,
				dislike: 9
			},
			2023: {
				exceptional: 18,
				delicious: 24,
				liked: 65,
				okay: 40,
				dislike: 8
			},
			2022: {
				exceptional: 17,
				delicious: 43,
				liked: 86,
				okay: 65,
				dislike: 11
			},
			2021: {
				exceptional: 20,
				delicious: 36,
				liked: 100,
				okay: 53,
				dislike: 7
			},
			2020: {
				exceptional: 13,
				delicious: 39,
				liked: 72,
				okay: 26,
				dislike: 8
			},
			2019: {
				exceptional: 5,
				delicious: 24,
				liked: 39,
				okay: 19,
				dislike: 2
			},
			2018: {
				exceptional: 10,
				delicious: 22,
				liked: 29,
				okay: 18,
				dislike: 3
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 94,
				not_worth_it: 15
			},
			2023: {
				worth_it: 139,
				not_worth_it: 19
			},
			2022: {
				worth_it: 199,
				not_worth_it: 31
			},
			2021: {
				worth_it: 172,
				not_worth_it: 52
			},
			2020: {
				worth_it: 120,
				not_worth_it: 35
			},
			2019: {
				worth_it: 76,
				not_worth_it: 18
			},
			2018: {
				percent_only: true,
				worth_it: 80.20,
				not_worth_it: 19.80
			}
		},
		official_tags: ["Hell"],
		extra_tags: ["The Church", "Permanent Reward"],
		lore_tags: [],
		unlock_fate: 25,
		reset_fate: 25
	}, {
		id: 3,
		title: "Secrets Framed in Gold",
		release_date: "2010/11",
		description: "A storyline of notable menace.",
		author: "Emily Short",
		story_rankings: {
			2024: {
				exceptional: 22,
				delicious: 13,
				liked: 23,
				okay: 29,
				dislike: 12
			},
			2023: {
				exceptional: 18,
				delicious: 30,
				liked: 28,
				okay: 22,
				dislike: 16
			},
			2022: {
				exceptional: 34,
				delicious: 33,
				liked: 54,
				okay: 39,
				dislike: 19
			},
			2021: {
				exceptional: 26,
				delicious: 40,
				liked: 49,
				okay: 36,
				dislike: 15
			},
			2020: {
				exceptional: 39,
				delicious: 37,
				liked: 27,
				okay: 20,
				dislike: 9
			},
			2019: {
				exceptional: 19,
				delicious: 21,
				liked: 22,
				okay: 12,
				dislike: 4
			},
			2018: {
				exceptional: 21,
				delicious: 20,
				liked: 10,
				okay: 7,
				dislike: 4
			}
		},
		lore_tags: [],
		unlock_fate: 19,
		reset_fate: 25
	}, {
		id: 4,
		title: "Inconvenienced by Your Aunt",
		release_date_est: "2011/01",
		description: "Open up a set of new opportunities for your aunt.<br><em>This story cannot be fully reset. The listed reset cost is to change the final choice.</em>",
		story_rankings: {
			2024: {
				exceptional: 18,
				delicious: 24,
				liked: 54,
				okay: 41,
				dislike: 9
			},
			2023: {
				exceptional: 32,
				delicious: 34,
				liked: 54,
				okay: 38,
				dislike: 7
			},
			2022: {
				exceptional: 25,
				delicious: 71,
				liked: 76,
				okay: 57,
				dislike: 11
			},
			2021: {
				exceptional: 24,
				delicious: 46,
				liked: 83,
				okay: 43,
				dislike: 15
			},
			2020: {
				exceptional: 12,
				delicious: 35,
				liked: 41,
				okay: 35,
				dislike: 7
			},
			2019: {
				exceptional: 3,
				delicious: 15,
				liked: 36,
				okay: 25,
				dislike: 1
			},
			2018: {
				exceptional: 6,
				delicious: 20,
				liked: 26,
				okay: 17,
				dislike: 5
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 140,
				not_worth_it: 5
			},
			2023: {
				worth_it: 147,
				not_worth_it: 17
			},
			2022: {
				worth_it: 213,
				not_worth_it: 25
			},
			2021: {
				worth_it: 165,
				not_worth_it: 47
			},
			2020: {
				worth_it: 95,
				not_worth_it: 37
			},
			2019: {
				worth_it: 54,
				not_worth_it: 26
			},
			2018: {
				percent_only: true,
				worth_it: 61.30,
				not_worth_it: 38.70
			}
		},
		extra_tags: ["Permanent Reward"],
		lore_tags: [],
		unlock_fate: 15,
		reset_fate: 15
	}, {
		id: 5,
		title: "Flute Street",
		release_date: "2011/04",
		description: "35 storylets and opportunities about the mysteries far below the Neath. What are the Rubbery Men up to down there? What else lurks in the darkness?",
		author: "Alexis Kennedy",
		story_rankings: {
			2024: {
				exceptional: 12,
				delicious: 32,
				liked: 48,
				okay: 36,
				dislike: 14
			},
			2023: {
				exceptional: 22,
				delicious: 46,
				liked: 63,
				okay: 28,
				dislike: 10
			},
			2022: {
				exceptional: 26,
				delicious: 60,
				liked: 82,
				okay: 51,
				dislike: 18
			},
			2021: {
				exceptional: 36,
				delicious: 79,
				liked: 72,
				okay: 40,
				dislike: 5
			},
			2020: {
				exceptional: 30,
				delicious: 59,
				liked: 45,
				okay: 14,
				dislike: 1
			},
			2019: {
				exceptional: 20,
				delicious: 33,
				liked: 25,
				okay: 7,
				dislike: 0
			},
			2018: {
				exceptional: 18,
				delicious: 31,
				liked: 23,
				okay: 6,
				dislike: 8
			}
		},
		reward_rankings: {
			// this is ranking without Theological Husbandry
			2024: {
				worth_it: 80,
				not_worth_it: 56
			},
			2023: {
				worth_it: 123,
				not_worth_it: 44
			},
			2022: {
				worth_it: 189,
				not_worth_it: 48
			},
			2021: {
				worth_it: 190,
				not_worth_it: 47
			},
			2020: {
				worth_it: 123,
				not_worth_it: 31
			},
			2019: {
				worth_it: 78,
				not_worth_it: 12
			}
			,
			2018: {
				percent_only: true,
				worth_it: 85.40,
				not_worth_it: 14.60
			}
		},
		extra_tags: ["Rubberies", "Permanent Reward"],
		lore_tags: [],
		unlock_fate: 25,
		reset_fate: 0
	}, {
		id: 6,
		title: "Theological Husbandry",
		release_date: "2011/09/01",
		description: "Unlock the hidden secrets of the Fourth Coil - four new beasts to breed and call your own. And the terrible truth of the Bishop's plans. A dozen storylets and opportunities of bestial savagery and revenge!",
		story_rankings: {
			2024: {
				exceptional: 19,
				delicious: 27,
				liked: 43,
				okay: 23,
				dislike: 7
			},
			2023: {
				exceptional: 37,
				delicious: 47,
				liked: 46,
				okay: 13,
				dislike: 4
			},
			2022: {
				exceptional: 47,
				delicious: 54,
				liked: 76,
				okay: 38,
				dislike: 5
			},
			2021: {
				exceptional: 47,
				delicious: 76,
				liked: 64,
				okay: 20,
				dislike: 4
			},
			2020: {
				exceptional: 41,
				delicious: 60,
				liked: 35,
				okay: 11,
				dislike: 4
			},
			2019: {
				exceptional: 36,
				delicious: 24,
				liked: 21,
				okay: 10,
				dislike: 0
			},
			2018: {
				exceptional: 26,
				delicious: 34,
				liked: 13,
				okay: 3,
				dislike: 0
			}
		},
		reward_rankings: {
			// this is ranking without Flute Street
			2024: {
				worth_it: 95,
				not_worth_it: 25
			},
			2023: {
				worth_it: 127,
				not_worth_it: 18
			},
			2022: {
				worth_it: 205,
				not_worth_it: 20
			},
			2021: {
				worth_it: 187,
				not_worth_it: 38
			},
			2020: {
				worth_it: 135,
				not_worth_it: 17
			},
			2019: {
				worth_it: 86,
				not_worth_it: 9
			},
			2018: {
				percent_only: true,
				worth_it: 94,
				not_worth_it: 6
			}
		},
		extra_tags: ["Bishop of Southwark", "Permanent Reward", "The Church"],
		lore_tags: [],
		unlock_fate: 20
	}, {
		id: 7,
		title: "The Spinning of the Wheels",
		release_date_est: "2011/09/12",
		description: "Unlock the secrets of the Velocipede Squad and decide what to do about them. A dozen storylets and opportunities of metropolitan savagery!",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 4,
				liked: 12,
				okay: 21,
				dislike: 20
			},
			2023: {
				exceptional: 8,
				delicious: 8,
				liked: 25,
				okay: 21,
				dislike: 23
			},
			2022: {
				exceptional: 3,
				delicious: 12,
				liked: 26,
				okay: 39,
				dislike: 27
			},
			2021: {
				exceptional: 4,
				delicious: 18,
				liked: 35,
				okay: 40,
				dislike: 21
			},
			2020: {
				exceptional: 2,
				delicious: 13,
				liked: 33,
				okay: 29,
				dislike: 12
			},
			2019: {
				exceptional: 0,
				delicious: 8,
				liked: 16,
				okay: 18,
				dislike: 13
			},
			2018: {
				exceptional: 1,
				delicious: 8,
				liked: 11,
				okay: 15,
				dislike: 9
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 31,
				not_worth_it: 39
			},
			2023: {
				worth_it: 50,
				not_worth_it: 44
			},
			2022: {
				worth_it: 71,
				not_worth_it: 54
			},
			2021: {
				worth_it: 61,
				not_worth_it: 69
			},
			2020: {
				worth_it: 67,
				not_worth_it: 32
			},
			2019: {
				worth_it: 54,
				not_worth_it: 10
			},
			2018: {
				percent_only: true,
				worth_it: 63.60,
				not_worth_it: 36.40
			}
		},
		extra_tags: ["Constables", "Permanent Reward"],
		lore_tags: [],
		unlock_fate: 20
	}, {
		id: 8,
		title: "Mysteries of the Foreign Office",
		release_date_est: "2011/10",
		description: "This story will allow you to uncover the truth about the struggle between Face and Teeth. Certain unusual rewards may also be available.",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 14,
				liked: 24,
				okay: 16,
				dislike: 3
			},
			2023: {
				exceptional: 7,
				delicious: 21,
				liked: 31,
				okay: 14,
				dislike: 3
			},
			2022: {
				exceptional: 9,
				delicious: 30,
				liked: 35,
				okay: 22,
				dislike: 11
			},
			2021: {
				exceptional: 14,
				delicious: 32,
				liked: 41,
				okay: 19,
				dislike: 8
			},
			2020: {
				exceptional: 8,
				delicious: 27,
				liked: 24,
				okay: 16,
				dislike: 5
			},
			2019: {
				exceptional: 4,
				delicious: 13,
				liked: 17,
				okay: 8,
				dislike: 1
			},
			2018: {
				exceptional: 6,
				delicious: 15,
				liked: 11,
				okay: 5,
				dislike: 1
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 11,
				not_worth_it: 43
			},
			2023: {
				worth_it: 31,
				not_worth_it: 51
			},
			2022: {
				worth_it: 34,
				not_worth_it: 67
			},
			2021: {
				worth_it: 30,
				not_worth_it: 85
			},
			2020: {
				worth_it: 23,
				not_worth_it: 58
			},
			2019: {
				worth_it: 14,
				not_worth_it: 31
			},
			2018: {
				percent_only: true,
				worth_it: 38.30,
				not_worth_it: 61.70
			}
		},
		extra_tags: ["The Great Game", "Permanent Reward"],
		spoiler_tags: ["Urchins", "Snuffers"],
		lore_tags: [],
		unlock_fate: 20
	}, {
		/* est. release between 2011/09 (since it requires Velocipede Squad) & 2012/05/31 (earliest mention I've found)*/
		id: 9,
		title: "The Jack-of-Smiles Case",
		description: "Pick up the knife. Become Jack. Find out.<br><em>WARNING \u2013 doing this will mean you become Jack-of-Smiles. The content here will be significantly more graphic and visceral than usual for Fallen London.</em>",
		story_rankings: {
			2024: {
				exceptional: 16,
				delicious: 24,
				liked: 27,
				okay: 10,
				dislike: 8
			},
			2023: {
				exceptional: 25,
				delicious: 34,
				liked: 36,
				okay: 15,
				dislike: 2
			},
			2022: {
				exceptional: 39,
				delicious: 50,
				liked: 38,
				okay: 33,
				dislike: 11
			},
			2021: {
				exceptional: 50,
				delicious: 50,
				liked: 38,
				okay: 17,
				dislike: 8
			},
			2020: {
				exceptional: 39,
				delicious: 36,
				liked: 23,
				okay: 16,
				dislike: 2
			},
			2019: {
				exceptional: 19,
				delicious: 29,
				liked: 16,
				okay: 9,
				dislike: 3
			},
			2018: {
				exceptional: 17,
				delicious: 19,
				liked: 14,
				okay: 5,
				dislike: 1
			}
		},
		lore_tags: [],
		unlock_fate: 10
	}, {
		id: 10,
		title: "A Functionary's Confidant",
		release_date: "2014/09",
		description: "This will allow you to visit the Functionary at his home, and delve into the secrets of his past and work. These stories have a conversational focus. They cost no actions to play and have no material rewards.",
		author: "Emily Short",
		story_rankings: {
			2024: {
				exceptional: 4,
				delicious: 7,
				liked: 9,
				okay: 9,
				dislike: 2
			},
			2023: {
				exceptional: 7,
				delicious: 6,
				liked: 13,
				okay: 12,
				dislike: 2
			},
			2022: {
				exceptional: 14,
				delicious: 17,
				liked: 16,
				okay: 13,
				dislike: 7
			},
			2021: {
				exceptional: 13,
				delicious: 26,
				liked: 22,
				okay: 12,
				dislike: 4
			},
			2020: {
				exceptional: 11,
				delicious: 18,
				liked: 18,
				okay: 10,
				dislike: 0
			},
			2019: {
				exceptional: 8,
				delicious: 4,
				liked: 11,
				okay: 8,
				dislike: 1
			},
			2018: {
				exceptional: 6,
				delicious: 14,
				liked: 11,
				okay: 3,
				dislike: 3
			}
		},
		spoiler_tags: ["New Sequence"],
		lore_tags: [],
		unlock_fate: 15
	}, {
		id: 11,
		title: "A Trade in Faces",
		release_date: "2014/10/30",
		author: "Chris Gardiner",
		description: "This will begin A Trade in Faces, a story of fluid identity and London's forgotten corners. Uncover the mysteries of the Face-Tailor! It is suitable for characters with some skills at 50 or above.",
		story_rankings: {
			2024: {
				exceptional: 6,
				delicious: 8,
				liked: 21,
				okay: 21,
				dislike: 3
			},
			2023: {
				exceptional: 5,
				delicious: 19,
				liked: 31,
				okay: 16,
				dislike: 7
			},
			2022: {
				exceptional: 6,
				delicious: 15,
				liked: 35,
				okay: 26,
				dislike: 6
			},
			2021: {
				exceptional: 8,
				delicious: 27,
				liked: 34,
				okay: 19,
				dislike: 6
			},
			2020: {
				exceptional: 9,
				delicious: 16,
				liked: 38,
				okay: 14,
				dislike: 4
			},
			2019: {
				exceptional: 5,
				delicious: 9,
				liked: 15,
				okay: 10,
				dislike: 0
			},
			2018: {
				exceptional: 6,
				delicious: 20,
				liked: 7,
				okay: 10,
				dislike: 2
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 15,
				not_worth_it: 36
			},
			2023: {
				worth_it: 22,
				not_worth_it: 56
			},
			2022: {
				worth_it: 20,
				not_worth_it: 67
			},
			2021: {
				worth_it: 24,
				not_worth_it: 67
			},
			2020: {
				worth_it: 19,
				not_worth_it: 59
			},
			2019: {
				worth_it: 14,
				not_worth_it: 31
			},
			2018: {
				percent_only: true,
				worth_it: 19,
				not_worth_it: 81
			}
		},
		extra_tags: ["Snuffers", "Permanent Reward"],
		lore_tags: [],
		unlock_fate: 25,
		reset_fate: 25
	}, {
		id: 12,
		title: "The Gift",
		release_date: "2014/12/24",
		author: "Chris Gardiner",
		description: "A tale of terror, aberration, and the horrors beneath the Palace. [Suitable for characters with main skills at 50 or above.]",
		story_rankings: {
			2024: {
				exceptional: 32,
				delicious: 33,
				liked: 20,
				okay: 8,
				dislike: 0
			},
			2023: {
				exceptional: 56,
				delicious: 38,
				liked: 20,
				okay: 7,
				dislike: 0
			},
			2022: {
				exceptional: 81,
				delicious: 58,
				liked: 25,
				okay: 5,
				dislike: 1
			},
			2021: {
				exceptional: 89,
				delicious: 54,
				liked: 30,
				okay: 3,
				dislike: 1
			},
			2020: {
				exceptional: 74,
				delicious: 41,
				liked: 10,
				okay: 2,
				dislike: 0
			},
			2019: {
				exceptional: 47,
				delicious: 27,
				liked: 10,
				okay: 3,
				dislike: 0
			},
			2018: {
				exceptional: 41,
				delicious: 18,
				liked: 9,
				okay: 3,
				dislike: 0
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 22,
				not_worth_it: 62
			},
			2023: {
				worth_it: 50,
				not_worth_it: 64
			},
			2022: {
				worth_it: 73,
				not_worth_it: 92
			},
			2021: {
				worth_it: 70,
				not_worth_it: 95
			},
			2020: {
				worth_it: 39,
				not_worth_it: 75
			},
			2019: {
				worth_it: 36,
				not_worth_it: 45
			},
			2018: {
				percent_only: true,
				worth_it: 39,
				not_worth_it: 61
			}
		},
		official_tags: ["Society", "The Palace"],
		extra_tags: ["Festive Tale", "Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "new-content-the-gift/"
		},
		unlock_fate: 30,
		reset_fate: 30
	}, {
		id: 13,
		title: "The Blemmigan Affair",
		release_date: "2015/02/27",
		author: "Richard Cobbett",
		description: "Investigate London's recent fungal fad in the Blemmigan Affair! A mischievous story of inappropriate poetry and a perilous expedition into Bugsby's marsh. [Especially suited to newcomers, it requires main abilities of 10 or more.]",
		story_rankings: {
			2024: {
				exceptional: 13,
				delicious: 18,
				liked: 13,
				okay: 9,
				dislike: 4
			},
			2023: {
				exceptional: 20,
				delicious: 28,
				liked: 25,
				okay: 11,
				dislike: 1
			},
			2022: {
				exceptional: 13,
				delicious: 40,
				liked: 24,
				okay: 25,
				dislike: 1
			},
			2021: {
				exceptional: 24,
				delicious: 23,
				liked: 29,
				okay: 12,
				dislike: 2
			},
			2020: {
				exceptional: 16,
				delicious: 25,
				liked: 28,
				okay: 10,
				dislike: 0
			},
			2019: {
				exceptional: 7,
				delicious: 17,
				liked: 13,
				okay: 3,
				dislike: 2
			},
			2018: {
				exceptional: 6,
				delicious: 19,
				liked: 14,
				okay: 3,
				dislike: 0
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 35,
				not_worth_it: 24
			},
			2023: {
				worth_it: 55,
				not_worth_it: 29
			},
			2022: {
				worth_it: 66,
				not_worth_it: 44
			},
			2021: {
				worth_it: 48,
				not_worth_it: 47
			},
			2020: {
				worth_it: 45,
				not_worth_it: 31
			},
			2019: {
				worth_it: 25,
				not_worth_it: 19
			},
			2018: {
				percent_only: true,
				worth_it: 51.70,
				not_worth_it: 48.30
			}
		},
		official_tags: ["Bohemians"],
		extra_tags: ["Arts", "Mushrooms", "Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "new-content-the-blemmigan-affair/"
		},
		unlock_fate: 30,
		reset_fate: 30
	}, {
		id: 14,
		title: "The Haunting at the Marsh House",
		release_date: "2015/04/29",
		author: "Chris Gardiner",
		description: "Do you believe in ghosts? Begin the Gothic tale of the Haunting at the Marsh-House.",
		story_rankings: {
			2024: {
				exceptional: 0,
				delicious: 8,
				liked: 13,
				okay: 21,
				dislike: 7
			},
			2023: {
				exceptional: 0,
				delicious: 9,
				liked: 21,
				okay: 15,
				dislike: 2
			},
			2022: {
				exceptional: 1,
				delicious: 9,
				liked: 27,
				okay: 12,
				dislike: 2
			},
			2021: {
				exceptional: 3,
				delicious: 11,
				liked: 23,
				okay: 17,
				dislike: 4
			},
			2020: {
				exceptional: 1,
				delicious: 12,
				liked: 18,
				okay: 26,
				dislike: 4
			},
			2019: {
				exceptional: 0,
				delicious: 3,
				liked: 9,
				okay: 16,
				dislike: 1
			},
			2018: {
				exceptional: 0,
				delicious: 9,
				liked: 10,
				okay: 8,
				dislike: 2
			}
		},
		reward_rankings: {
			2022: {
				worth_it: 13,
				not_worth_it: 53
			},
			2021: {
				worth_it: 13,
				not_worth_it: 49
			},
			2020: {
				worth_it: 16,
				not_worth_it: 41
			},
			2019: {
				worth_it: 9,
				not_worth_it: 23
			},
			2018: {
				percent_only: true,
				worth_it: 27.50,
				not_worth_it: 72.50
			}
		},
		official_tags: ["Society"],
		extra_tags: ["Drownies", "Exceptional Story"],
		spoiler_tags: ["Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "new-content-the-haunting-at-the-marshhouse/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 15,
		title: "A Court of Cats",
		release_date: "2015/06/01",
		author: "Chris Gardiner",
		description: "Assist the Duchess in arbitrating the secret Court of Cats, and learn more about London's feline inhabitants!",
		story_rankings: {
			2024: {
				exceptional: 3,
				delicious: 5,
				liked: 14,
				okay: 14,
				dislike: 2
			},
			2023: {
				exceptional: 3,
				delicious: 12,
				liked: 25,
				okay: 25,
				dislike: 5
			},
			2022: {
				exceptional: 5,
				delicious: 11,
				liked: 25,
				okay: 22,
				dislike: 5
			},
			2021: {
				exceptional: 5,
				delicious: 14,
				liked: 26,
				okay: 19,
				dislike: 4
			},
			2020: {
				exceptional: 4,
				delicious: 9,
				liked: 13,
				okay: 22,
				dislike: 4
			},
			2019: {
				exceptional: 0,
				delicious: 3,
				liked: 9,
				okay: 11,
				dislike: 2
			},
			2018: {
				exceptional: 1,
				delicious: 2,
				liked: 9,
				okay: 10,
				dislike: 3
			}
		},
		official_tags: ["The Duchess", "Cats"],
		extra_tags: ["Parabola", "Fingerkings", "Ophidian Gentleman", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "exceptional-story-june--the-court-of-cats/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 16,
		title: "Lost in Reflections",
		release_date: "2015/07/01",
		authors: ["Richard Cobbett", "Alexis Kennedy"],
		description: "Help July, lost in reflections behind the mirrors, and dream of what your fate may be!",
		story_rankings: {
			2024: {
				exceptional: 27,
				delicious: 23,
				liked: 17,
				okay: 8,
				dislike: 0
			},
			2023: {
				exceptional: 28,
				delicious: 35,
				liked: 30,
				okay: 11,
				dislike: 0
			},
			2022: {
				exceptional: 50,
				delicious: 44,
				liked: 27,
				okay: 4,
				dislike: 1
			},
			2021: {
				exceptional: 55,
				delicious: 42,
				liked: 28,
				okay: 4,
				dislike: 0
			},
			2020: {
				exceptional: 65,
				delicious: 27,
				liked: 12,
				okay: 4,
				dislike: 1
			},
			2019: {
				exceptional: 43,
				delicious: 28,
				liked: 3,
				okay: 0,
				dislike: 0
			},
			2018: {
				exceptional: 38,
				delicious: 11,
				liked: 6,
				okay: 0,
				dislike: 1
			}
		},
		official_tags: ["Revolutionaries", "Fingerkings"],
		extra_tags: ["Parabola", "The Surface", "Calendar Council", "Exceptional Story"],
		spoiler_tags: ["Permanent Reward", "Sixth City", "The Correspondence", "Masters"],
		lore_tags: [],
		threads: {
			failbetter: "julys-exceptional-story-lost-in-reflections/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 17,
		title: "The Last Dog Society",
		release_date: "2015/08/01",
		author: "Rob Morgan",
		description: "A murderer hides within a secret society at the heart of Her Enduring Majesty's navy! Infiltrate their ranks and uncover the secrets of the Last Dog Society.",
		story_rankings: {
			2024: {
				exceptional: 1,
				delicious: 3,
				liked: 4,
				okay: 14,
				dislike: 3
			},
			2023: {
				exceptional: 2,
				delicious: 2,
				liked: 10,
				okay: 18,
				dislike: 6
			},
			2022: {
				exceptional: 0,
				delicious: 6,
				liked: 8,
				okay: 21,
				dislike: 6
			},
			2021: {
				exceptional: 1,
				delicious: 4,
				liked: 25,
				okay: 10,
				dislike: 4
			},
			2020: {
				exceptional: 1,
				delicious: 5,
				liked: 16,
				okay: 18,
				dislike: 5
			},
			2019: {
				exceptional: 0,
				delicious: 1,
				liked: 8,
				okay: 11,
				dislike: 3
			},
			2018: {
				exceptional: 1,
				delicious: 6,
				liked: 7,
				okay: 8,
				dislike: 4
			}
		},
		official_tags: ["The Admiralty", "The Docks"],
		extra_tags: ["New Sequence", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "augusts-exceptional-story-the-last-dog-society/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 18,
		title: "Cut with Moonlight",
		release_date: "2015/08/27",
		author: "Chris Gardiner",
		description: "Navigate the perils and profits of London's forbidden sunlight trade in this tale of lunacy, ambition and addiction.",
		story_rankings: {
			2024: {
				exceptional: 6,
				delicious: 15,
				liked: 22,
				okay: 12,
				dislike: 0
			},
			2023: {
				exceptional: 8,
				delicious: 28,
				liked: 35,
				okay: 9,
				dislike: 0
			},
			2022: {
				exceptional: 14,
				delicious: 22,
				liked: 38,
				okay: 19,
				dislike: 1
			},
			2021: {
				exceptional: 11,
				delicious: 29,
				liked: 30,
				okay: 11,
				dislike: 2
			},
			2020: {
				exceptional: 8,
				delicious: 30,
				liked: 25,
				okay: 13,
				dislike: 1
			},
			2019: {
				exceptional: 8,
				delicious: 16,
				liked: 9,
				okay: 5,
				dislike: 0
			},
			2018: {
				exceptional: 14,
				delicious: 16,
				liked: 9,
				okay: 3,
				dislike: 0
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 35,
				not_worth_it: 18
			},
			2023: {
				worth_it: 70,
				not_worth_it: 16
			},
			2022: {
				worth_it: 76,
				not_worth_it: 19
			},
			2021: {
				worth_it: 37,
				not_worth_it: 47
			},
			2020: {
				worth_it: 23,
				not_worth_it: 49
			},
			2019: {
				worth_it: 17,
				not_worth_it: 19
			},
			2018: {
				percent_only: true,
				worth_it: 37.30,
				not_worth_it: 62.70
			}
		},
		official_tags: ["Criminals"],
		extra_tags: ["Society", "Exceptional Story"],
		spoiler_tags: ["Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "septembers-exceptional-story-cut-with-moonlight/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 19,
		title: "Discernment",
		release_date: "2015/09/24",
		author: "Emily Short",
		description: "Souls are a queer business. What dictates the market value? History, or potential? Supply or demand? Would Salome's soul fetch a high price?",
		story_rankings: {
			2024: {
				exceptional: 0,
				delicious: 6,
				liked: 10,
				okay: 11,
				dislike: 10
			},
			2023: {
				exceptional: 3,
				delicious: 7,
				liked: 14,
				okay: 20,
				dislike: 5
			},
			2022: {
				exceptional: 2,
				delicious: 8,
				liked: 17,
				okay: 19,
				dislike: 7
			},
			2021: {
				exceptional: 3,
				delicious: 13,
				liked: 15,
				okay: 6,
				dislike: 6
			},
			2020: {
				exceptional: 1,
				delicious: 5,
				liked: 24,
				okay: 13,
				dislike: 5
			},
			2019: {
				exceptional: 0,
				delicious: 5,
				liked: 8,
				okay: 12,
				dislike: 2
			},
			2018: {
				exceptional: 0,
				delicious: 5,
				liked: 9,
				okay: 7,
				dislike: 8
			}
		},
		official_tags: ["Hell"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "octobers-exceptional-story-discernment/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 20,
		title: "Flint",
		release_date: "2015/11/26",
		author: "Alexis Kennedy",
		description: "In the bright heart of the Elder Continent lies a prison built to hold a pretender-god... [A large piece of content.]",
		season: "Heart's Blood",
		story_rankings: {
			2024: {
				exceptional: 21,
				delicious: 19,
				liked: 18,
				okay: 11,
				dislike: 1
			},
			2023: {
				exceptional: 29,
				delicious: 23,
				liked: 21,
				okay: 11,
				dislike: 5
			},
			2022: {
				exceptional: 41,
				delicious: 32,
				liked: 36,
				okay: 10,
				dislike: 6
			},
			2021: {
				exceptional: 48,
				delicious: 41,
				liked: 16,
				okay: 17,
				dislike: 3
			},
			2020: {
				exceptional: 57,
				delicious: 31,
				liked: 12,
				okay: 3,
				dislike: 1
			},
			2019: {
				exceptional: 35,
				delicious: 20,
				liked: 8,
				okay: 1,
				dislike: 1
			},
			2018: {
				exceptional: 38,
				delicious: 8,
				liked: 4,
				okay: 1,
				dislike: 1
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 25,
				not_worth_it: 36
			},
			2023: {
				worth_it: 50,
				not_worth_it: 34
			},
			2022: {
				worth_it: 66,
				not_worth_it: 57
			},
			2021: {
				worth_it: 55,
				not_worth_it: 69
			},
			2020: {
				worth_it: 53,
				not_worth_it: 50
			},
			2019: {
				worth_it: 31,
				not_worth_it: 31
			},
			2018: {
				percent_only: true,
				worth_it: 61,
				not_worth_it: 39
			}
		},
		extra_tags: ["Bishop of St Fiacre's", "Elder Continent", "Exceptional Story", "Permanent Reward"],
		spoiler_tags: ["Snuffers"],
		lore_tags: [],
		threads: {
			failbetter: "exceptional-story-flint-part-ii-wine-or-wound/"
		},
		unlock_fate: 120,
		reset_fate: 120
	}, {
		id: 21,
		title: "The Art of Murder",
		release_date: "2015/12/31",
		author: "Gavin Inglis",
		description: "A new sensation in Bohemian circles. Sharp knives. Deadly poison. Elaborate and impractical deathtraps.",
		season: "Heart's Blood",
		story_rankings: {
			2024: {
				exceptional: 1,
				delicious: 5,
				liked: 12,
				okay: 13,
				dislike: 3
			},
			2023: {
				exceptional: 1,
				delicious: 7,
				liked: 12,
				okay: 20,
				dislike: 8
			},
			2022: {
				exceptional: 1,
				delicious: 10,
				liked: 20,
				okay: 17,
				dislike: 7
			},
			2021: {
				exceptional: 4,
				delicious: 7,
				liked: 28,
				okay: 18,
				dislike: 7
			},
			2020: {
				exceptional: 2,
				delicious: 18,
				liked: 22,
				okay: 17,
				dislike: 6
			},
			2019: {
				exceptional: 1,
				delicious: 11,
				liked: 15,
				okay: 10,
				dislike: 2
			},
			2018: {
				exceptional: 2,
				delicious: 11,
				liked: 17,
				okay: 7,
				dislike: 0
			}
		},
		official_tags: ["Bohemians"],
		extra_tags: ["Arts", "Exceptional Story"],
		spoiler_tags: ["Snuffers"],
		lore_tags: [],
		threads: {
			failbetter: "an-exceptional-story-the-art-of-murder/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 22,
		title: "The Waltz That Moved the World",
		release_date: "2016/01/28",
		author: "Cash DeCuir",
		description: "The Resolute Aesthete has travelled from Vienna. She has her mother's bloodied bandages, three leads to her father, and a knife.",
		season: "Heart's Blood",
		story_rankings: {
			2024: {
				exceptional: 7,
				delicious: 12,
				liked: 16,
				okay: 11,
				dislike: 4
			},
			2023: {
				exceptional: 8,
				delicious: 17,
				liked: 16,
				okay: 14,
				dislike: 2
			},
			2022: {
				exceptional: 15,
				delicious: 23,
				liked: 20,
				okay: 12,
				dislike: 1
			},
			2021: {
				exceptional: 18,
				delicious: 30,
				liked: 25,
				okay: 9,
				dislike: 5
			},
			2020: {
				exceptional: 25,
				delicious: 24,
				liked: 21,
				okay: 6,
				dislike: 2
			},
			2019: {
				exceptional: 15,
				delicious: 16,
				liked: 13,
				okay: 3,
				dislike: 0
			},
			2018: {
				exceptional: 14,
				delicious: 12,
				liked: 9,
				okay: 4,
				dislike: 0
			}
		},
		official_tags: ["Society", "The Great Game"],
		extra_tags: ["Exceptional Story", "Deep Lore"],
		lore_tags: [],
		threads: {
			failbetter: "exceptional-story-the-waltz-that-moved-the-world/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 23,
		title: "The Frequently Deceased",
		release_date: "2016/02/25",
		author: "Emily Short",
		description: "A family of promising youngsters have killed their Governess again. This time, she hasn't come back.",
		season: "Family Ties",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 10,
				liked: 8,
				okay: 10,
				dislike: 3
			},
			2023: {
				exceptional: 5,
				delicious: 11,
				liked: 14,
				okay: 16,
				dislike: 2
			},
			2022: {
				exceptional: 5,
				delicious: 18,
				liked: 18,
				okay: 15,
				dislike: 3
			},
			2021: {
				exceptional: 5,
				delicious: 18,
				liked: 22,
				okay: 9,
				dislike: 4
			},
			2020: {
				exceptional: 5,
				delicious: 18,
				liked: 23,
				okay: 10,
				dislike: 2
			},
			2019: {
				exceptional: 4,
				delicious: 10,
				liked: 14,
				okay: 6,
				dislike: 1
			},
			2018: {
				exceptional: 5,
				delicious: 9,
				liked: 18,
				okay: 3,
				dislike: 2
			}
		},
		reward_rankings: {
			2021: {
				worth_it: 12,
				not_worth_it: 52
			},
			2020: {
				worth_it: 7,
				not_worth_it: 46
			},
			2019: {
				worth_it: 8,
				not_worth_it: 24
			},
			2018: {
				percent_only: true,
				worth_it: 27.10,
				not_worth_it: 72.90
			}
		},
		official_tags: ["Society"],
		extra_tags: ["Class Division", "Exceptional Story"],
		spoiler_tags: ["Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "march-exceptional-story-the-frequently-deceased/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 24,
		title: "The Seven-Day Reign",
		release_date: "2016/03/31",
		author: "Chris Gardiner",
		description: "The Young Stags are holding a lottery! They require a temporary sovereign to reign over them for seven days.",
		season: "Family Ties",
		story_rankings: {
			2024: {
				exceptional: 1,
				delicious: 5,
				liked: 21,
				okay: 10,
				dislike: 2
			},
			2023: {
				exceptional: 0,
				delicious: 15,
				liked: 18,
				okay: 12,
				dislike: 4
			},
			2022: {
				exceptional: 2,
				delicious: 14,
				liked: 17,
				okay: 15,
				dislike: 5
			},
			2021: {
				exceptional: 4,
				delicious: 13,
				liked: 22,
				okay: 8,
				dislike: 5
			},
			2020: {
				exceptional: 5,
				delicious: 13,
				liked: 14,
				okay: 14,
				dislike: 6
			},
			2019: {
				exceptional: 2,
				delicious: 9,
				liked: 7,
				okay: 9,
				dislike: 3
			},
			2018: {
				exceptional: 2,
				delicious: 8,
				liked: 10,
				okay: 13,
				dislike: 2
			}
		},
		official_tags: ["Society"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "april-exceptional-story-the-sevenday-reign/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 25,
		title: "The Pentecost Predicament",
		release_date: "2016/04/28",
		author: "Richard Cobbett",
		description: "Be served by trained Pentecost Apes whose aspirations of humanity will amuse and repel. Discover the dark culinary secrets of the Empire of Hands. Just keep a tight grip on your soul.",
		season: "Family Ties",
		story_rankings: {
			2024: {
				exceptional: 0,
				delicious: 5,
				liked: 14,
				okay: 16,
				dislike: 7
			},
			2023: {
				exceptional: 2,
				delicious: 7,
				liked: 24,
				okay: 13,
				dislike: 5
			},
			2022: {
				exceptional: 9,
				delicious: 5,
				liked: 21,
				okay: 19,
				dislike: 5
			},
			2021: {
				exceptional: 3,
				delicious: 15,
				liked: 18,
				okay: 9,
				dislike: 3
			},
			2020: {
				exceptional: 2,
				delicious: 10,
				liked: 17,
				okay: 15,
				dislike: 6
			},
			2019: {
				exceptional: 0,
				delicious: 11,
				liked: 11,
				okay: 8,
				dislike: 1
			},
			2018: {
				exceptional: 1,
				delicious: 10,
				liked: 11,
				okay: 11,
				dislike: 2
			}
		},
		extra_tags: ["Exceptional Story", "Sunless Sea"],
		lore_tags: [],
		threads: {
			failbetter: "may-exceptional-story-the-pentecost-predicament/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 26,
		title: "Five Minutes to Midday",
		release_date: "2016/05/26",
		author: "Cash DeCuir",
		description: "Being the story of a man with a bomb and a political vendetta. Join the Subdued Protester's plot for justice in this tale of intrigue and deception.",
		season: "Revolutions",
		story_rankings: {
			2024: {
				exceptional: 6,
				delicious: 9,
				liked: 14,
				okay: 9,
				dislike: 2
			},
			2023: {
				exceptional: 13,
				delicious: 18,
				liked: 11,
				okay: 14,
				dislike: 3
			},
			2022: {
				exceptional: 16,
				delicious: 17,
				liked: 17,
				okay: 17,
				dislike: 5
			},
			2021: {
				exceptional: 12,
				delicious: 24,
				liked: 21,
				okay: 12,
				dislike: 3
			},
			2020: {
				exceptional: 9,
				delicious: 15,
				liked: 21,
				okay: 9,
				dislike: 2
			},
			2019: {
				exceptional: 3,
				delicious: 7,
				liked: 11,
				okay: 8,
				dislike: 3
			},
			2018: {
				exceptional: 3,
				delicious: 12,
				liked: 15,
				okay: 7,
				dislike: 1
			}
		},
		official_tags: ["Hell", "Revolutionaries"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "june-exceptional-story-five-minutes-to-midday/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 27,
		title: "The Chimney Pot Wars",
		release_date: "2016/06/30",
		author: "Chris Gardiner",
		description: "London's urchin-gangs are at each others' throats, and chaos spills down to the streets! Form alliances, lead an urchin-army of your own, and solve the riddle of the Chimney Pot Wars!",
		season: "Revolutions",
		story_rankings: {
			2024: {
				exceptional: 4,
				delicious: 6,
				liked: 11,
				okay: 6,
				dislike: 3
			},
			2023: {
				exceptional: 7,
				delicious: 11,
				liked: 15,
				okay: 10,
				dislike: 5
			},
			2022: {
				exceptional: 16,
				delicious: 13,
				liked: 22,
				okay: 16,
				dislike: 5
			},
			2021: {
				exceptional: 10,
				delicious: 23,
				liked: 24,
				okay: 8,
				dislike: 3
			},
			2020: {
				exceptional: 16,
				delicious: 17,
				liked: 22,
				okay: 10,
				dislike: 1
			},
			2019: {
				exceptional: 5,
				delicious: 11,
				liked: 12,
				okay: 4,
				dislike: 1
			},
			2018: {
				exceptional: 6,
				delicious: 19,
				liked: 6,
				okay: 3,
				dislike: 1
			}
		},
		official_tags: ["Urchins"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "july-exceptional-story-the-chimney-pot-wars/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 28,
		title: "The Calendar Code",
		release_date: "2016/07/28",
		author: "Gavin Inglis",
		description: "Infiltrate a private library at the behest of a mysterious buyer and unravel the mechanism that protects its heart!",
		season: "Revolutions",
		story_rankings: {
			2024: {
				exceptional: 11,
				delicious: 15,
				liked: 13,
				okay: 10,
				dislike: 1
			},
			2023: {
				exceptional: 17,
				delicious: 25,
				liked: 25,
				okay: 12,
				dislike: 1
			},
			2022: {
				exceptional: 25,
				delicious: 34,
				liked: 28,
				okay: 9,
				dislike: 0
			},
			2021: {
				exceptional: 28,
				delicious: 32,
				liked: 19,
				okay: 9,
				dislike: 1
			},
			2020: {
				exceptional: 29,
				delicious: 16,
				liked: 16,
				okay: 7,
				dislike: 0
			},
			2019: {
				exceptional: 14,
				delicious: 17,
				liked: 6,
				okay: 3,
				dislike: 1
			},
			2018: {
				exceptional: 15,
				delicious: 15,
				liked: 9,
				okay: 1,
				dislike: 0
			}
		},
		official_tags: ["Revolutionaries"],
		extra_tags: ["Calendar Council", "Exceptional Story"],
		spoiler_tags: ["Second City", "The Duchess"],
		lore_tags: [],
		threads: {
			failbetter: "august-exceptional-story-the-calendar-code/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 29,
		title: "Where You and I Must Go",
		release_date: "2016/08/25",
		author: "Cash DeCuir",
		description: "Travel north with a crew and a coffin. Survive a white nightmare. Unravel the mystery of one of London's greatest follies!",
		season: "Wrecks",
		story_rankings: {
			2024: {
				exceptional: 9,
				delicious: 19,
				liked: 16,
				okay: 6,
				dislike: 2
			},
			2023: {
				exceptional: 19,
				delicious: 27,
				liked: 18,
				okay: 7,
				dislike: 2
			},
			2022: {
				exceptional: 24,
				delicious: 30,
				liked: 20,
				okay: 10,
				dislike: 2
			},
			2021: {
				exceptional: 27,
				delicious: 29,
				liked: 17,
				okay: 4,
				dislike: 5
			},
			2020: {
				exceptional: 32,
				delicious: 30,
				liked: 19,
				okay: 6,
				dislike: 1
			},
			2019: {
				exceptional: 18,
				delicious: 21,
				liked: 4,
				okay: 0,
				dislike: 0
			},
			2018: {
				exceptional: 20,
				delicious: 15,
				liked: 11,
				okay: 0,
				dislike: 1
			}
		},
		official_tags: ["The Admiralty"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "september-es-where-you-and-i-must-go/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 30,
		title: "Our Lady of Pyres",
		release_date: "2016/09/29",
		author: "James Chew",
		description: "Journey to a wrecked ship where two faiths are at war. Conspire against the New Sequence and the Conflagrati. Deliver the Severe Bluejacket's bequeathment.",
		season: "Wrecks",
		story_rankings: {
			2024: {
				exceptional: 1,
				delicious: 5,
				liked: 14,
				okay: 8,
				dislike: 2
			},
			2023: {
				exceptional: 4,
				delicious: 11,
				liked: 19,
				okay: 9,
				dislike: 5
			},
			2022: {
				exceptional: 2,
				delicious: 9,
				liked: 24,
				okay: 18,
				dislike: 5
			},
			2021: {
				exceptional: 3,
				delicious: 15,
				liked: 33,
				okay: 14,
				dislike: 6
			},
			2020: {
				exceptional: 6,
				delicious: 14,
				liked: 24,
				okay: 17,
				dislike: 6
			},
			2019: {
				exceptional: 2,
				delicious: 5,
				liked: 13,
				okay: 12,
				dislike: 2
			},
			2018: {
				exceptional: 3,
				delicious: 13,
				liked: 15,
				okay: 10,
				dislike: 3
			}
		},
		official_tags: ["The Admiralty"],
		extra_tags: ["New Sequence", "Animescence", "Exceptional Story", "Sunless Sea"],
		lore_tags: [],
		threads: {
			failbetter: "october-exceptional-story-our-lady-of-pyres/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 31,
		title: "The Final Curtain",
		release_date: "2016/10/27",
		author: "Olivia Wood",
		description: "A failing theatre. A singular plant. The first performance of The Mirror'd Chariot. Must the show go on?",
		season: "Wrecks",
		story_rankings: {
			2024: {
				exceptional: 1,
				delicious: 5,
				liked: 10,
				okay: 11,
				dislike: 3
			},
			2023: {
				exceptional: 2,
				delicious: 8,
				liked: 17,
				okay: 15,
				dislike: 2
			},
			2022: {
				exceptional: 3,
				delicious: 13,
				liked: 15,
				okay: 12,
				dislike: 5
			},
			2021: {
				exceptional: 8,
				delicious: 17,
				liked: 24,
				okay: 11,
				dislike: 2
			},
			2020: {
				exceptional: 5,
				delicious: 18,
				liked: 31,
				okay: 7,
				dislike: 3
			},
			2019: {
				exceptional: 4,
				delicious: 11,
				liked: 12,
				okay: 7,
				dislike: 1
			},
			2018: {
				exceptional: 13,
				delicious: 10,
				liked: 12,
				okay: 6,
				dislike: 1
			}
		},
		official_tags: ["Parabola", "Bohemians"],
		extra_tags: ["Arts", "Exceptional Story"],
		spoiler_tags: ["Fingerkings"],
		lore_tags: [],
		threads: {
			failbetter: "november-exceptional-story-the-final-curtain/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 32,
		title: "The Persona Engine",
		release_date: "2016/11/24",
		author: "Cash DeCuir",
		description: "London's government departments are ineffective and widely scorned. And now, expertly forged work orders are eroding their capacities further. Discover what hidden power has been playing the government against itself!",
		season: "Skies",
		story_rankings: {
			2024: {
				exceptional: 12,
				delicious: 19,
				liked: 19,
				okay: 14,
				dislike: 3
			},
			2023: {
				exceptional: 25,
				delicious: 22,
				liked: 23,
				okay: 14,
				dislike: 1
			},
			2022: {
				exceptional: 27,
				delicious: 36,
				liked: 37,
				okay: 9,
				dislike: 2
			},
			2021: {
				exceptional: 31,
				delicious: 30,
				liked: 24,
				okay: 7,
				dislike: 0
			},
			2020: {
				exceptional: 19,
				delicious: 22,
				liked: 20,
				okay: 10,
				dislike: 1
			},
			2019: {
				exceptional: 9,
				delicious: 20,
				liked: 12,
				okay: 4,
				dislike: 0
			},
			2018: {
				exceptional: 15,
				delicious: 22,
				liked: 13,
				okay: 4,
				dislike: 0
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 18,
				not_worth_it: 42
			},
			2023: {
				worth_it: 34,
				not_worth_it: 45
			},
			2022: {
				worth_it: 61,
				not_worth_it: 52
			}
		},
		extra_tags: ["Exceptional Story"],
		spoiler_tags: ["Red Science", "Discordance"],
		lore_tags: [],
		threads: {
			failbetter: "december-exceptional-story-the-persona-engine/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 33,
		title: "The Empress' Shadow",
		release_date: "2016/12/23",
		author: "Emily Short",
		description: "The Empress' Shadow, the only child of Her Enduring Majesty residing on the Surface, has come to London. Train servants for her in Sinning Jenny's Finishing School; uncover the political and familial intrigues that threaten London.",
		story_rankings: {
			2024: {
				exceptional: 14,
				delicious: 33,
				liked: 23,
				okay: 5,
				dislike: 1
			},
			2023: {
				exceptional: 23,
				delicious: 48,
				liked: 34,
				okay: 8,
				dislike: 1
			},
			2022: {
				exceptional: 33,
				delicious: 63,
				liked: 46,
				okay: 10,
				dislike: 3
			},
			2021: {
				exceptional: 49,
				delicious: 76,
				liked: 26,
				okay: 8,
				dislike: 2
			},
			2020: {
				exceptional: 38,
				delicious: 47,
				liked: 26,
				okay: 6,
				dislike: 0
			},
			2019: {
				exceptional: 25,
				delicious: 29,
				liked: 13,
				okay: 2,
				dislike: 0
			},
			2018: {
				exceptional: 26,
				delicious: 24,
				liked: 9,
				okay: 1,
				dislike: 0
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 59,
				not_worth_it: 22
			},
			2023: {
				worth_it: 93,
				not_worth_it: 25
			},
			2022: {
				worth_it: 119,
				not_worth_it: 46
			},
			2021: {
				worth_it: 108,
				not_worth_it: 54
			},
			2020: {
				worth_it: 88,
				not_worth_it: 33
			},
			2019: {
				worth_it: 56,
				not_worth_it: 16
			},
			2018: {
				percent_only: true,
				worth_it: 90,
				not_worth_it: 10
			}
		},
		official_tags: ["Society", "The Palace"],
		extra_tags: ["The Surface", "The Great Game", "Festive Tale", "Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "a-new-festive-tale-available-for-fate/"
		},
		unlock_fate: 35,
		reset_fate: 30
	}, {
		id: 34,
		title: "The Twelve-Fifteen from Moloch Street",
		release_date: "2016/12/29",
		author: "James Chew",
		description: "Moloch Street Station, where the soulless depart London. Board the train to Hell, intercept an infernal agent, and solve a murder in which all the suspects are already damned. All aboard!",
		season: "Skies",
		story_rankings: {
			2024: {
				exceptional: 20,
				delicious: 26,
				liked: 15,
				okay: 4,
				dislike: 1
			},
			2023: {
				exceptional: 40,
				delicious: 40,
				liked: 25,
				okay: 7,
				dislike: 3
			},
			2022: {
				exceptional: 51,
				delicious: 47,
				liked: 21,
				okay: 2,
				dislike: 1
			},
			2021: {
				exceptional: 56,
				delicious: 46,
				liked: 19,
				okay: 3,
				dislike: 2
			},
			2020: {
				exceptional: 33,
				delicious: 31,
				liked: 21,
				okay: 3,
				dislike: 1
			},
			2019: {
				exceptional: 35,
				delicious: 12,
				liked: 8,
				okay: 0,
				dislike: 2
			},
			2018: {
				exceptional: 30,
				delicious: 19,
				liked: 7,
				okay: 2,
				dislike: 2
			}
		},
		official_tags: ["Hell"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "jan-es-the-twelvefifteen-from-moloch-street/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 35,
		title: "The Century Exhibition",
		release_date: "2017/01/26",
		authors: ["Fred Zeleny", "James Chew", "Cash DeCuir"],
		description: "A magnificent Exhibition has come to London, with all the wonders of modern technology on display! But a thief is amidst the gaudy pavilions, seeking an elusive prize, and an aberrant menace from the Heavens is about to be unleashed. Dare you make an enemy of time itself?",
		season: "Skies",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 12,
				liked: 16,
				okay: 9,
				dislike: 3
			},
			2023: {
				exceptional: 5,
				delicious: 22,
				liked: 24,
				okay: 13,
				dislike: 4
			},
			2022: {
				exceptional: 10,
				delicious: 31,
				liked: 27,
				okay: 15,
				dislike: 4
			},
			2021: {
				exceptional: 10,
				delicious: 31,
				liked: 31,
				okay: 10,
				dislike: 6
			},
			2020: {
				exceptional: 10,
				delicious: 19,
				liked: 22,
				okay: 20,
				dislike: 1
			},
			2019: {
				exceptional: 4,
				delicious: 17,
				liked: 20,
				okay: 5,
				dislike: 0
			},
			2018: {
				exceptional: 8,
				delicious: 17,
				liked: 15,
				okay: 8,
				dislike: 3
			}
		},
		official_tags: ["Hell", "The University"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "februarys-es-the-century-exhibition/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 36,
		title: "The Clay Man's Arm",
		release_date: "2017/02/23",
		author: "Graham Robertson",
		description: "Deep in the Clay Quarter, someone is experimenting with flesh and clay. A distressing morning discovery makes clear that you have been his next victim. Will you seek out this scholar and his collection of ghastly subjects?",
		season: "Stones",
		story_rankings: {
			2024: {
				exceptional: 3,
				delicious: 3,
				liked: 12,
				okay: 5,
				dislike: 12
			},
			2023: {
				exceptional: 0,
				delicious: 7,
				liked: 13,
				okay: 14,
				dislike: 13
			},
			2022: {
				exceptional: 1,
				delicious: 13,
				liked: 18,
				okay: 23,
				dislike: 19
			},
			2021: {
				exceptional: 5,
				delicious: 14,
				liked: 19,
				okay: 22,
				dislike: 14
			},
			2020: {
				exceptional: 4,
				delicious: 10,
				liked: 20,
				okay: 21,
				dislike: 15
			},
			2019: {
				exceptional: 2,
				delicious: 4,
				liked: 15,
				okay: 13,
				dislike: 7
			},
			2018: {
				exceptional: 5,
				delicious: 10,
				liked: 18,
				okay: 12,
				dislike: 8
			}
		},
		official_tags: ["Clay Men"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "marchs-exceptional-story-the-clay-mans-arm/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 37,
		title: "The Heart, the Devil and the Zee",
		release_date: "2017/03/30",
		authors: ["Olivia Wood", "James Chew"],
		description: "A Conscientious Nurse. A Besotted Poet. A Garrulous Devil. Untangle a knot of love and duty among the sooty tenements of London\u2019s working poor. But be warned: in this tale of hard lives and hard choices there is no peace to be made between the devils, the honey, and the heart.",
		season: "Stones",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 3,
				liked: 7,
				okay: 10,
				dislike: 6
			},
			2023: {
				exceptional: 2,
				delicious: 4,
				liked: 13,
				okay: 13,
				dislike: 6
			},
			2022: {
				exceptional: 2,
				delicious: 9,
				liked: 13,
				okay: 22,
				dislike: 8
			},
			2021: {
				exceptional: 5,
				delicious: 13,
				liked: 25,
				okay: 20,
				dislike: 4
			},
			2020: {
				exceptional: 6,
				delicious: 10,
				liked: 18,
				okay: 19,
				dislike: 9
			},
			2019: {
				exceptional: 4,
				delicious: 6,
				liked: 18,
				okay: 11,
				dislike: 3
			},
			2018: {
				exceptional: 7,
				delicious: 10,
				liked: 15,
				okay: 9,
				dislike: 5
			}
		},
		official_tags: ["Bohemians", "Urchins"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "aprils-es-the-heart-the-devil-and-the-zee/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 38,
		title: "HOJOTOHO!",
		release_date: "2017/04/27",
		author: "Cash DeCuir",
		description: "London's street musicians sing of a child Valkyrie, who roams the winding streets with her broom-spear, setting right the wrongs of the world. Join her crusade and mentor the abandoned children she has taken under her wing. But take heed: she has declared herself against a singularly dangerous enemy \u2013 Mister Songbird, the whistler in the night.",
		season: "Stones",
		story_rankings: {
			2024: {
				exceptional: 53,
				delicious: 35,
				liked: 19,
				okay: 2,
				dislike: 4
			},
			2023: {
				exceptional: 63,
				delicious: 22,
				liked: 10,
				okay: 7,
				dislike: 8
			},
			2022: {
				exceptional: 89,
				delicious: 36,
				liked: 11,
				okay: 6,
				dislike: 6
			},
			2021: {
				exceptional: 98,
				delicious: 33,
				liked: 12,
				okay: 6,
				dislike: 3
			},
			2020: {
				exceptional: 71,
				delicious: 27,
				liked: 6,
				okay: 4,
				dislike: 1
			},
			2019: {
				exceptional: 51,
				delicious: 15,
				liked: 3,
				okay: 2,
				dislike: 2
			},
			2018: {
				exceptional: 39,
				delicious: 6,
				liked: 5,
				okay: 1,
				dislike: 1
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 65,
				not_worth_it: 33
			},
			2023: {
				worth_it: 68,
				not_worth_it: 30
			},
			2022: {
				worth_it: 90,
				not_worth_it: 51
			},
			2021: {
				worth_it: 90,
				not_worth_it: 54
			},
			2020: {
				worth_it: 57,
				not_worth_it: 39
			},
			2019: {
				worth_it: 44,
				not_worth_it: 28
			},
			2018: {
				percent_only: true,
				worth_it: 60,
				not_worth_it: 40
			}
		},
		official_tags: ["Urchins", "The Great Game"],
		extra_tags: ["Exceptional Story"],
		spoiler_tags: ["Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "mays-exceptional-story-hojotoho/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 39,
		title: "The Web of the Motherlings",
		release_date: "2017/05/25",
		author: "Graham Robertson",
		description: "Relics of a mysterious Fourth City Cult have disappeared, sparking a desperate scramble to reclaim them. A scarred Khaganian competes with a one-eyed, spidery Seamstress. Choose your alliances carefully, and be ready for a confrontation in the Forgotten Quarter.",
		season: "Ruins",
		story_rankings: {
			2024: {
				exceptional: 1,
				delicious: 6,
				liked: 18,
				okay: 5,
				dislike: 4
			},
			2023: {
				exceptional: 0,
				delicious: 13,
				liked: 20,
				okay: 13,
				dislike: 4
			},
			2022: {
				exceptional: 1,
				delicious: 17,
				liked: 27,
				okay: 15,
				dislike: 9
			},
			2021: {
				exceptional: 4,
				delicious: 17,
				liked: 35,
				okay: 12,
				dislike: 5
			},
			2020: {
				exceptional: 5,
				delicious: 27,
				liked: 21,
				okay: 14,
				dislike: 3
			},
			2019: {
				exceptional: 3,
				delicious: 17,
				liked: 16,
				okay: 8,
				dislike: 2
			},
			2018: {
				exceptional: 3,
				delicious: 18,
				liked: 18,
				okay: 13,
				dislike: 1
			}
		},
		extra_tags: ["Sorrow-Spiders", "Fourth City", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "junes-exceptional-story-web-of-the-motherlings/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 40,
		title: "All Things Must End",
		release_date: "2017/06/29",
		author: "Olivia Wood",
		description: "The discovery of a singular moth prompts a journey to Tanah-Chook, a tomb-colony with ancient roots. Assist an Ebullient Undertaker in her final duties. Investigate what befell the Third City's revolutionaries. And learn the fiercest complaint of the dead.",
		season: "Ruins",
		story_rankings: {
			2024: {
				exceptional: 6,
				delicious: 19,
				liked: 12,
				okay: 2,
				dislike: 3
			},
			2023: {
				exceptional: 14,
				delicious: 27,
				liked: 14,
				okay: 5,
				dislike: 0
			},
			2022: {
				exceptional: 21,
				delicious: 30,
				liked: 19,
				okay: 9,
				dislike: 2
			},
			2021: {
				exceptional: 21,
				delicious: 32,
				liked: 27,
				okay: 8,
				dislike: 0
			},
			2020: {
				exceptional: 27,
				delicious: 23,
				liked: 22,
				okay: 7,
				dislike: 1
			},
			2019: {
				exceptional: 22,
				delicious: 14,
				liked: 16,
				okay: 1,
				dislike: 0
			},
			2018: {
				exceptional: 25,
				delicious: 19,
				liked: 6,
				okay: 3,
				dislike: 0
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 12,
				not_worth_it: 31
			},
			2023: {
				worth_it: 24,
				not_worth_it: 42
			},
			2022: {
				worth_it: 29,
				not_worth_it: 63
			},
			2021: {
				worth_it: 28,
				not_worth_it: 72
			},
			2020: {
				worth_it: 30,
				not_worth_it: 50
			},
			2019: {
				worth_it: 24,
				not_worth_it: 27
			},
			2018: {
				percent_only: true,
				worth_it: 49.20,
				not_worth_it: 50.80
			}
		},
		official_tags: ["The Tomb-Colonies"],
		extra_tags: ["Third City", "Exceptional Story"],
		spoiler_tags: ["Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "julys-exceptional-story-all-things-must-end/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 41,
		title: "The Attendants",
		release_date: "2017/07/27",
		author: "James Chew",
		description: "Accept an invitation to a secret party from a mysterious acquaintance. Navigate a sinister game of hide and seek and survive the horrors of charades. Explore a manor hiding its greater half. Will you uncover the hidden history of the Second City?",
		season: "Ruins",
		story_rankings: {
			2024: {
				exceptional: 8,
				delicious: 13,
				liked: 18,
				okay: 7,
				dislike: 0
			},
			2023: {
				exceptional: 7,
				delicious: 26,
				liked: 18,
				okay: 10,
				dislike: 1
			},
			2022: {
				exceptional: 9,
				delicious: 29,
				liked: 33,
				okay: 6,
				dislike: 1
			},
			2021: {
				exceptional: 13,
				delicious: 30,
				liked: 28,
				okay: 8,
				dislike: 4
			},
			2020: {
				exceptional: 7,
				delicious: 32,
				liked: 21,
				okay: 12,
				dislike: 3
			},
			2019: {
				exceptional: 5,
				delicious: 16,
				liked: 19,
				okay: 4,
				dislike: 4
			},
			2018: {
				exceptional: 8,
				delicious: 17,
				liked: 17,
				okay: 8,
				dislike: 4
			}
		},
		extra_tags: ["Second City", "Exceptional Story"],
		spoiler_tags: ["Parabola"],
		lore_tags: [],
		threads: {
			failbetter: "augusts-exceptional-story-the-attendants/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 42,
		title: "Trial and Error",
		release_date: "2017/08/31",
		author: "Graham Robertson",
		description: "One of London's most notorious barristers is relying on unusual help. Uncover the secret to his victories, then face him in open court!",
		season: "Sceptres",
		story_rankings: {
			2024: {
				exceptional: 1,
				delicious: 2,
				liked: 11,
				okay: 9,
				dislike: 3
			},
			2023: {
				exceptional: 0,
				delicious: 5,
				liked: 13,
				okay: 14,
				dislike: 4
			},
			2022: {
				exceptional: 1,
				delicious: 5,
				liked: 13,
				okay: 20,
				dislike: 5
			},
			2021: {
				exceptional: 6,
				delicious: 6,
				liked: 19,
				okay: 20,
				dislike: 7
			},
			2020: {
				exceptional: 3,
				delicious: 12,
				liked: 18,
				okay: 19,
				dislike: 10
			},
			2019: {
				exceptional: 0,
				delicious: 7,
				liked: 19,
				okay: 14,
				dislike: 8
			},
			2018: {
				exceptional: 4,
				delicious: 15,
				liked: 20,
				okay: 13,
				dislike: 8
			}
		},
		official_tags: ["Hell"],
		extra_tags: ["Law", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "septembers-exceptional-story-trial-and-error/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 43,
		title: "The Stone Guest",
		release_date: "2017/09/28",
		author: "Cash DeCuir",
		description: "Don Juan: de Molina created him in poetry, Moliere brought him to the stage, Mozart immortalised him in music. And now, if you can revive fallen spirits, broker a deal with devils, fend off rude cabbies, etc., you shall join a collection of the Neath's greatest artists to capture his life and after-life in moving pictures.",
		season: "Sceptres",
		story_rankings: {
			2024: {
				exceptional: 4,
				delicious: 2,
				liked: 12,
				okay: 7,
				dislike: 3
			},
			2023: {
				exceptional: 2,
				delicious: 11,
				liked: 10,
				okay: 12,
				dislike: 4
			},
			2022: {
				exceptional: 8,
				delicious: 6,
				liked: 18,
				okay: 12,
				dislike: 7
			},
			2021: {
				exceptional: 4,
				delicious: 17,
				liked: 19,
				okay: 17,
				dislike: 4
			},
			2020: {
				exceptional: 8,
				delicious: 8,
				liked: 27,
				okay: 12,
				dislike: 7
			},
			2019: {
				exceptional: 5,
				delicious: 9,
				liked: 18,
				okay: 10,
				dislike: 7
			},
			2018: {
				exceptional: 7,
				delicious: 13,
				liked: 26,
				okay: 5,
				dislike: 5
			}
		},
		official_tags: ["Bohemians"],
		extra_tags: ["Arts", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "octobers-exceptional-story-the-stone-guest/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 44,
		title: "The Sinking Synod",
		release_date: "2017/10/26",
		author: "Gavin Inglis",
		description: "A new diocese has been proposed for London! Help the Church Synod decide who'll be the new Bishop of Watchmaker's Hill.",
		season: "Sceptres",
		story_rankings: {
			2024: {
				exceptional: 6,
				delicious: 20,
				liked: 22,
				okay: 6,
				dislike: 5
			},
			2023: {
				exceptional: 17,
				delicious: 17,
				liked: 26,
				okay: 16,
				dislike: 1
			},
			2022: {
				exceptional: 14,
				delicious: 24,
				liked: 27,
				okay: 12,
				dislike: 6
			},
			2021: {
				exceptional: 17,
				delicious: 25,
				liked: 41,
				okay: 14,
				dislike: 4
			},
			2020: {
				exceptional: 11,
				delicious: 21,
				liked: 24,
				okay: 13,
				dislike: 4
			},
			2019: {
				exceptional: 7,
				delicious: 14,
				liked: 24,
				okay: 9,
				dislike: 2
			},
			2018: {
				exceptional: 11,
				delicious: 22,
				liked: 15,
				okay: 12,
				dislike: 1
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 34,
				not_worth_it: 18
			},
			2023: {
				worth_it: 48,
				not_worth_it: 23
			},
			2022: {
				worth_it: 48,
				not_worth_it: 38
			},
			2021: {
				worth_it: 48,
				not_worth_it: 50
			}
		},
		official_tags: ["The Church"],
		extra_tags: ["Bishop of St Fiacre's", "Bishop of Southwark", "Theology", "Exceptional Story", "Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "november-exceptional-story-the-sinking-synod/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 45,
		title: "Steeped in Honey",
		release_date: "2017/11/30",
		author: "Mary Goodden",
		description: "Investigate Veilgarden's most exclusive Honey Den, infiltrate a house of charity, and venture among reprobates at Watchmaker's Hill. Join us in a cup of red honey and burrow through the lost memories of the woman with the bee-stung eyes.",
		season: "Silver",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 6,
				liked: 14,
				okay: 2,
				dislike: 2
			},
			2023: {
				exceptional: 6,
				delicious: 10,
				liked: 19,
				okay: 3,
				dislike: 0
			},
			2022: {
				exceptional: 5,
				delicious: 17,
				liked: 15,
				okay: 10,
				dislike: 1
			},
			2021: {
				exceptional: 12,
				delicious: 20,
				liked: 24,
				okay: 7,
				dislike: 4
			},
			2020: {
				exceptional: 15,
				delicious: 23,
				liked: 22,
				okay: 11,
				dislike: 2
			},
			2019: {
				exceptional: 16,
				delicious: 19,
				liked: 14,
				okay: 4,
				dislike: 2
			},
			2018: {
				exceptional: 18,
				delicious: 25,
				liked: 12,
				okay: 5,
				dislike: 1
			}
		},
		official_tags: ["Bohemians", "Society"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "december-exceptional-story-steeped-in-honey/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 46,
		title: "The Marriage of Feducci",
		release_date: "2017/12/18",
		author: "Cash DeCuir",
		description: "It is the first Royal Wedding since London fell: Feducci\u2019s proposal of marriage to the Captivating Princess has been accepted! Join the Beleaguered Wedding Planner to make or break this historic moment.",
		story_rankings: {
			2024: {
				exceptional: 7,
				delicious: 8,
				liked: 13,
				okay: 5,
				dislike: 2
			},
			2023: {
				exceptional: 7,
				delicious: 14,
				liked: 15,
				okay: 6,
				dislike: 2
			},
			2022: {
				exceptional: 3,
				delicious: 14,
				liked: 21,
				okay: 16,
				dislike: 8
			},
			2021: {
				exceptional: 10,
				delicious: 19,
				liked: 21,
				okay: 14,
				dislike: 12
			},
			2020: {
				exceptional: 11,
				delicious: 15,
				liked: 23,
				okay: 12,
				dislike: 9
			},
			2019: {
				exceptional: 8,
				delicious: 9,
				liked: 18,
				okay: 5,
				dislike: 6
			},
			2018: {
				exceptional: 6,
				delicious: 16,
				liked: 9,
				okay: 9,
				dislike: 4
			}
		},
		reward_rankings: {
			2021: {
				worth_it: 12,
				not_worth_it: 66
			},
			2020: {
				worth_it: 9,
				not_worth_it: 60
			},
			2019: {
				worth_it: 9,
				not_worth_it: 37
			},
			2018: {
				percent_only: true,
				worth_it: 25,
				not_worth_it: 75
			}
		},
		official_tags: ["Society"],
		extra_tags: ["The Palace", "Elder Continent", "Festive Tale"],
		spoiler_tags: ["Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "a-new-festive-tale-available-for-fate/"
		},
		unlock_fate: 35,
		reset_fate: 30
	}, {
		id: 47,
		title: "Lamentation Lock",
		release_date: "2017/12/28",
		author: "Gavin Inglis",
		description: "Pursue a smuggler to Lamentation Lock, built halfway between the Neath and the Surface. Trade in brutality, disquiet and estrangement with those who fled their sins.",
		season: "Silver",
		story_rankings: {
			2024: {
				exceptional: 4,
				delicious: 6,
				liked: 9,
				okay: 6,
				dislike: 3
			},
			2023: {
				exceptional: 3,
				delicious: 6,
				liked: 20,
				okay: 4,
				dislike: 5
			},
			2022: {
				exceptional: 5,
				delicious: 14,
				liked: 13,
				okay: 14,
				dislike: 8
			},
			2021: {
				exceptional: 5,
				delicious: 18,
				liked: 24,
				okay: 23,
				dislike: 4
			},
			2020: {
				exceptional: 6,
				delicious: 23,
				liked: 23,
				okay: 17,
				dislike: 5
			},
			2019: {
				exceptional: 5,
				delicious: 23,
				liked: 16,
				okay: 11,
				dislike: 2
			},
			2018: {
				exceptional: 13,
				delicious: 17,
				liked: 23,
				okay: 8,
				dislike: 3
			}
		},
		official_tags: ["Criminals"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "januarys-exceptional-story-lamentation-lock/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 48,
		title: "Factory of Favours",
		release_date: "2018/01/25",
		author: "Graham Robertson",
		description: "One of Iron and Misery's factories has gone silent. Learn why, negotiate with Clay Men, and bargain with Rattus Faber. Untangle the web that snarls the forces of industry and determine the factory's fate!",
		season: "Silver",
		story_rankings: {
			2024: {
				exceptional: 0,
				delicious: 4,
				liked: 4,
				okay: 9,
				dislike: 12
			},
			2023: {
				exceptional: 1,
				delicious: 1,
				liked: 18,
				okay: 5,
				dislike: 11
			},
			2022: {
				exceptional: 0,
				delicious: 0,
				liked: 16,
				okay: 18,
				dislike: 14
			},
			2021: {
				exceptional: 1,
				delicious: 7,
				liked: 19,
				okay: 30,
				dislike: 17
			},
			2020: {
				exceptional: 1,
				delicious: 8,
				liked: 26,
				okay: 19,
				dislike: 20
			},
			2019: {
				exceptional: 1,
				delicious: 1,
				liked: 16,
				okay: 22,
				dislike: 21
			},
			2018: {
				exceptional: 4,
				delicious: 6,
				liked: 24,
				okay: 19,
				dislike: 14
			}
		},
		official_tags: ["Clay Men"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "februarys-exceptional-story-factory-of-favours/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 49,
		title: "The Pursuit of Moths",
		release_date: "2018/02/22",
		author: "Harry Tuffs",
		description: "A rogue artist has been spreading incandescent graffiti. Join a secret society in pursuit of the mysterious vandal and help them chase down the culprit - if their own secrets don't catch up to them first.",
		season: "Adorations",
		story_rankings: {
			2024: {
				exceptional: 4,
				delicious: 7,
				liked: 7,
				okay: 12,
				dislike: 2
			},
			2023: {
				exceptional: 8,
				delicious: 11,
				liked: 15,
				okay: 7,
				dislike: 3
			},
			2022: {
				exceptional: 4,
				delicious: 21,
				liked: 18,
				okay: 8,
				dislike: 3
			},
			2021: {
				exceptional: 8,
				delicious: 26,
				liked: 22,
				okay: 11,
				dislike: 1
			},
			2020: {
				exceptional: 9,
				delicious: 33,
				liked: 31,
				okay: 9,
				dislike: 1
			},
			2019: {
				exceptional: 19,
				delicious: 26,
				liked: 19,
				okay: 4,
				dislike: 1
			},
			2018: {
				exceptional: 23,
				delicious: 24,
				liked: 21,
				okay: 5,
				dislike: 0
			}
		},
		extra_tags: ["Exceptional Story"],
		spoiler_tags: ["Snuffers"],
		lore_tags: [],
		threads: {
			failbetter: "marchs-exceptional-story-the-pursuit-of-moths/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 50,
		title: "The Murgatroyd Formula",
		release_date: "2018/03/29",
		author: "Mary Goodden",
		description: "Murgatroyd\u2019s Apothecary is yet to open, and it is already in crisis. Embroil yourself in a feud between Mr Murgatroyd\u2019s ambitious daughters, track down a dealer in moon-milk, and orchestrate the grand opening of the mercantile family's latest enterprise.",
		season: "Adorations",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 5,
				liked: 21,
				okay: 8,
				dislike: 3
			},
			2023: {
				exceptional: 5,
				delicious: 11,
				liked: 23,
				okay: 14,
				dislike: 0
			},
			2022: {
				exceptional: 4,
				delicious: 15,
				liked: 22,
				okay: 12,
				dislike: 2
			},
			2021: {
				exceptional: 7,
				delicious: 20,
				liked: 29,
				okay: 13,
				dislike: 2
			},
			2020: {
				exceptional: 7,
				delicious: 17,
				liked: 31,
				okay: 19,
				dislike: 2
			},
			2019: {
				exceptional: 3,
				delicious: 18,
				liked: 29,
				okay: 14,
				dislike: 2
			},
			2018: {
				exceptional: 8,
				delicious: 22,
				liked: 24,
				okay: 18,
				dislike: 2
			}
		},
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "aprils-exceptional-story-the-murgatroyd-formula/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 51,
		title: "The Rat-Catcher",
		release_date: "2018/04/26",
		author: "Chandler Groover",
		description: "Team up with a Department of Menace Eradicator to find her missing partner! Collect bounties, interrogate rodents, and venture into the Prickfinger Wastes. Keep your friends close, your enemies closer, and your pistol closest still.",
		season: "Adorations",
		story_rankings: {
			2024: {
				exceptional: 10,
				delicious: 11,
				liked: 9,
				okay: 2,
				dislike: 2
			},
			2023: {
				exceptional: 10,
				delicious: 22,
				liked: 14,
				okay: 7,
				dislike: 3
			},
			2022: {
				exceptional: 16,
				delicious: 21,
				liked: 20,
				okay: 4,
				dislike: 1
			},
			2021: {
				exceptional: 20,
				delicious: 30,
				liked: 22,
				okay: 10,
				dislike: 0
			},
			2020: {
				exceptional: 28,
				delicious: 27,
				liked: 22,
				okay: 6,
				dislike: 1
			},
			2019: {
				exceptional: 29,
				delicious: 19,
				liked: 14,
				okay: 5,
				dislike: 1
			}
		},
		extra_tags: ["Elder Continent", "Starved Men", "Rats", "Exceptional Story"],
		spoiler_tags: ["Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "mays-exceptional-story-the-ratcatcher/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 52,
		title: "The Bones of London",
		release_date: "2018/05/31",
		author: "Gavin Inglis",
		description: "What lies beneath London's streets? Take up your theodolite and find out! Delve into London's mysteries, uncover buried secrets and help a hidden society map the Fifth City.",
		season: "Embers",
		story_rankings: {
			2024: {
				exceptional: 1,
				delicious: 1,
				liked: 16,
				okay: 11,
				dislike: 0
			},
			2023: {
				exceptional: 4,
				delicious: 4,
				liked: 28,
				okay: 11,
				dislike: 1
			},
			2022: {
				exceptional: 3,
				delicious: 13,
				liked: 27,
				okay: 13,
				dislike: 1
			},
			2021: {
				exceptional: 7,
				delicious: 20,
				liked: 30,
				okay: 15,
				dislike: 2
			},
			2020: {
				exceptional: 3,
				delicious: 29,
				liked: 31,
				okay: 15,
				dislike: 2
			},
			2019: {
				exceptional: 2,
				delicious: 27,
				liked: 21,
				okay: 11,
				dislike: 4
			}
		},
		reward_rankings: {
			2020: {
				worth_it: 17,
				not_worth_it: 51
			},
			2019: {
				worth_it: 16,
				not_worth_it: 37
			}
		},
		official_tags: ["Society", "Masters"],
		extra_tags: ["Fifth City", "Revolutionaries", "Exceptional Story", "Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "junes-exceptional-story-the-bones-of-london/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 53,
		title: "Written in the Glim",
		release_date: "2018/06/28",
		author: "Mary Goodden",
		description: "Can the false-stars predict the future? Help a disgraced astrologer, discover your false-star sign, and embark on a hazardous research trip to find out.",
		season: "Embers",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 11,
				liked: 28,
				okay: 10,
				dislike: 4
			},
			2023: {
				exceptional: 3,
				delicious: 14,
				liked: 35,
				okay: 9,
				dislike: 2
			},
			2022: {
				exceptional: 3,
				delicious: 24,
				liked: 26,
				okay: 11,
				dislike: 0
			},
			2021: {
				exceptional: 5,
				delicious: 24,
				liked: 29,
				okay: 12,
				dislike: 3
			},
			2020: {
				exceptional: 9,
				delicious: 22,
				liked: 35,
				okay: 12,
				dislike: 1
			},
			2019: {
				exceptional: 12,
				delicious: 23,
				liked: 22,
				okay: 6,
				dislike: 1
			}
		},
		reward_rankings: {
			2022: {
				worth_it: 12,
				not_worth_it: 60
			}
		},
		extra_tags: ["Starved Men", "Shapeling Arts", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "julys-exceptional-story-written-in-the-glim/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 54,
		title: "Required Repairs",
		release_date: "2018/07/26",
		author: "Gavin Inglis",
		description: "Calamity has fallen on your lodgings! Cracks and leaks, rats and holes! Uncover a sinister plot interfering with your plumbing and baffle the Borough Council",
		season: "Embers",
		story_rankings: {
			2024: {
				exceptional: 1,
				delicious: 2,
				liked: 15,
				okay: 8,
				dislike: 6
			},
			2023: {
				exceptional: 3,
				delicious: 7,
				liked: 21,
				okay: 11,
				dislike: 3
			},
			2022: {
				exceptional: 8,
				delicious: 10,
				liked: 17,
				okay: 16,
				dislike: 8
			},
			2021: {
				exceptional: 9,
				delicious: 13,
				liked: 23,
				okay: 25,
				dislike: 10
			},
			2020: {
				exceptional: 9,
				delicious: 16,
				liked: 29,
				okay: 13,
				dislike: 12
			},
			2019: {
				exceptional: 5,
				delicious: 19,
				liked: 23,
				okay: 13,
				dislike: 7
			}
		},
		extra_tags: ["Sorrow-Spiders", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "augusts-exceptional-story-required-repairs/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 55,
		title: "For All The Saints Who From Their Labours Rest",
		release_date: "2018/08/30",
		author: "James Chew",
		description: "Join an Intrepid Deacon on a deadly mission for the Church. Hunt for a missing saint, infiltrate the Brass Embassy and uncover the hidden history of Hell. What will you give up to learn the truth?",
		season: "Celebrations",
		story_rankings: {
			2024: {
				exceptional: 30,
				delicious: 17,
				liked: 9,
				okay: 1,
				dislike: 1
			},
			2023: {
				exceptional: 45,
				delicious: 22,
				liked: 15,
				okay: 4,
				dislike: 1
			},
			2022: {
				exceptional: 51,
				delicious: 26,
				liked: 22,
				okay: 6,
				dislike: 0
			},
			2021: {
				exceptional: 57,
				delicious: 32,
				liked: 17,
				okay: 5,
				dislike: 1
			},
			2020: {
				exceptional: 54,
				delicious: 33,
				liked: 15,
				okay: 5,
				dislike: 0
			},
			2019: {
				exceptional: 49,
				delicious: 15,
				liked: 4,
				okay: 3,
				dislike: 0
			}
		},
		official_tags: ["Hell", "The Church"],
		extra_tags: ["Bishop of Southwark", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "septembers-exceptional-story-for-all-the-saints/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 56,
		title: "The Magician's Dream",
		release_date: "2018/09/27",
		author: "Mary Goodden",
		description: "The magicians of the Glass are holding their annual masque, and you are invited. Don a mask, attend the ball, and uncover the secrets that plague the society's most promising magician.",
		season: "Celebrations",
		story_rankings: {
			2024: {
				exceptional: 1,
				delicious: 7,
				liked: 11,
				okay: 7,
				dislike: 3
			},
			2023: {
				exceptional: 4,
				delicious: 8,
				liked: 21,
				okay: 9,
				dislike: 2
			},
			2022: {
				exceptional: 2,
				delicious: 16,
				liked: 22,
				okay: 10,
				dislike: 4
			},
			2021: {
				exceptional: 7,
				delicious: 23,
				liked: 27,
				okay: 20,
				dislike: 1
			},
			2020: {
				exceptional: 8,
				delicious: 27,
				liked: 34,
				okay: 11,
				dislike: 5
			},
			2019: {
				exceptional: 7,
				delicious: 32,
				liked: 28,
				okay: 9,
				dislike: 1
			}
		},
		official_tags: ["Parabola", "The Glass"],
		extra_tags: ["Fingerkings", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "octobers-exceptional-story-the-magicians-dream/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 57,
		title: "A Little Pandemonium",
		release_date: "2018/10/25",
		author: "Jack de Quidt",
		description: "The Ministry of Public Decency are resurrecting an old London tradition: Bonfire Night. But unsanctioned fireworks and effigies are circulating in the crowd. Investigate incendiary goings-on among the bonfires and revelers. Will you aid the troublemakers, or the constables? What harm, a little pandemonium?",
		season: "Celebrations",
		story_rankings: {
			2024: {
				exceptional: 8,
				delicious: 12,
				liked: 12,
				okay: 3,
				dislike: 2
			},
			2023: {
				exceptional: 10,
				delicious: 17,
				liked: 13,
				okay: 6,
				dislike: 1
			},
			2022: {
				exceptional: 20,
				delicious: 18,
				liked: 18,
				okay: 10,
				dislike: 1
			},
			2021: {
				exceptional: 23,
				delicious: 29,
				liked: 18,
				okay: 12,
				dislike: 2
			},
			2020: {
				exceptional: 17,
				delicious: 34,
				liked: 26,
				okay: 10,
				dislike: 1
			},
			2019: {
				exceptional: 17,
				delicious: 40,
				liked: 16,
				okay: 3,
				dislike: 1
			}
		},
		official_tags: ["Revolutionaries"],
		extra_tags: ["Exceptional Story"],
		spoiler_tags: ["Calendar Council"],
		lore_tags: [],
		threads: {
			failbetter: "novembers-exceptional-story-a-little-pandemonium/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 58,
		title: "Daylight",
		release_date: "2018/11/29",
		author: "Ash McAllan",
		description: "Join forces with a zailor and young rat to uncover the secrets of an abandoned model village. Journey to an island in the middle of the unterzee to uncover the painful legacy of an inventor's folly and confront the horror that lingers there still.",
		season: "Hobbies",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 6,
				liked: 7,
				okay: 16,
				dislike: 8
			},
			2023: {
				exceptional: 3,
				delicious: 10,
				liked: 16,
				okay: 14,
				dislike: 10
			},
			2022: {
				exceptional: 6,
				delicious: 15,
				liked: 27,
				okay: 12,
				dislike: 15
			},
			2021: {
				exceptional: 9,
				delicious: 16,
				liked: 24,
				okay: 18,
				dislike: 18
			},
			2020: {
				exceptional: 6,
				delicious: 19,
				liked: 28,
				okay: 29,
				dislike: 18
			},
			2019: {
				exceptional: 6,
				delicious: 14,
				liked: 32,
				okay: 16,
				dislike: 18
			}
		},
		extra_tags: ["Parabola", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "decembers-exceptional-story-daylight/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 59,
		title: "The Ceremony",
		release_date: "2018/12/19",
		author: "Cassandra Khaw",
		description: "A spate of exquisite and unpleasant transformations sweeps London: new Rubbery enhancements of a drastic degree. What lies behind the tentacles? What secrets are hidden within the spiraling bones? Who is the Gilded Sacrifice?",
		story_rankings: {
			2024: {
				exceptional: 12,
				delicious: 17,
				liked: 9,
				okay: 4,
				dislike: 2
			},
			2023: {
				exceptional: 29,
				delicious: 19,
				liked: 15,
				okay: 7,
				dislike: 1
			},
			2022: {
				exceptional: 47,
				delicious: 25,
				liked: 11,
				okay: 9,
				dislike: 4
			},
			2021: {
				exceptional: 53,
				delicious: 27,
				liked: 11,
				okay: 8,
				dislike: 3
			},
			2020: {
				exceptional: 59,
				delicious: 20,
				liked: 11,
				okay: 8,
				dislike: 1
			},
			2019: {
				exceptional: 45,
				delicious: 16,
				liked: 6,
				okay: 0,
				dislike: 2
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 11,
				not_worth_it: 38
			},
			2023: {
				worth_it: 21,
				not_worth_it: 48
			},
			2022: {
				worth_it: 33,
				not_worth_it: 68
			},
			2021: {
				worth_it: 44,
				not_worth_it: 88
			},
			2020: {
				worth_it: 70,
				not_worth_it: 28
			},
			2019: {
				worth_it: 61,
				not_worth_it: 15
			}
		},
		official_tags: ["Rubberies", "Bohemians"],
		extra_tags: ["Festive Tale", "Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "a-new-premium-festive-tale-the-ceremony/"
		},
		unlock_fate: 35,
		reset_fate: 30
	}, {
		id: 60,
		title: "The Price of Loss",
		release_date: "2018/12/27",
		author: "Kevin Snow",
		description: "Enter a tent where the lost, the grieving and the wounded gather. Share their stories, enter their dreams, and reveal the truth behind their healing rituals. Is the medicine really better than the cure?",
		season: "Hobbies",
		story_rankings: {
			2024: {
				exceptional: 0,
				delicious: 1,
				liked: 10,
				okay: 9,
				dislike: 5
			},
			2023: {
				exceptional: 1,
				delicious: 7,
				liked: 12,
				okay: 12,
				dislike: 10
			},
			2022: {
				exceptional: 0,
				delicious: 5,
				liked: 13,
				okay: 21,
				dislike: 12
			},
			2021: {
				exceptional: 4,
				delicious: 10,
				liked: 31,
				okay: 16,
				dislike: 16
			},
			2020: {
				exceptional: 4,
				delicious: 16,
				liked: 24,
				okay: 33,
				dislike: 20
			},
			2019: {
				exceptional: 8,
				delicious: 12,
				liked: 25,
				okay: 21,
				dislike: 12
			}
		},
		official_tags: ["Society", "Parabola"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "januarys-exceptional-story-the-price-of-loss/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 61,
		title: "Cricket, Anyone?",
		release_date: "2019/01/31",
		author: "Chandler Groover",
		description: "The Tournament of Toasts! The greatest cricket match of the season! And the perfect opportunity for Benthic and Summerset College to tear each other to pieces! Grab a bat, grab a drink, but keep your wits sharp: the stakes are higher than they seem.",
		season: "Hobbies",
		story_rankings: {
			2024: {
				exceptional: 85,
				delicious: 29,
				liked: 10,
				okay: 2,
				dislike: 2
			},
			2023: {
				exceptional: 89,
				delicious: 13,
				liked: 9,
				okay: 1,
				dislike: 8
			},
			2022: {
				exceptional: 140,
				delicious: 27,
				liked: 8,
				okay: 2,
				dislike: 1
			},
			2021: {
				exceptional: 152,
				delicious: 19,
				liked: 3,
				okay: 0,
				dislike: 2
			},
			2020: {
				exceptional: 110,
				delicious: 13,
				liked: 5,
				okay: 0,
				dislike: 1
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 22,
				not_worth_it: 98
			},
			2023: {
				worth_it: 32,
				not_worth_it: 75
			},
			2022: {
				worth_it: 65,
				not_worth_it: 104
			},
			2021: {
				worth_it: 76,
				not_worth_it: 89
			},
			2020: {
				worth_it: 68,
				not_worth_it: 53
			}
		},
		official_tags: ["The University", "Hell"],
		extra_tags: ["Sports", "Masters", "The Great Game", "Exceptional Story", "Deep Lore"],
		spoiler_tags: ["Mr Wines", "The Correspondence", "Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "februarys-exceptional-story-cricket-anyone/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 62,
		title: "Noises from Upstairs",
		release_date: "2019/02/28",
		author: "Gavin Inglis",
		description: "Accompany a jaded supernatural investigator on his final case. Journey to a remote peninsula and explore a clifftop haunted house. Is this another unscrupulous fake? Or do you need to see things from a different perspective?",
		season: "Explorations",
		story_rankings: {
			2024: {
				exceptional: 3,
				delicious: 10,
				liked: 18,
				okay: 4,
				dislike: 0
			},
			2023: {
				exceptional: 9,
				delicious: 21,
				liked: 26,
				okay: 6,
				dislike: 0
			},
			2022: {
				exceptional: 14,
				delicious: 33,
				liked: 28,
				okay: 10,
				dislike: 1
			},
			2021: {
				exceptional: 17,
				delicious: 28,
				liked: 40,
				okay: 11,
				dislike: 3
			},
			2020: {
				exceptional: 8,
				delicious: 36,
				liked: 50,
				okay: 16,
				dislike: 3
			}
		},
		official_tags: ["Criminals"],
		extra_tags: ["Exceptional Story"],
		spoiler_tags: ["Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "march-exceptional-story-noises-from-upstairs/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 63,
		title: "Tauroktonos",
		release_date: "2019/03/28",
		author: "Harry Tuffs",
		description: "An ailing professor uncovers a secret beneath a butcher's shop: a forgotten Mithraic temple. Delve into its mysteries, open its great brass door, and lead an expedition of troubled souls along the Devilbone Road.",
		season: "Explorations",
		story_rankings: {
			2024: {
				exceptional: 7,
				delicious: 23,
				liked: 11,
				okay: 4,
				dislike: 0
			},
			2023: {
				exceptional: 17,
				delicious: 28,
				liked: 23,
				okay: 7,
				dislike: 1
			},
			2022: {
				exceptional: 22,
				delicious: 25,
				liked: 20,
				okay: 7,
				dislike: 2
			},
			2021: {
				exceptional: 24,
				delicious: 28,
				liked: 31,
				okay: 4,
				dislike: 3
			},
			2020: {
				exceptional: 19,
				delicious: 46,
				liked: 29,
				okay: 13,
				dislike: 2
			}
		},
		official_tags: ["Hell"],
		extra_tags: ["Bishop of Southwark", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "april-exceptional-story-tauroktonos/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 64,
		title: "The Stag and the Shark",
		release_date: "2019/04/25",
		author: "Mary Goodden",
		description: "A Young Stag has grown weary of quaffing brandy and stealing trousers. He wants to do something spectacular for his next lark \u2013 and he's looking for a suitable chum to join him! Steer his ship on a perilous shark-watching voyage. Will he return the same man as he left?",
		season: "Explorations",
		story_rankings: {
			2024: {
				exceptional: 4,
				delicious: 3,
				liked: 16,
				okay: 10,
				dislike: 3
			},
			2023: {
				exceptional: 3,
				delicious: 10,
				liked: 25,
				okay: 14,
				dislike: 3
			},
			2022: {
				exceptional: 1,
				delicious: 22,
				liked: 34,
				okay: 15,
				dislike: 4
			},
			2021: {
				exceptional: 6,
				delicious: 25,
				liked: 32,
				okay: 21,
				dislike: 4
			},
			2020: {
				exceptional: 5,
				delicious: 21,
				liked: 50,
				okay: 29,
				dislike: 9
			}
		},
		official_tags: ["Society", "The Admiralty"],
		extra_tags: ["New Sequence", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "may-exceptional-story-the-stag-and-the-shark/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 65,
		title: "The Garden Embassy",
		release_date: "2019/05/30",
		author: "Ash McAllan",
		description: "The Decorated Ambassador arrives in London, determined to put on an unforgettable evening. For one night only, the Garden Embassy is hosting a ball. It is essential London makes the right impression. Will you rise to the occasion?",
		season: "Bargains",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 13,
				liked: 13,
				okay: 11,
				dislike: 4
			},
			2023: {
				exceptional: 5,
				delicious: 12,
				liked: 25,
				okay: 16,
				dislike: 5
			},
			2022: {
				exceptional: 4,
				delicious: 18,
				liked: 27,
				okay: 13,
				dislike: 12
			},
			2021: {
				exceptional: 8,
				delicious: 27,
				liked: 32,
				okay: 25,
				dislike: 10
			},
			2020: {
				exceptional: 10,
				delicious: 30,
				liked: 42,
				okay: 30,
				dislike: 6
			}
		},
		official_tags: ["Society", "The Surface"],
		extra_tags: ["Revolutionaries", "Exceptional Story", "Sunless Sea"],
		lore_tags: [],
		threads: {
			failbetter: "june-exceptional-story-the-garden-embassy/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 66,
		title: "My Kingdom for a Pig",
		release_date: "2019/06/27",
		author: "Chandler Groover",
		description: "A pox has spread across London! Join a crack team of conspirators to infiltrate a high-stakes auction and delve beneath a fungal vineyard in search of a cure. What will you find in the Fifth City\u2019s deepest cellars?",
		season: "Bargains",
		story_rankings: {
			2024: {
				exceptional: 49,
				delicious: 10,
				liked: 6,
				okay: 1,
				dislike: 2
			},
			2023: {
				exceptional: 69,
				delicious: 18,
				liked: 11,
				okay: 3,
				dislike: 3
			},
			2022: {
				exceptional: 90,
				delicious: 27,
				liked: 11,
				okay: 1,
				dislike: 0
			},
			2021: {
				exceptional: 93,
				delicious: 29,
				liked: 7,
				okay: 1,
				dislike: 1
			},
			2020: {
				exceptional: 85,
				delicious: 33,
				liked: 9,
				okay: 4,
				dislike: 3
			}
		},
		official_tags: ["The Bazaar", "The Gracious Widow"],
		extra_tags: ["First City", "Second City", "Third City", "Fourth City", "Fifth City", "Mushrooms", "Exceptional Story", "Deep Lore"],
		lore_tags: [],
		threads: {
			failbetter: "july-exceptional-story-my-kingdom-for-a-pig/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 67,
		title: "The Shallows",
		release_date: "2019/07/25",
		author: "Gavin Inglis",
		description: "A sensitive Ministry operation gone wrong. A secretive Revolutionary cell blown open. Journey to a farther shore to discover a secret lost beyond retrieve? Will you take up the Boatman's oar?",
		season: "Bargains",
		story_rankings: {
			2024: {
				exceptional: 28,
				delicious: 25,
				liked: 7,
				okay: 2,
				dislike: 0
			},
			2023: {
				exceptional: 47,
				delicious: 36,
				liked: 15,
				okay: 2,
				dislike: 0
			},
			2022: {
				exceptional: 59,
				delicious: 42,
				liked: 23,
				okay: 7,
				dislike: 0
			},
			2021: {
				exceptional: 64,
				delicious: 41,
				liked: 17,
				okay: 5,
				dislike: 2
			},
			2020: {
				exceptional: 60,
				delicious: 39,
				liked: 17,
				okay: 7,
				dislike: 1
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 18,
				not_worth_it: 43
			},
			2023: {
				worth_it: 37,
				not_worth_it: 56
			},
			2022: {
				worth_it: 53,
				not_worth_it: 76
			},
			2021: {
				worth_it: 64,
				not_worth_it: 61
			},
			2020: {
				worth_it: 62,
				not_worth_it: 57
			}
		},
		official_tags: ["Revolutionaries"],
		extra_tags: ["The Boatman", "Exceptional Story"],
		spoiler_tags: ["Mr Mirrors", "Calendar Council", "Masters", "Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "august-exceptional-story-the-shallows/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 68,
		title: "The Heretic of Hollow Street",
		release_date: "2019/08/29",
		author: "Harry Tuffs",
		description: "A taciturn barber in Veilgarden offers to cut more than just his patrons' hair. Where did he get his comb and scissors from? Are you brave enough to sit in the Barber of Hollow Street's chair?",
		season: "Propinquity",
		story_rankings: {
			2024: {
				exceptional: 5,
				delicious: 15,
				liked: 22,
				okay: 5,
				dislike: 1
			},
			2023: {
				exceptional: 6,
				delicious: 23,
				liked: 21,
				okay: 6,
				dislike: 0
			},
			2022: {
				exceptional: 8,
				delicious: 23,
				liked: 32,
				okay: 8,
				dislike: 3
			},
			2021: {
				exceptional: 15,
				delicious: 33,
				liked: 26,
				okay: 16,
				dislike: 6
			},
			2020: {
				exceptional: 10,
				delicious: 42,
				liked: 42,
				okay: 11,
				dislike: 5
			}
		},
		official_tags: ["Society", "Parabola"],
		extra_tags: ["Fingerkings", "Exceptional Story"],
		spoiler_tags: ["The Bloody-Handed Queen"],
		lore_tags: [],
		threads: {
			failbetter: "september--the-heretic-of-hollow-street/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 69,
		title: "Say It With Flowers",
		release_date: "2019/09/26",
		author: "Olivia Wood",
		description: "London is awash with fresh flowers! Is it a passing floral fad, or does something rotten lurk beneath the petals? Intercept coded bouquets, spy on spies, and investigate intrigue along the Cumaean Canal.",
		season: "Propinquity",
		story_rankings: {
			2024: {
				exceptional: 4,
				delicious: 8,
				liked: 18,
				okay: 3,
				dislike: 1
			},
			2023: {
				exceptional: 5,
				delicious: 23,
				liked: 20,
				okay: 8,
				dislike: 4
			},
			2022: {
				exceptional: 6,
				delicious: 26,
				liked: 21,
				okay: 21,
				dislike: 4
			},
			2021: {
				exceptional: 9,
				delicious: 29,
				liked: 43,
				okay: 13,
				dislike: 4
			},
			2020: {
				exceptional: 17,
				delicious: 40,
				liked: 52,
				okay: 9,
				dislike: 6
			}
		},
		official_tags: ["Society", "The Great Game", "The Palace"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "october-exceptional-story-say-it-with-flowers/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 70,
		title: "The Committee",
		release_date: "2019/10/31",
		author: "Gavin Inglis",
		description: "Something is rotten in the state of London. An old man refuses to go gracefully to the Tomb-Colonies. He claims to have unfinished business. Delve into secrets best left buried and uncover the last regret of a once brilliant servant.",
		season: "Propinquity",
		story_rankings: {
			2024: {
				exceptional: 0,
				delicious: 5,
				liked: 9,
				okay: 8,
				dislike: 6
			},
			2023: {
				exceptional: 0,
				delicious: 5,
				liked: 17,
				okay: 18,
				dislike: 6
			},
			2022: {
				exceptional: 3,
				delicious: 14,
				liked: 21,
				okay: 21,
				dislike: 8
			},
			2021: {
				exceptional: 2,
				delicious: 19,
				liked: 28,
				okay: 29,
				dislike: 11
			},
			2020: {
				exceptional: 9,
				delicious: 32,
				liked: 43,
				okay: 22,
				dislike: 11
			}
		},
		official_tags: ["Society", "The Bazaar"],
		extra_tags: ["Fifth City", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "november-exceptional-story-the-committee/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 71,
		title: "Por Una Cabeza",
		release_date: "2019/11/28",
		author: "Chandler Groover",
		description: "You are invited to join the Mollusca Tour de London! Intrigue, dirty tricks and all manner of slugs abound! Who will win \u2013 who will cheat \u2013 and what will be the cost for racer and gambler alike?",
		season: "Animals",
		story_rankings: {
			2024: {
				exceptional: 45,
				delicious: 34,
				liked: 8,
				okay: 3,
				dislike: 1
			},
			2023: {
				exceptional: 49,
				delicious: 20,
				liked: 13,
				okay: 2,
				dislike: 2
			},
			2022: {
				exceptional: 64,
				delicious: 26,
				liked: 10,
				okay: 3,
				dislike: 3
			},
			2021: {
				exceptional: 76,
				delicious: 33,
				liked: 16,
				okay: 0,
				dislike: 2
			},
			2020: {
				exceptional: 68,
				delicious: 41,
				liked: 16,
				okay: 3,
				dislike: 1
			}
		},
		official_tags: ["Hell", "Bohemians"],
		extra_tags: ["Sports", "Elder Continent", "Exceptional Story"],
		spoiler_tags: ["Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "december-exceptional-story-por-una-cabeza/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 72,
		title: "The Brass Grail",
		release_date: "2019/12/18",
		author: "Nigel Evans",
		description: "Scandal in London as the Bishop of Southwark is summoned to court. Old sins weigh heavily upon the mitred head. Can one ever truly escape the shadow of Hell?",
		story_rankings: {
			2024: {
				exceptional: 25,
				delicious: 26,
				liked: 10,
				okay: 2,
				dislike: 0
			},
			2023: {
				exceptional: 38,
				delicious: 23,
				liked: 20,
				okay: 4,
				dislike: 1
			},
			2022: {
				exceptional: 56,
				delicious: 37,
				liked: 17,
				okay: 5,
				dislike: 2
			},
			2021: {
				exceptional: 49,
				delicious: 28,
				liked: 18,
				okay: 8,
				dislike: 0
			},
			2020: {
				exceptional: 33,
				delicious: 32,
				liked: 19,
				okay: 3,
				dislike: 1
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 23,
				not_worth_it: 36
			},
			2023: {
				worth_it: 35,
				not_worth_it: 49
			},
			2022: {
				worth_it: 49,
				not_worth_it: 69
			},
			2021: {
				worth_it: 36,
				not_worth_it: 67
			},
			2020: {
				worth_it: 28,
				not_worth_it: 59
			}
		},
		official_tags: ["Hell", "The Church"],
		extra_tags: ["Bishop of Southwark", "Festive Tale", "Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "premium-story-the-brass-grail/"
		},
		unlock_fate: 35,
		reset_fate: 30
	}, {
		id: 73,
		title: "Fine Dining",
		release_date: "2019/12/27",
		author: "Jack de Quidt",
		description: "It's opening night at Dolly's, one of London's most exclusive restaurants. The critics are in, pens sharpened to make mincemeat of the new set menu. Tempers will fray; pots will boil. Can you survive a night in the kitchen? You'll have to wait for the reviews to come in...",
		season: "Animals",
		story_rankings: {
			2024: {
				exceptional: 12,
				delicious: 13,
				liked: 7,
				okay: 4,
				dislike: 9
			},
			2023: {
				exceptional: 14,
				delicious: 17,
				liked: 13,
				okay: 8,
				dislike: 14
			},
			2022: {
				exceptional: 16,
				delicious: 15,
				liked: 24,
				okay: 11,
				dislike: 28
			},
			2021: {
				exceptional: 20,
				delicious: 25,
				liked: 30,
				okay: 19,
				dislike: 35
			},
			2020: {
				exceptional: 18,
				delicious: 29,
				liked: 28,
				okay: 16,
				dislike: 29
			}
		},
		official_tags: ["Society", "Tigers"],
		extra_tags: ["Food", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "january-exceptional-story-fine-dining/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 74,
		title: "Borrowed Glory",
		release_date: "2020/01/30",
		author: "Cassandra Khaw",
		description: "London is awash with dreams as a carefree tiger-prince comes to visit. But is there more to his appetites than his guests imagine? Will you capture his attentions? And, more importantly, should you wish to?",
		season: "Animals",
		story_rankings: {
			2024: {
				exceptional: 3,
				delicious: 5,
				liked: 15,
				okay: 13,
				dislike: 3
			},
			2023: {
				exceptional: 1,
				delicious: 9,
				liked: 24,
				okay: 20,
				dislike: 3
			},
			2022: {
				exceptional: 3,
				delicious: 15,
				liked: 37,
				okay: 20,
				dislike: 10
			},
			2021: {
				exceptional: 7,
				delicious: 20,
				liked: 44,
				okay: 27,
				dislike: 19
			}
		},
		official_tags: ["Parabola", "Tigers"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "februarys-exceptional-story-borrowed-glory/",
			reddit: "ewfbx9/februarys_exceptional_story_borrowed_glory/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 75,
		title: "Shades of Yesterday",
		release_date: "2020/02/27",
		author: "Gavin Inglis",
		description: "Artists and the literati rejoice \u2014 a pen show visits London! Nibs as numerous as a battalion\u2019s bayonets! Inks, in all the wild colours of the Neath! Try them to your hearts content! What\u2019s that? Rumours of stolen memories? Utter nonsense. Come, and look at all the lovely pens.",
		season: "Endeavour",
		story_rankings: {
			2024: {
				exceptional: 0,
				delicious: 7,
				liked: 15,
				okay: 13,
				dislike: 2
			},
			2023: {
				exceptional: 0,
				delicious: 10,
				liked: 31,
				okay: 16,
				dislike: 2
			},
			2022: {
				exceptional: 5,
				delicious: 20,
				liked: 33,
				okay: 27,
				dislike: 4
			},
			2021: {
				exceptional: 12,
				delicious: 26,
				liked: 45,
				okay: 34,
				dislike: 4
			}
		},
		official_tags: ["Bohemians"],
		extra_tags: ["Exceptional Story"],
		spoiler_tags: ["Neathbow"],
		lore_tags: [],
		threads: {
			failbetter: "march-exceptional-story-shades-of-yesterday/",
			reddit: "fadc2p/marchs_exceptional_story_shades_of_yesterday/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 76,
		title: "Go Tell the King of Cats",
		release_date: "2020/03/26",
		author: "James Chew",
		description: "A difficult tabby cat has made his home in your lodgings. He insists that only you can help him get what he wants \u2013 a return to his former glory. He has a long journey in mind, one that will take him across the kingdoms of cats in search of their mythical King.",
		season: "Endeavour",
		story_rankings: {
			2024: {
				exceptional: 24,
				delicious: 19,
				liked: 12,
				okay: 2,
				dislike: 1
			},
			2023: {
				exceptional: 47,
				delicious: 29,
				liked: 16,
				okay: 3,
				dislike: 1
			},
			2022: {
				exceptional: 62,
				delicious: 39,
				liked: 22,
				okay: 13,
				dislike: 0
			},
			2021: {
				exceptional: 74,
				delicious: 55,
				liked: 28,
				okay: 14,
				dislike: 2
			}
		},
		official_tags: ["Cats", "Tigers", "Parabola", "The Duchess"],
		extra_tags: ["Exceptional Story"],
		spoiler_tags: ["Fingerkings"],
		lore_tags: [],
		threads: {
			failbetter: "april-exceptional-story-go-tell-the-king-of-cats/",
			reddit: "fuak21/aprilss_exceptional_story_go_tell_the_king_of/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 77,
		title: "The Ballad of Johnny Croak",
		release_date: "2020/04/30",
		author: "Harry Tuffs",
		description: "A string of horrible murders strikes London, made all the more horrid by their permanence. Who is Johnny Croak, the legendary urchin with the lethal crossbow? What\u2019s his connection to Mr Fires' factories? And what's with all the frogs?",
		season: "Endeavour",
		story_rankings: {
			2024: {
				exceptional: 18,
				delicious: 16,
				liked: 23,
				okay: 5,
				dislike: 0
			},
			2023: {
				exceptional: 28,
				delicious: 27,
				liked: 20,
				okay: 6,
				dislike: 2
			},
			2022: {
				exceptional: 46,
				delicious: 40,
				liked: 24,
				okay: 13,
				dislike: 4
			},
			2021: {
				exceptional: 53,
				delicious: 57,
				liked: 38,
				okay: 19,
				dislike: 7
			}
		},
		official_tags: ["Urchins", "Masters"],
		extra_tags: ["Exceptional Story"],
		spoiler_tags: ["Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "mays-es-and-changes-to-efship/",
			reddit: "gavd9s/mays_exceptional_story_the_ballad_of_johnny_croak/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 78,
		title: "The Dilettante's Debut",
		release_date: "2020/05/28",
		author: "Hannah Powell-Smith",
		description: "The great and noble house of Fairfax has fallen into decay. Its last heir idles away his fortune on dark dreams. Will you restore them to glory? Or help bring about their final collapse?",
		story_rankings: {
			2024: {
				exceptional: 0,
				delicious: 6,
				liked: 14,
				okay: 15,
				dislike: 5
			},
			2023: {
				exceptional: 0,
				delicious: 5,
				liked: 25,
				okay: 25,
				dislike: 7
			},
			2022: {
				exceptional: 0,
				delicious: 25,
				liked: 40,
				okay: 43,
				dislike: 9
			},
			2021: {
				exceptional: 6,
				delicious: 33,
				liked: 67,
				okay: 56,
				dislike: 19
			}
		},
		official_tags: ["Society"],
		extra_tags: ["Exceptional Story"],
		spoiler_tags: ["Shapeling Arts"],
		lore_tags: [],
		threads: {
			failbetter: "junes-exceptional-story-the-dilettantes-debut/",
			reddit: "gs5wmn/junes_exceptional_story_the_dilettantes_debut/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 79,
		title: "Paisley",
		release_date: "2020/06/25",
		author: "Chandler Groover",
		description: "An aesthete has been murdered, and only his paisley outfit can identify the killer. Unfortunately, that outfit is on the lam. Track down the missing ensemble, conspire with textiles, and try to avoid a wardrobe malfunction.",
		story_rankings: {
			2024: {
				exceptional: 36,
				delicious: 49,
				liked: 13,
				okay: 1,
				dislike: 4
			},
			2023: {
				exceptional: 39,
				delicious: 41,
				liked: 14,
				okay: 6,
				dislike: 0
			},
			2022: {
				exceptional: 52,
				delicious: 48,
				liked: 32,
				okay: 8,
				dislike: 4
			},
			2021: {
				exceptional: 84,
				delicious: 74,
				liked: 39,
				okay: 12,
				dislike: 2
			}
		},
		official_tags: ["Society", "Polythreme", "Revolutionaries"],
		extra_tags: ["Exceptional Story"],
		spoiler_tags: ["Masters"],
		lore_tags: [],
		threads: {
			failbetter: "july-exceptional-story-paisley/",
			reddit: "hfmun4/julys_exceptional_story_paisley_by_chandler/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 80,
		title: "Unto Dust",
		release_date: "2020/07/30",
		author: "James Chew",
		description: "Caught in the machinations of a wily Tomb-Colonist, it's up to you to save a high society funeral from disaster! Can you navigate the demands of a corpse who won't stay dead, to say nothing of his confounded relatives... In Fallen London the dead don't stay buried and the living never rest easy.",
		story_rankings: {
			2024: {
				exceptional: 0,
				delicious: 6,
				liked: 15,
				okay: 12,
				dislike: 4
			},
			2023: {
				exceptional: 2,
				delicious: 16,
				liked: 34,
				okay: 15,
				dislike: 2
			},
			2022: {
				exceptional: 5,
				delicious: 31,
				liked: 44,
				okay: 31,
				dislike: 4
			},
			2021: {
				exceptional: 14,
				delicious: 66,
				liked: 68,
				okay: 30,
				dislike: 9
			}
		},
		official_tags: ["Society", "The Tomb-Colonies"],
		extra_tags: ["Law", "Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "august-exceptional-story-unto-dust/",
			reddit: "i0llvo/augusts_exceptional_story_unto_dust/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 81,
		title: "Homecoming",
		release_date: "2020/08/27",
		author: "Mary Goodden",
		description: "A spate of sunburn affects Londoners \u2013 an improbable affliction for a city underground. What can be causing it\u2013 and is there a connection to the newly opened Nostros Hotel and its very mysterious management?",
		story_rankings: {
			2024: {
				exceptional: 6,
				delicious: 15,
				liked: 12,
				okay: 5,
				dislike: 1
			},
			2023: {
				exceptional: 11,
				delicious: 28,
				liked: 27,
				okay: 8,
				dislike: 1
			},
			2022: {
				exceptional: 28,
				delicious: 48,
				liked: 41,
				okay: 14,
				dislike: 3
			},
			2021: {
				exceptional: 51,
				delicious: 66,
				liked: 42,
				okay: 31,
				dislike: 4
			}
		},
		official_tags: ["Masters", "Second City"],
		extra_tags: ["Exceptional Story"],
		spoiler_tags: ["Mr Wines", "Mr Spices"],
		lore_tags: [],
		threads: {
			failbetter: "september-exceptional-story-homecoming/",
			reddit: "ihs0vs/septembers_exceptional_story_homecoming/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 82,
		title: "Fading to a Coda",
		release_date: "2020/09/24",
		author: "Nigel Evans",
		description: "A notorious bar-room singer has made a surprise return to London. The Growling Radical makes dangerous enemies everywhere he goes \u2013 his latest exile was supposed to be his last. What has drawn him back to the Fifth City? Why has he fixed his eye on a Master of the Bazaar?",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 7,
				liked: 14,
				okay: 13,
				dislike: 10
			},
			2023: {
				exceptional: 5,
				delicious: 10,
				liked: 35,
				okay: 19,
				dislike: 9
			},
			2022: {
				exceptional: 8,
				delicious: 25,
				liked: 41,
				okay: 32,
				dislike: 30
			},
			2021: {
				exceptional: 18,
				delicious: 36,
				liked: 66,
				okay: 53,
				dislike: 26
			}
		},
		reward_rankings: {
			2021: {
				worth_it: 29,
				not_worth_it: 122
			}
		},
		extra_tags: ["Arts", "Exceptional Story", "Permanent Reward"],
		spoiler_tags: ["Mr Wines", "Masters"],
		lore_tags: [],
		threads: {
			failbetter: "october-exceptional-story-fading-to-a-coda/",
			reddit: "iz7scl/octobers_exceptional_story_fading_to_a_coda/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 83,
		title: "Older, Not Wiser",
		release_date: "2020/10/29",
		author: "Olivia Wood",
		description: "Terror hits London! A team of crack cat-burglars menaces the city by night, making off with all manner of valuables. The constables are baffled: no one can identify a single suspect. Who are these mystery criminals? And why do they leave behind a scent distinctly redolent of peach?",
		story_rankings: {
			2024: {
				exceptional: 5,
				delicious: 11,
				liked: 17,
				okay: 9,
				dislike: 1
			},
			2023: {
				exceptional: 6,
				delicious: 23,
				liked: 23,
				okay: 13,
				dislike: 6
			},
			2022: {
				exceptional: 20,
				delicious: 29,
				liked: 48,
				okay: 25,
				dislike: 14
			},
			2021: {
				exceptional: 27,
				delicious: 61,
				liked: 68,
				okay: 42,
				dislike: 8
			}
		},
		official_tags: ["Criminals", "The Gracious Widow"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "november-exceptional-story-older-not-wiser/",
			reddit: "jkgqed/novembers_exceptional_story_older_not_wiser/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 84,
		title: "Upwards!",
		release_date: "2020/11/24",
		description: "Inspired by the fever for palaeontology and armed with a doctorate on the ritual practices of the Starved Men, a Probationary Professor plans an expedition to the cavern roof. Will you join her? [Playing Upwards will unlock a permanent dig offering two unique osteology items that can be used in the Bone Market.]",
		story_rankings: {
			2024: {
				exceptional: 11,
				delicious: 16,
				liked: 24,
				okay: 17,
				dislike: 2
			},
			2023: {
				exceptional: 8,
				delicious: 39,
				liked: 38,
				okay: 15,
				dislike: 2
			},
			2022: {
				exceptional: 17,
				delicious: 45,
				liked: 45,
				okay: 23,
				dislike: 3
			},
			2021: {
				exceptional: 15,
				delicious: 31,
				liked: 40,
				okay: 16,
				dislike: 9
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 48,
				not_worth_it: 19
			},
			2023: {
				worth_it: 91,
				not_worth_it: 16
			},
			2022: {
				worth_it: 114,
				not_worth_it: 22
			},
			2021: {
				worth_it: 88,
				not_worth_it: 27
			}
		},
		extra_tags: ["Palaeontology", "Starved Men", "Permanent Reward", "The University"],
		lore_tags: [],
		threads: {
			reddit: "k03z7h/upwards/"
		},
		unlock_fate: 25,
	}, {
		id: 85,
		title: "The Icarian Cup",
		release_date: "2020/11/26",
		author: "Harry Tuffs",
		description: "The Icarian Cup returns to London! A daring boatrace around the Southern Archipelago, the prize a magnificent trophy (and a bounty of sapphires)! Pay no mind to the talk about the race being 'cursed' and 'ships disappearing' or 'the race was closed in 1868 after too many captains disappeared'. All that matters is that the Icarian Cup is back and in need of captains brave enough to run the race!",
		story_rankings: {
			2024: {
				exceptional: 21,
				delicious: 25,
				liked: 16,
				okay: 0,
				dislike: 1
			},
			2023: {
				exceptional: 45,
				delicious: 39,
				liked: 20,
				okay: 5,
				dislike: 3
			},
			2022: {
				exceptional: 66,
				delicious: 69,
				liked: 24,
				okay: 10,
				dislike: 3
			},
			2021: {
				exceptional: 114,
				delicious: 77,
				liked: 33,
				okay: 9,
				dislike: 3
			}
		},
		official_tags: ["Zailors", "The Docks"],
		extra_tags: ["Exceptional Story", "Sunless Sea"],
		lore_tags: [],
		threads: {
			failbetter: "december-exceptional-story-the-icarian-cup/",
			reddit: "k1f2jo/decembers_exceptional_story_the_icarian_cup/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 86,
		title: "Caveat Emptor",
		release_date: "2020/12/31",
		author: "Chandler Groover",
		description: "A notorious Vicomte is buying property across London. Now he has an eye on your address \u2013 and perhaps something just a little below your collar. Navigate the estate market, infiltrate hidden lairs, and contend with a truly blood-sucking landlord.",
		story_rankings: {
			2024: {
				exceptional: 24,
				delicious: 26,
				liked: 14,
				okay: 7,
				dislike: 1
			},
			2023: {
				exceptional: 48,
				delicious: 35,
				liked: 24,
				okay: 9,
				dislike: 0
			},
			2022: {
				exceptional: 72,
				delicious: 60,
				liked: 39,
				okay: 6,
				dislike: 2
			},
			2021: {
				exceptional: 96,
				delicious: 63,
				liked: 33,
				okay: 9,
				dislike: 2
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 27,
				not_worth_it: 40
			},
			2023: {
				worth_it: 64,
				not_worth_it: 43
			},
			2022: {
				worth_it: 99,
				not_worth_it: 75
			},
			2021: {
				worth_it: 105,
				not_worth_it: 84
			}
		},
		extra_tags: ["Exceptional Story", "Permanent Reward"],
		spoiler_tags: ["The Bazaar"],
		lore_tags: [],
		threads: {
			failbetter: "january-exceptional-story-caveat-emptor/",
			reddit: "knq946/januarys_exceptional_story_caveat_emptor/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 87,
		title: "The Fair Unknown",
		release_date: "2021/01/28",
		author: "James Chew",
		description: "A Menace Eradicator embarks on a quest to save his missing lover. Will you journey with him to the fabled Tournament of Rubies? Test your skill at arms against Parabola's most skilled combatants. Unmask the Mystery Knight. Perhaps even win the favour of the Bloody-Handed Queen. All is to play for in the Queen's tourney ground.",
		story_rankings: {
			2024: {
				exceptional: 14,
				delicious: 21,
				liked: 10,
				okay: 7,
				dislike: 1
			},
			2023: {
				exceptional: 27,
				delicious: 32,
				liked: 22,
				okay: 12,
				dislike: 0
			},
			2022: {
				exceptional: 51,
				delicious: 49,
				liked: 45,
				okay: 21,
				dislike: 4
			}
		},
		extra_tags: ["Exceptional Story", "Parabola", "Sports"],
		spoiler_tags: ["The Bloody-Handed Queen"],
		lore_tags: [],
		threads: {
			failbetter: "february-exceptional-story-the-fair-unknown/",
			reddit: "l77tc3/februarys_exceptional_story_the_fair_unknown/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 88,
		title: "The Thing That Came in From the Fog",
		release_date: "2021/02/25",
		author: "Harry Tuffs",
		description: "An uninvited visitor appears in the fog. He seeps into your lodgings and occupies the bathtub; he spills crumbs on your carpet and tea on your favourite chair. What does this fog-man want – and how can you get him to leave? And why is he so fixated on chess?",
		story_rankings: {
			2024: {
				exceptional: 3,
				delicious: 15,
				liked: 22,
				okay: 9,
				dislike: 2
			},
			2023: {
				exceptional: 16,
				delicious: 34,
				liked: 35,
				okay: 12,
				dislike: 1
			},
			2022: {
				exceptional: 32,
				delicious: 63,
				liked: 56,
				okay: 30,
				dislike: 1
			}
		},
		extra_tags: ["Exceptional Story", "Elder Continent", "Animescence"],
		lore_tags: [],
		threads: {
			failbetter: "march-es-the-thing-that-came-in-from-the-fog/",
			reddit: "lsefvt/marchs_exceptional_story_the_thing_that_came_in/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 89,
		title: "For a Dream of Innocence",
		release_date: "2021/03/25",
		author: "Nigel Evans",
		description: "A Vatican spy descends to London, leaving chaos in her wake. Why has she surrounded herself with a congregation of adoring Rubbery Men? To what uncertain end does she drive them? And what lies dreaming in the vat she's left behind, waiting to emerge...",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 6,
				liked: 20,
				okay: 12,
				dislike: 8
			},
			2023: {
				exceptional: 8,
				delicious: 15,
				liked: 31,
				okay: 21,
				dislike: 10
			},
			2022: {
				exceptional: 15,
				delicious: 34,
				liked: 54,
				okay: 44,
				dislike: 21
			}
		},
		extra_tags: ["Exceptional Story", "The Great Game", "Rubberies"],
		spoiler_tags: ["Shapeling Arts"],
		lore_tags: [],
		threads: {
			failbetter: "april-exceptional-story-for-a-dream-of-innocence/",
			reddit: "md0veg/aprils_exceptional_story_for_a_dream_of_innocence/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 90,
		title: "Leviathan",
		release_date: "2021/04/29",
		author: "Mary Goodden",
		description: "London's fad for fossils has drawn the Carpenter's Granddaughter to the Neath. Her search for the truth behind her mother's legacy will draw her deeper still: to the Gant Pole where great beasts dwell and die. Will you support her expedition? Can you avoid the attentions of the fell haruspices who reside there? And will you return the same as you were?",
		story_rankings: {
			2024: {
				exceptional: 10,
				delicious: 22,
				liked: 26,
				okay: 7,
				dislike: 0
			},
			2023: {
				exceptional: 19,
				delicious: 31,
				liked: 43,
				okay: 7,
				dislike: 4
			},
			2022: {
				exceptional: 38,
				delicious: 57,
				liked: 51,
				okay: 33,
				dislike: 9
			}
		},
		official_tags: ["Unterzee"],
		extra_tags: ["Exceptional Story", "Palaeontology", "Zailors"],
		spoiler_tags: ["Red Science"],
		lore_tags: [],
		threads: {
			failbetter: "exceptional-story-for-may-leviathan/",
			reddit: "n12jyn/mays_exceptional_story_leviathan/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 91,
		title: "Reunion",
		release_date: "2021/05/27",
		author: "James Chew",
		description: "An Evasive Aristocrat has returned to London in search of his old family. Having fatally misplaced his bodyguard, he's in need of a new escort through London's underworld. But how much of his past does he really remember? Why does he seek it in Red Honey? Will he remember who he is before he returns to the Surface - if he returns at all?",
		story_rankings: {
			2024: {
				exceptional: 10,
				delicious: 18,
				liked: 20,
				okay: 7,
				dislike: 1
			},
			2023: {
				exceptional: 23,
				delicious: 25,
				liked: 33,
				okay: 11,
				dislike: 0
			},
			2022: {
				exceptional: 36,
				delicious: 60,
				liked: 50,
				okay: 25,
				dislike: 4
			}
		},
		official_tags: ["Society", "The Palace"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "june-exceptional-story-reunion/",
			reddit: "nm5wgr/junes_exceptional_story_reunion/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 92,
		title: "We Absolutely Meant to Go to Zee",
		release_date: "2021/06/24",
		author: "Olivia Wood",
		description: "A nautical upset leaves you adrift with three spirited children in an unzeeworthy boat. Explore forgotten islets. Visit the pirate haven of Gaider's Mourn. And for goodness' sake, get them home in time for tea.",
		story_rankings: {
			2024: {
				exceptional: 29,
				delicious: 26,
				liked: 11,
				okay: 8,
				dislike: 2
			},
			2023: {
				exceptional: 42,
				delicious: 39,
				liked: 24,
				okay: 16,
				dislike: 2
			},
			2022: {
				exceptional: 92,
				delicious: 64,
				liked: 47,
				okay: 14,
				dislike: 4
			}
		},
		official_tags: ["Unterzee"],
		extra_tags: ["Exceptional Story", "The Docks", "Drownies", "Sunless Sea"],
		lore_tags: [],
		threads: {
			failbetter: "july-excpt-story-we-absolutely-meant-to-go-to-zee/",
			reddit: "o70t6y/julys_exceptional_story_we_absolutely_meant_to_go/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 93,
		title: "The Tempest",
		release_date: "2021/07/29",
		author: "Mary Goodden",
		description: "Weather? In London? A ferocious storm has come in from the zee. Join a crowd of storm chasers to examine this rare phenomenon. Consider the omens in the forks of lightning. Ignore the small child who persists in kicking your ankles.",
		story_rankings: {
			2024: {
				exceptional: 11,
				delicious: 18,
				liked: 26,
				okay: 6,
				dislike: 1
			},
			2023: {
				exceptional: 20,
				delicious: 34,
				liked: 35,
				okay: 15,
				dislike: 2
			},
			2022: {
				exceptional: 49,
				delicious: 66,
				liked: 72,
				okay: 19,
				dislike: 8
			}
		},
		official_tags: ["Urchins"],
		extra_tags: ["Exceptional Story"],
		spoiler_tags: ["Storm"],
		lore_tags: [],
		threads: {
			failbetter: "augusts-exceptional-story-the-tempest/",
			reddit: "otx3dt/augusts_exceptional_story_the_tempest_official/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 94,
		title: "Damp Martyrs",
		release_date: "2021/08/26",
		author: "Gavin Inglis",
		description: "A wealthy heiress has disappeared! Her family suspect kidnap, and blame the Circumcellion Brotherhood: a nautical band of martyr-marauders. Join a naval task force to uncover the truth, and learn whether salvation can be found on the cold Unterzee.",
		story_rankings: {
			2024: {
				exceptional: 1,
				delicious: 7,
				liked: 28,
				okay: 22,
				dislike: 7
			},
			2023: {
				exceptional: 1,
				delicious: 18,
				liked: 37,
				okay: 33,
				dislike: 16
			},
			2022: {
				exceptional: 12,
				delicious: 35,
				liked: 78,
				okay: 60,
				dislike: 23
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 10,
				not_worth_it: 56
			},
			2022: {
				worth_it: 16,
				not_worth_it: 165
			}
		},
		official_tags: ["Society", "Unterzee"],
		extra_tags: ["Exceptional Story", "Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "september-exceptional-story-damp-martyrs/",
			reddit: "pc7il7/septembers_exceptional_story_damp_martyrs/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 95,
		title: "A Crown of Thorns",
		release_date: "2021/09/30",
		author: "Mary Goodden",
		description: "The Thorned Manservant returned from the War against Hell forever changed. Now he has found mysterious employment at the Shuttered Palace. Satiate your curiosity, infiltrate the palace, and uncover his connection with a certain secluded prince...",
		story_rankings: {
			2024: {
				exceptional: 5,
				delicious: 18,
				liked: 27,
				okay: 12,
				dislike: 0
			},
			2023: {
				exceptional: 13,
				delicious: 36,
				liked: 40,
				okay: 16,
				dislike: 3
			},
			2022: {
				exceptional: 27,
				delicious: 83,
				liked: 76,
				okay: 18,
				dislike: 6
			}
		},
		official_tags: ["Royal Family"],
		extra_tags: ["Exceptional Story", "The Palace"],
		lore_tags: [],
		threads: {
			failbetter: "october-exceptional-story-a-crown-of-thorns/",
			reddit: "pyj24m/octobers_exceptional_story_a_crown_of_thorns/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 96,
		title: "The Crocodile Who Would Be King",
		release_date: "2021/10/28",
		author: "Chandler Groover",
		description: "An alligator in the sewers? Nonsense! It's clearly a crocodile, and Mr Inch is offering a bounty to anyone who can capture it: dead or alive. Embark upon a hunt beneath the city streets, confront what's really lurking in the waterways, and try not to lose any body parts you've become overly accustomed to.",
		story_rankings: {
			2024: {
				exceptional: 12,
				delicious: 36,
				liked: 33,
				okay: 7,
				dislike: 4
			},
			2023: {
				exceptional: 35,
				delicious: 44,
				liked: 33,
				okay: 12,
				dislike: 9
			},
			2022: {
				exceptional: 62,
				delicious: 92,
				liked: 59,
				okay: 27,
				dislike: 10
			}
		},
		official_tags: ["Parabola"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "november-es-the-crocodile-who-would-be-king/",
			reddit: "qhnjf1/novembers_exceptional_story_the_crocodile_who/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 97,
		title: "The House of Silk and Flame",
		release_date: "2021/11/25",
		author: "James Chew",
		description: "Midnight visitors are common in London, but visiting royalty is rare. Accompany an Injurious Princess on a perilous errand: the recovery of her brother from a tyrant's clutches. Journey through the northerly realms of the Elder Continent, navigate the politics of a poisonous court, and beware the spider's kiss.",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 23,
				liked: 34,
				okay: 9,
				dislike: 4
			},
			2023: {
				exceptional: 10,
				delicious: 35,
				liked: 48,
				okay: 17,
				dislike: 15
			},
			2022: {
				exceptional: 33,
				delicious: 73,
				liked: 83,
				okay: 31,
				dislike: 14
			}
		},
		official_tags: ["Elder Continent"],
		extra_tags: ["Exceptional Story", "The Palace"],
		lore_tags: [],
		threads: {
			failbetter: "december-es-the-house-of-silk-and-flame/",
			reddit: "r1wirt/decembers_exceptional_story_the_house_of_silk_and/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 98,
		title: "The Poisoner's Library",
		release_date: "2021/12/02",
		description: "A woman wearing the mask of a golden swan haunts the canals of Jericho. Visited by recurring nightmares of a wedding held in a ruined palace surrounded by poisoned waters, she is seeking answers. Answers only the keeper of a specialised library can provide\u2026 [This story requires the possession of a Jericho Library and is suitable for higher level players who have begun the Great Hellbound Railway storyline.]",
		story_rankings: {
			2024: {
				exceptional: 28,
				delicious: 24,
				liked: 14,
				okay: 6,
				dislike: 3
			},
			2023: {
				exceptional: 45,
				delicious: 30,
				liked: 20,
				okay: 4,
				dislike: 2
			},
			2022: {
				exceptional: 43,
				delicious: 41,
				liked: 15,
				okay: 8,
				dislike: 2
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 41,
				not_worth_it: 30
			},
			2023: {
				worth_it: 62,
				not_worth_it: 38
			},
			2022: {
				worth_it: 68,
				not_worth_it: 42
			}
		},
		official_tags: ["Hell", "Second City"],
		extra_tags: ["Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "the-poisoners-library/",
			reddit: "r75rpj/new_fate_story_for_advanced_players_the_poisoners/"
		},
		unlock_fate: 25
	}, {
		id: 99,
		title: "Adornment",
		release_date: "2021/12/30",
		author: "Harry Tuffs",
		description: "An incendiary smuggler seeks to rob Mr Stones of a prize beyond compare. Their intent for the Master is secret and unwise, and to achieve their end, they require a great deal of luck, a most unusual Clay Man, and you. How far must you go to untangle this labyrinthine scheme? And how far must you delve to learn what lies beneath the mines of Mr Stones?",
		story_rankings: {
			2024: {
				exceptional: 27,
				delicious: 28,
				liked: 18,
				okay: 5,
				dislike: 0
			},
			2023: {
				exceptional: 49,
				delicious: 44,
				liked: 30,
				okay: 19,
				dislike: 1
			},
			2022: {
				exceptional: 64,
				delicious: 87,
				liked: 41,
				okay: 12,
				dislike: 5
			}
		},
		official_tags: ["Masters", "Revolutionaries"],
		extra_tags: ["Exceptional Story", "Mr Stones"],
		lore_tags: [],
		threads: {
			failbetter: "january-exceptional-story-adornment/",
			reddit: "rs191g/januarys_exceptional_story_adornment_official/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 100,
		title: "Mistress Of The Skies",
		release_date: "2022/01/27",
		author: "Mary Goodden",
		description: "The Mistress of the Skies invites Londoners \u2013 rich and poor \u2013 to paint their faces with the colours of the Neath. Investigate the Striking Saleswoman, with her iridescent nails and sparkling eyelids; observe the consequences of this new pigment on London\u2019s social fabric; learn the true identity of the reclusive Mistress. And try not to trip over the cats.",
		story_rankings: {
			2024: {
				exceptional: 3,
				delicious: 17,
				liked: 30,
				okay: 15,
				dislike: 5
			},
			2023: {
				exceptional: 12,
				delicious: 37,
				liked: 45,
				okay: 33,
				dislike: 8
			}
		},
		official_tags: ["Society", "Bohemians"],
		extra_tags: ["Exceptional Story", "Neathbow"],
		lore_tags: [],
		threads: {
			failbetter: "february-exceptional-story-mistress-of-the-skies/",
			reddit: "se0w6v/februarys_exceptional_story_mistress_of_the_skies/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 101,
		title: "A Stretch in the Sky",
		release_date: "2022/02/24",
		author: "Olivia Wood",
		description: "New Newgate, the colossal stalactite-prison suspended above London, has its secrets. Its upper floors are fast filling up: to make room, someone must go free. Pose as a prisoner to infiltrate a cadre of high-profile pirates; uncover the reasons for your cellmates\u2019 incarceration; choose your allegiances carefully as you identify a candidate for release. And remember to sleep with one eye open, this close to the Roof\u2026",
		story_rankings: {
			2024: {
				exceptional: 3,
				delicious: 14,
				liked: 36,
				okay: 10,
				dislike: 4
			},
			2023: {
				exceptional: 10,
				delicious: 35,
				liked: 43,
				okay: 25,
				dislike: 14
			}
		},
		official_tags: ["Criminals"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "march-exceptional-story-a-stretch-in-the-sky/",
			reddit: "t0imsx/marchs_exceptional_story_a_stretch_in_the_sky/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 102,
		title: "Totentanz",
		release_date: "2022/03/31",
		author: "Matt Diaz",
		description: "The streets are crowded with dusty bandages. Foot traffic has slowed to a crawl. Tomb-colonists flock to London in advance of their one, last dance: the Totentanz. Hunt down its lost instruments and learn its deadly choreography. Help the Withered Celebrant throw a revel bathed in moonlight. Appease an ancient power that claims to hold even the Masters to account. Will you dance the true steps of death?",
		story_rankings: {
			2024: {
				exceptional: 5,
				delicious: 21,
				liked: 35,
				okay: 20,
				dislike: 4
			},
			2023: {
				exceptional: 14,
				delicious: 38,
				liked: 65,
				okay: 27,
				dislike: 3
			}
		},
		official_tags: ["The Tomb-Colonies"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "april-exceptional-story-totentanz/",
			reddit: "tsypet/aprils_exceptional_story_totentanz_official/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 103,
		title: "The Queen of the Elephants",
		release_date: "2022/04/28",
		author: "Harry Tuffs",
		description: "Meet Queen Mary, the fashionable thief taking London\u2019s underworld by storm. Accompany her and her Forty Elephants on an audacious heist that stretches the bounds of the possible, learn of the Queen of Elephants\u2019 true ambitions, and face up to the fearsome nightmare that made her who she is today.",
		story_rankings: {
			2024: {
				exceptional: 14,
				delicious: 29,
				liked: 24,
				okay: 10,
				dislike: 2
			},
			2023: {
				exceptional: 29,
				delicious: 60,
				liked: 31,
				okay: 17,
				dislike: 4
			}
		},
		official_tags: ["Criminals", "Bohemians"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "may-exceptional-story-the-queen-of-the-elephants/",
			reddit: "ue43c7/mays_exceptional_story_the_queen_of_the_elephants/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 104,
		title: "Codename: Sugarplum",
		release_date: "2022/05/26",
		author: "Chandler Groover",
		description: "A journalist from the Surface has lost her pet dachshund, and contracted you to retrieve him. How hard can it be to find a missing dog? Just follow the clues, infiltrate a few undercover networks, expose a vast conspiracy in the Great Game, uncover the dark secrets of the sugar substitute industry, and you'll be back in time for tea.",
		story_rankings: {
			2024: {
				exceptional: 65,
				delicious: 19,
				liked: 5,
				okay: 4,
				dislike: 3
			},
			2023: {
				exceptional: 117,
				delicious: 33,
				liked: 15,
				okay: 3,
				dislike: 4
			}
		},
		official_tags: ["The Great Game"],
		extra_tags: ["Exceptional Story", "Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "june-exceptional-story-codename-sugarplum/",
			reddit: "uyce7e/junes_exceptional_story_codename_sugarplum/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 105,
		title: "Dernier Cri",
		release_date: "2022/06/30",
		author: "Gavin Inglis",
		description: "Famed London fashion house Abreo Couture is under new management, and about to launch its most daring collection yet. Explore unique Neathy fashions, design your very own lines, investigate the Persistent Prognosticator\u2019s eccentric hiring practices, and help your Deputy Head of Design change hearts and minds one stitch at a time.",
		story_rankings: {
			2024: {
				exceptional: 5,
				delicious: 19,
				liked: 29,
				okay: 13,
				dislike: 9
			},
			2023: {
				exceptional: 13,
				delicious: 29,
				liked: 48,
				okay: 34,
				dislike: 20
			}
		},
		official_tags: ["The Great Game"],
		extra_tags: ["Exceptional Story", "Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "july-exceptional-story-dernier-cri/",
			reddit: "vo7zl8/julys_exceptional_story_dernier_cri_official/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 106,
		title: "Inheritance",
		release_date: "2022/07/28",
		author: "Mary Goodden",
		description: "A Sage Archivist is consumed by her study of the First City. It is an obsession that has seen her sever all ties and take up unsettling experiments with lacre. Her former paramour fears for her safety, and looks to you to investigate this malady. Delve into the ruins below London to uncover a twisted legacy of love, memory, and duty.",
		story_rankings: {
			2024: {
				exceptional: 4,
				delicious: 28,
				liked: 28,
				okay: 12,
				dislike: 5
			},
			2023: {
				exceptional: 24,
				delicious: 35,
				liked: 61,
				okay: 18,
				dislike: 6
			}
		},
		official_tags: ["Masters", "Benthic"],
		extra_tags: ["Exceptional Story", "First City"],
		lore_tags: [],
		threads: {
			failbetter: "august-exceptional-story-inheritance/",
			reddit: "wa8qr6/augusts_exceptional_story_inheritance_official/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 107,
		title: "A Columbidaean Commotion",
		release_date: "2022/08/25",
		author: "George Lockett",
		description: "London's bird population has suffered since the Fall \u2013 robbed of their skies, forced to compete with bats, and preyed upon as easy meals. But the Unremarkable Fancier has plans to improve the humble pigeon's lot. Help the struggling Philoperisteron Society elect a new chairman, learn the secrets of the Delightful Dove's ever-sprouting twig, and find these birds a new place in the day-to-day life of the Neath.",
		story_rankings: {
			2024: {
				exceptional: 11,
				delicious: 35,
				liked: 23,
				okay: 16,
				dislike: 3
			},
			2023: {
				exceptional: 16,
				delicious: 53,
				liked: 51,
				okay: 29,
				dislike: 4
			}
		},
		official_tags: ["Urchins", "Clay Men"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "september-exceptional-story-a-columbidaean-commotion/20656",
			reddit: "wxj8ca/septembers_exceptional_story_a_columbidaean/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 108,
		title: "The Exile's Chalice",
		release_date: "2022/09/29",
		author: "Luke van den Barselaar",
		description: "A lost city, submerged for generations. A great act of love and rebellion. A despot's wrath. And the quest for a grail... The bright forces of the Presbyterate mobilise to reclaim a forgotten legacy and rewrite history. The Morbid Archaeologist will not allow it \u2013 but they cannot stand alone. Journey across the Unterzee and delve into the depths of history in 'The Exile's Chalice'.",
		story_rankings: {
			2024: {
				exceptional: 37,
				delicious: 22,
				liked: 17,
				okay: 6,
				dislike: 3
			},
			2023: {
				exceptional: 73,
				delicious: 47,
				liked: 18,
				okay: 11,
				dislike: 3
			}
		},
		official_tags: ["Elder Continent"],
		extra_tags: ["Exceptional Story", "Unterzee"],
		lore_tags: [],
		threads: {
			failbetter: "october-exceptional-story-the-exiles-chalice/20743",
			reddit: "xrh533/octobers_exceptional_story_the_exiles_chalice/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 109,
		title: "A Devil's Due",
		release_date: "2022/10/27",
		author: "Bruno Dias",
		description: "Verity is on the hunt for a particular soul, and she won't take 'no' for an answer! Consort with devils high and low as you track the Lyrical Soul through infernal society, strike bargains in a primordial currency of Hell, and uncover an ancient story of love and loss.",
		story_rankings: {
			2024: {
				exceptional: 11,
				delicious: 18,
				liked: 34,
				okay: 20,
				dislike: 3
			},
			2023: {
				exceptional: 13,
				delicious: 31,
				liked: 55,
				okay: 31,
				dislike: 18
			}
		},
		official_tags: ["Hell"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "november-exceptional-story-a-devils-due/20819",
			reddit: "yf1yx3/novembers_exceptional_story_a_devils_due_official/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 110,
		title: "The Mushroom's Dream",
		release_date: "2022/11/17",
		description: "Something is rotten on the Mangrove College. Once-aimless philosophers have a newfound purpose, and the Mangrove Schools vie for intellectual dominance. Delve into the swamp of philosophy and help the Unreconstructed Cynic synthesise his transient philosophies as you both seek the truth that lurks at the heart of the Wisp-Ways. Completing The Mushroom's Dream unlocks a unique, persistent activity on the Mangrove College. This story requires use of a ship.",
		story_rankings: {
			2024: {
				exceptional: 11,
				delicious: 31,
				liked: 20,
				okay: 7,
				dislike: 6
			},
			2023: {
				exceptional: 19,
				delicious: 39,
				liked: 18,
				okay: 5,
				dislike: 3
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 60,
				not_worth_it: 10
			},
			2023: {
				worth_it: 70,
				not_worth_it: 15
			}
		},
		official_tags: ["Mushrooms"],
		extra_tags: ["Permanent Reward"],
		lore_tags: [],
		threads: {
			failbetter: "the-mushrooms-dream/20886",
		},
		unlock_fate: 25,
	}, {
		id: 111,
		title: "SALON SCANDAL!",
		release_date: "2022/11/24",
		author: "Chandler Groover",
		description: "What actually happened at the Duchess of Bedfordshire's infamous salon? Who was caught in the Duke's bedchamber? How did Lord J_____ die? And who stole the Vicar's cassock? The papers print such outlandish rumours! But you were there. You saw it all. And yours is the tale that will separate fact from fiction.",
		story_rankings: {
			2024: {
				exceptional: 14,
				delicious: 21,
				liked: 41,
				okay: 13,
				dislike: 9
			},
			2023: {
				exceptional: 21,
				delicious: 42,
				liked: 50,
				okay: 40,
				dislike: 25
			}
		},
		official_tags: ["Society"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "december-exceptional-story-salon-scandal/20912",
			reddit: "z3o4zq/decembers_exceptional_story_salon_scandal/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 112,
		title: "Stolen Stanzas",
		release_date: "2022/12/29",
		author: "Gavin Inglis",
		description: "The entire print run of Julian R. Harvey's poetic epic 'Reflections on Ys' has been confiscated by the agents of Mr Pages! Investigate the contents of this mysterious volume, plan an audacious theft from a Ministry warehouse, and reunite the publisher with his product. Then maybe \u2013 just maybe \u2013 you'll be able to sneak a peek at the latest work to pique Mr Pages' ire.",
		story_rankings: {
			2024: {
				exceptional: 3,
				delicious: 21,
				liked: 38,
				okay: 24,
				dislike: 8
			},
			2023: {
				exceptional: 18,
				delicious: 51,
				liked: 42,
				okay: 22,
				dislike: 7
			}
		},
		official_tags: ["Bohemians"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "january-exceptional-story-stolen-stanzas/21034",
			reddit: "zyxj3d/januarys_exceptional_story_stolen_stanzas/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 113,
		title: "The Hollow Triptych",
		release_date: "2023/01/26",
		author: "Luke van den Barselaar",
		description: "An art gallery has appeared on Hollow Street \u2013 one of London's most insalubrious locales \u2013 and its collection is taking the city by storm. The rich and mannered brave the muck of Veilgarden to catch a glimpse of coiling, idiosyncratic masterworks. But just who is the Recondite Painter? How did he come by his hallucinatory talents? And why, when you sleep, do you dream of paint, and of skin that is not your own?",
		story_rankings: {
			2024: {
				exceptional: 13,
				delicious: 40,
				liked: 35,
				okay: 10,
				dislike: 1
			}
		},
		official_tags: ["Bohemians"],
		extra_tags: ["Exceptional Story", "Parabola", "Arts"],
		lore_tags: [],
		threads: {
			failbetter: "february-exceptional-story-the-hollow-triptych/21121",
			reddit: "10lzsnf/februarys_exceptional_story_the_hollow_triptych/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 114,
		title: "A Bright Future",
		release_date: "2023/02/23",
		author: "George Lockett",
		description: "The Illuminating Futurist has a bright vision for London \u2013 a city of light and prosperity. But the future of the Fifth City is contested by fierce and mighty powers, and they are rarely kind to competition. Secure funding for the Futurist's invention from the sometimes-great and almost-good, weigh the betterment of the people against the interests of capital, decide the future of this Miraculous Device, and \u2013 perhaps \u2013 blow something up.",
		story_rankings: {
			2024: {
				exceptional: 9,
				delicious: 36,
				liked: 34,
				okay: 11,
				dislike: 8
			}
		},
		official_tags: ["Masters"],
		extra_tags: ["Exceptional Story", "Revolutionaries"],
		lore_tags: [],
		threads: {
			failbetter: "march-exceptional-story-a-bright-future/21245",
			reddit: "11a8vux/marchs_exceptional_story_a_bright_future_official/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 115,
		title: "The Deadly Dapperlings",
		release_date: "2023/03/30",
		author: "Camille Nohra",
		description: "The Benthic Engineering Club, \u2018The Deadly Dapperlings\u2019, need an advisor to help with the upcoming Inventors Competition. Their work-in-progress \u2013 a fungal network that they hope will revolutionise long-distance communication \u2013 is brilliant, revolutionary, and only occasionally explodes.",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 19,
				liked: 43,
				okay: 25,
				dislike: 2
			}
		},
		official_tags: ["The University"],
		extra_tags: ["Exceptional Story", "Benthic"],
		lore_tags: [],
		threads: {
			failbetter: "april-exceptional-story-the-deadly-dapperlings/21327",
			reddit: "126kyva/aprils_exceptional_story_the_deadly_dapperlings/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 116,
		title: "The Mudlark's Lament",
		release_date: "2023/04/27",
		author: "Luke van den Barselaar",
		description: "There is a beast in the sewers beneath the Fifth City \u2013 or so the urchins believe. But the Precocious Tosher has no fear, and a good, honest monster-battering is the only thing standing between her and the ballads of urchin legend. All she needs is an expendab\u2014 ahem, a dependable longshanks to bear witness to her heroism.",
		story_rankings: {
			2024: {
				exceptional: 19,
				delicious: 35,
				liked: 27,
				okay: 9,
				dislike: 4
			}
		},
		official_tags: ["Urchins"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "may-exceptional-story-the-mudlarks-lament/21380",
			reddit: "1313xtx/mays_exceptional_story_the_mudlarks_lament/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 117,
		title: "Flame, Lead, Clay, Glass",
		release_date: "2023/05/25",
		author: "George Lockett",
		description: "A hungry fire blazes through the streets of London. A fire in the shape of a man. A fire that is looking for something. The Elevated Engineer seeks to harness it; the Clay Correspondent to destroy it. They oppose each other at every turn, and share a secret too terrible to say aloud.",
		story_rankings: {
			2024: {
				exceptional: 24,
				delicious: 36,
				liked: 28,
				okay: 12,
				dislike: 6
			}
		},
		official_tags: ["Clay Men"],
		extra_tags: ["Exceptional Story", "Red Science", "The Correspondence"],
		lore_tags: [],
		threads: {
			failbetter: "june-exceptional-story-flame-lead-clay-glass/21444",
			reddit: "13rrgfm/junes_exceptional_story_flame_lead_clay_glass/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 118,
		title: "The Stolen Song",
		release_date: "2023/06/29",
		author: "Katharine Neil",
		description: "A performance interrupted; a melody malappropriated; a courtroom in chaos. The Accused Contralto stands trial, charged with the theft of her latest music-hall song. Take the reins from the Contralto's truly abysmal counsel. Investigate the origins of the contested tune. Lead cross-examinations against eccentric and unwilling witnesses. Pursue truth, or justice, or simple acquittal by any means you can.",
		story_rankings: {
			2024: {
				exceptional: 1,
				delicious: 19,
				liked: 41,
				okay: 26,
				dislike: 14
			}
		},
		official_tags: ["Bohemians"],
		extra_tags: ["Exceptional Story", "Arts", "Law"],
		lore_tags: [],
		threads: {
			failbetter: "july-exceptional-story-the-stolen-song/21557",
			reddit: "14m6k6n/julys_exceptional_story_the_stolen_song_official/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 119,
		title: "The Bloody Wallpaper",
		release_date: "2023/07/27",
		author: "Chandler Groover",
		description: "The Red-and-Gold Gala commences! The Royal Bethlehem Hotel always hosts the Gala during the Season, when social butterflies flock to London for the year's most exclusive parties. It's going to be a busy night; Fine dining. Guests from across the waking and sleeping worlds. Not to mention all the gossip. Invitations are exclusive \u2013 and scarce \u2013 but as a member of hotel staff, you'll be right at the centre of the action. Just sign here \u2013 with your own blood, please.",
		story_rankings: {
			2024: {
				exceptional: 116,
				delicious: 19,
				liked: 6,
				okay: 2,
				dislike: 2
			}
		},
		extra_tags: ["Exceptional Story", "Society"],
		lore_tags: [],
		threads: {
			failbetter: "august-exceptional-story-the-bloody-wallpaper/21631",
			reddit: "15be0td/augusts_exceptional_story_the_bloody_wallpaper/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 120,
		title: "The Path of Blood and Smoke",
		release_date: "2023/08/31",
		author: "Luke van den Barselaar",
		description: "An ancient hunger wakes in the tomb-colonies. A prophecy of the Third City unfurls among the ice and rock, and if the Lace-Wrapped Emissary can be believed, you are the fulcrum. Follow in the footsteps of the Priest-Kings. Tread the path of trust and hear the call of betrayal.",
		story_rankings: {
			2024: {
				exceptional: 29,
				delicious: 39,
				liked: 33,
				okay: 14,
				dislike: 9
			}
		},
		official_tags: ["The Tomb-Colonies"],
		extra_tags: ["Exceptional Story", "Third City"],
		lore_tags: [],
		threads: {
			failbetter: "september-exceptional-story-the-path-of-blood-and-smoke/21749",
			reddit: "1677tyv/septembers_exceptional_story_the_path_of_blood/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 121,
		title: "The Children of the Glow",
		release_date: "2023/09/28",
		author: "Katharine Neil",
		description: "A new artistic school, styling themselves 'the Children of the Glow', has arisen from the dark. Their works are ethereal, glowing with an inner light that is a balm in the Neath's shadowed corners. London's fickle attention is piqued \u2013 and where public interest moves, the press must follow! As (temporary) arts correspondent for the London Unexpurgated Gazette, Mr Huffam has charged you with delivering the scoop on this shining clique of creatives. Unravel their politics. Rummage in their art supplies. Untangle, bit by bit, the mystery of their muse: the Luminous Miss Sparks. You've got a deadline, and column inches to fill. What are you waiting for?",
		story_rankings: {
			2024: {
				exceptional: 2,
				delicious: 28,
				liked: 55,
				okay: 36,
				dislike: 17
			}
		},
		extra_tags: ["Exceptional Story", "Bohemians", "Arts"],
		lore_tags: [],
		threads: {
			failbetter: "october-exceptional-story-the-children-of-the-glow/21816",
			reddit: "16ussfh/octobers_exceptional_story_the_children_of_the/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 122,
		title: "The Stripes of Wrath",
		release_date: "2023/10/26",
		author: "Gavin Inglis",
		description: "A hideous murder attempt rocks London \u2013 an attack so vicious it sends its victim off to the Grand Sanatorium. A tangle of magicians, tigers and insects has the Constables scratching their heads and the greatest detectives in London flummoxed. But what did the cabbies see? And what is that persistent buzzing noise\u2026",
		story_rankings: {
			2024: {
				exceptional: 28,
				delicious: 46,
				liked: 53,
				okay: 13,
				dislike: 0
			}
		},
		official_tags: ["Urchins", "Tigers"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "november-exceptional-story-the-stripes-of-wrath/21904",
			reddit: "17i0mgb/novembers_exceptional_story_the_stripes_of_wrath/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 123,
		title: "The Green King",
		release_date: "2023/11/30",
		author: "James Chew",
		description: "An invite in emerald; a woman in scarlet; an exclusive dining club that meets but once every seven years. What do these things have in common, and what on earth do they want with you? Infiltrate London's upper crust and learn the moves of a game long in the playing. Ensure your piece becomes a king rather than a pawn. Beware the pull of a castle across the zee.",
		story_rankings: {
			2024: {
				exceptional: 39,
				delicious: 77,
				liked: 28,
				okay: 7,
				dislike: 5
			}
		},
		official_tags: ["Bohemians", "Criminals", "Society"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "december-exceptional-story-the-green-king/22001",
			reddit: "187ivcp/decembers_exceptional_story_the_green_king/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 124,
		title: "A Dream of a Thousand Tails",
		release_date: "2023/12/12",
		description: "You dream that a story lives in your walls. It has got its teeth into your pantry. It tells itself in little leavings by the skirting board. You hear it when you're in bed, in the dark, scampering towards an ending.<br><br>The story has whiskers, and it goes like this: 'There is a king little bigger than a finger, who sits in a garden beyond the edge of telling. The king ties itself in knots, and feasts upon stories like a rat upon cheese...'",
		story_rankings: {
			2024: {
				exceptional: 64,
				delicious: 33,
				liked: 7,
				okay: 2,
				dislike: 0
			}
		},
		reward_rankings: {
			2024: {
				worth_it: 82,
				not_worth_it: 12
			}
		},
		lore_tags: [],
		threads: {
			failbetter: "premium-story-a-dream-of-a-thousand-tails/22044",
		},
		unlock_fate: 25
	}, {
		id: 125,
		title: "A Newt By Any Other Name",
		release_date: "2023/12/28",
		author: "Chandler Groover",
		description: "This can't be happening! Lord P______ is the most scandalous man in London, and his diamond newt has found its way into your hands! Do you know what he does with this newt? Even the mildest rumours could destroy your reputation by association! But you have a plan to save your good name. You'll have to recruit a few criminal accomplices, break into Lord P______'s townhouse, and put the newt back where it belongs \u2013 without anyone learning you ever touched it. Prepare to embark on a heist like none other.",
		story_rankings: {
			2024: {
				exceptional: 53,
				delicious: 58,
				liked: 21,
				okay: 3,
				dislike: 3
			}
		},
		official_tags: ["Bohemians", "Criminals"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "january-exceptional-story-a-newt-by-any-other-name/22076",
			reddit: "18t89kj/januarys_exceptional_story_a_newt_by_any_other/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 126,
		title: "The Tale of Old Fritz",
		release_date: "2024/01/25",
		author: "Ben Sabin",
		description: "The Doomed Diver is haunted by nightmares of sinking ships, a graveyard beast rising from the deep, and a lady with a lantern who guides them inexorably to their end. Driven to desperation, they've taken to wandering London at night, seeking companions for a doomed mission: to hunt the fabled Old Fritz across the wide Unterzee. Uncover their tragic history with the beast, and their connection to the lost ship Incredulous. Follow monster and man under the waters, and into the lightless depths below.",
		story_rankings: {},
		official_tags: ["The Docks"],
		extra_tags: ["Exceptional Story", "Unterzee", "Zailors"],
		lore_tags: [],
		threads: {
			failbetter: "february-exceptional-story-the-tale-of-old-fritz/22163",
			reddit: "1abpyr8/february_exceptional_story_the_tale_of_old_fritz/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 127,
		title: "Slobgollion",
		release_date: "2024/02/29",
		author: "Chandler Groover",
		description: "Rubbery Men don't take the slow boat. Wherever they go after death, none have ever returned to life \u2013 until now. Investigate the case of Mr Martin McIntosh, the subject of the first Rubbery resurrection. Get to know the bodies in Concord Square's morgue, and delve into the Fifth City's illegal amber trade. What secrets lurk beyond the grave? And what in the world does 'slobgollion' mean?",
		story_rankings: {},
		official_tags: ["Rubberies", "Constables"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "march-exceptional-story-slobgollion/22349",
			reddit: "1b3f6gz/marchs_exceptional_story_slobgollion_official/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 128,
		title: "There is the Richest Juice in Poison-Flowers",
		release_date: "2024/03/28",
		author: "Mary Goodden",
		description: "There is a plant on the Surface that blooms but once every ten years. Somehow, a Sun-touched Gardener has not only brought it down to the Neath \u2013 he has also kept it alive, and thriving. Now, at last, it is time for it to flower. But why is it being kept in a poison-garden? And who are the mystery buyers who mean to keep the plant for themselves? All will be illuminated, when the poison-flower blooms.",
		story_rankings: {},
		official_tags: ["Revolutionaries", "Bohemians"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "april-exceptional-story-there-is-the-richest-juice-in-poison-flowers/22440",
			reddit: "1bqlq2s/aprils_exceptional_story_there_is_the_richest/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 129,
		title: "The Sunken River",
		release_date: "2024/04/25",
		author: "Bruno Dias",
		description: "The Unsanctioned Zubmariner has one last voyage to make \u2013 and, through the Admiralty\u2019s machinations, you\u2019ve just been promoted to First Officer. Embark on an ill-advised, officially unacknowledged expedition beneath the dark waves. Follow a trail of blood to the very heart of the Unterzee. Try not to crack under the pressure.",
		story_rankings: {},
		official_tags: ["The Docks", "The Admiralty"],
		extra_tags: ["Exceptional Story", "Unterzee"],
		lore_tags: [],
		threads: {
			failbetter: "may-exceptional-story-the-sunken-river/22522",
			reddit: "1cdrwgm/mays_exceptional_story_the_sunken_river/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 130,
		title: "D\u00e9j\u00e0 Vu",
		release_date: "2024/05/30",
		author: "Gavin Inglis",
		description: "The venerable Portinari Dance Hall is failing \u2013 again! \u2013 and it's up to you to inject some life into the old place. Reimagine the establishment in a grand overhaul, and shepherd it through a fractious opening night. Uncover the hopes and aspirations of the patrons that seek respite beneath its roof. Are all dreams destined to fade with time?",
		story_rankings: {},
		official_tags: ["Bohemians"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "june-exceptional-story-deja-vu/22617",
			reddit: "1d4l58v/junes_exceptional_story_d\u00e9j\u00e0_vu_unofficial/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 131,
		title: "The Laws of the Game",
		release_date: "2024/06/27",
		author: "Adam Dixon",
		description: "The Ardent Regulator is a Ministry Official with a daunting task: cataloguing and codifying the definitive rules of the game of football. But no two teams play exactly the same game. Venderbight United have come equipped with mallets. One of the Lavender House players is wearing a bee costume. And why do the No Names Club wear masks?",
		story_rankings: {},
		official_tags: ["The Tomb-Colonies", "Hell", "The Church"],
		extra_tags: ["Exceptional Story", "Sports"],
		lore_tags: [],
		threads: {
			failbetter: "july-exceptional-story-the-laws-of-the-game/22676",
			reddit: "1dpz0b0/julys_exceptional_story_the_laws_of_the_game/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 132,
		title: "Arcana",
		release_date: "2024/07/25",
		author: "Chandler Groover",
		description: "Airag from the Year of the Tortoise is notoriously expensive, but it pales in comparison to the Year of the Serpent. Nobody can afford it. It has not even finished fermenting yet; it exists only as a commodity for the future. If the Thirsty Croupier can be believed, it exists in your future. When will the legendary airag be ready to drink? Are the rumours about its ingredients true? Consult the cards. Interpret the spread. Taste a delicacy that does not yet exist.",
		story_rankings: {},
		official_tags: ["The Khanate", "Elder Continent"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "august-exceptional-story-arcana/22731",
			reddit: "1ec0nop/augusts_exceptional_story_arcana_unofficial/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 133,
		title: "A Nest in the Eaves",
		release_date: "2024/08/29",
		author: "Mary Goodden",
		description: "The Starved Mystic believes London is sick, beset by the fevers of self-interest and individualism. For those who seek to escape this sickness, she offers change: a journey to the Roof, and induction into the ways of her hive. Steep yourself in the values of her clade. Shape yourself to suit your role. Confront the secrets in the beating heart of her community, and choose whether to accept them into your body.",
		story_rankings: {},
		official_tags: ["Starved Men"],
		extra_tags: ["Exceptional Story"],
		spoiler_tags: ["Hell"],
		lore_tags: [],
		threads: {
			failbetter: "september-exceptional-story-a-nest-in-the-eaves/22827",
			reddit: "1f4d2sx/septembers_exceptional_story_a_nest_in_the_eaves/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 134,
		title: "Death and Tax Evasion",
		release_date: "2024/09/26",
		author: "Harry Tuffs",
		description: "For many, the Neath has brought reprieve from the inevitabilities of death and taxation. For many, but not all. The Rattus faber get only one chance at life. And to make matters worse, now the Board of Underland Revenue is on their back. Suffer the knock of the Revenuer at your door. Investigate a schism in the ratty church, and learn what rodents hope for after death. And maybe \u2013 just maybe \u2013 resolve the question of your taxes.",
		story_rankings: {},
		official_tags: ["Rats"],
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "october-exceptional-story-death-and-tax-evasion/22908",
			reddit: "1fpy9cn/octobers_exceptional_story_death_and_tax_evasion/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 135,
		title: "Aces High",
		release_date: "2024/10/31",
		author: "Gavin Inglis",
		description: "The Earl of Awry's Bridge Club has received an unusual challenge: a tournament aboard an airship, held against the sons and daughters of empire. It is the perfect opportunity to network, and to raise one's station in society \u2013 provided, of course, that nobody cheats. Keep a sharp eye on the card tables, and a sharper eye on the competitors.",
		story_rankings: {},
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "exceptional-story-for-november-aces-high/23716",
			reddit: "1ggjrxq/novembers_exceptional_story_aces_high_unofficial/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 136,
		title: "A Dinner to Die For",
		release_date: "2024/11/28",
		author: "Ben Sabin",
		description: "Few get to enjoy the pleasure of watching a great detective at work up close. Fewer still get to do so while attempting to evade the attentions of a murderous mastermind. Happily, this evening, you'll get to do both: lucky you. Join the Surface's finest mind as he attempts to expose the rot at the heart of London's most enigmatic address. Wine, dine, uncover conspiracies and unmask murders. Try to survive till the cheese course.",
		story_rankings: {},
		extra_tags: ["Exceptional Story"],
		lore_tags: [],
		threads: {
			failbetter: "exceptional-story-for-december-a-dinner-to-die-for/23805",
			reddit: "1h2auzj/decembers_exceptional_story_a_dinner_to_die_for/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}, {
		id: 137,
		title: "The Play's the Thing",
		release_date: "2024/12/26",
		author: "Nell Raban",
		description: "Many dream about putting on a play; fewer have the opportunity to stage one. And fewer still get to stage theirs in dream itself. Join forces with the Neath's newest talent as she attempts to perform the impossible: a work of art that will bring truth to Parabola, where nothing is real. Select your cast, rehearse your lines and put on the performance of several lifetimes. Oh, and watch out for the subtext. It bites.",
		story_rankings: {},
		official_tags: ["Parabola"],
		extra_tags: ["Exceptional Story", "Cats", "Fingerkings"],
		lore_tags: [],
		threads: {
			failbetter: "exceptional-story-for-january-the-plays-the-thing/23869",
			reddit: "1hmw7p7/januarys_exceptional_story_the_plays_the_thing_un/"
		},
		unlock_fate: 45,
		reset_fate: 25
	},
  {
		id: 138,
		title: "A Deadly Custard",
		release_date: "2025/01/30",
		author: "Kate Gray",
		description: "Gazebos spring up in the centre of London. The smell of cake winds through the streets. The Lady Pockets, alongside her enigmatic butlers, is holding a baking competition \u2013 and you've been entered as a participant. Never mind that you didn't apply. Never mind that your rivals are more suspicious than a mouldy macaron, or that nobody will tell you what you're competing for. If you want answers, you'll need to stay in the game \u2013 so get set and ready to bake.",
		story_rankings: {},
		official_tags: ["The Great Game"],
		extra_tags: ["Exceptional Story", "Food"],
		lore_tags: [],
		threads: {
			failbetter: "exceptional-story-for-february-a-deadly-custard/24148",
			reddit: "1idltuv/februarys_exceptional_story_a_deadly_custard/"
		},
		unlock_fate: 45,
		reset_fate: 25
	},
	{
		id: 139,
		title: "Memories of Mozart",
		release_date: "2025/02/27",
		author: "Harry Tuffs",
		description: "Something has happened to your memories. Something drastic. They are, in fact, gone: a blank space in your skull where the last few days should be. This will not stand! You'll have to investigate. The task before you is simple: work backwards, and discover what connects you to a museum heist, a veiled woman, the distant land of Polythreme, and the alleged skull of Wolfgang Amadeus Mozart. Marvellous!",
		story_rankings: {},
		extra_tags: ["Exceptional Story", "Polythreme", "Neathbow"],
		lore_tags: [],
		threads: {
			failbetter: "march-exceptional-story-memories-of-mozart/24303",
			reddit: "1ize5qv/marchs_exceptional_story_memories_of_mozart/"
		},
		unlock_fate: 45,
		reset_fate: 25
	}
];
