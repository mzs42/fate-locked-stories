const SEASONS = {
	"Heart's Blood": {
		stories: [20, 21, 22],
		season_rankings: {
			2020: {
				exceptional: 4,
				delicious: 9,
				liked: 18,
				okay: 14,
				dislike: 5
			},
			2019: {
				exceptional: 2,
				delicious: 6,
				liked: 8,
				okay: 9,
				dislike: 5
			}
		}
	},
	"Family Ties": {
		stories: [23, 24, 25],
		season_rankings: {
			2020: {
				exceptional: 19,
				delicious: 16,
				liked: 10,
				okay: 2,
				dislike: 1
			},
			2019: {
				exceptional: 10,
				delicious: 9,
				liked: 5,
				okay: 1,
				dislike: 0
			}
		}
	},
	"Revolutions": {
		stories: [26, 27, 28],
		season_rankings: {
			2020: {
				exceptional: 21,
				delicious: 15,
				liked: 10,
				okay: 3,
				dislike: 0
			},
			2019: {
				exceptional: 15,
				delicious: 7,
				liked: 2,
				okay: 3,
				dislike: 0
			}
		}
	},
	"Wrecks": {
		stories: [29, 30, 31],
		season_rankings: {
			2020: {
				exceptional: 21,
				delicious: 19,
				liked: 17,
				okay: 5,
				dislike: 1
			},
			2019: {
				exceptional: 18,
				delicious: 8,
				liked: 4,
				okay: 3,
				dislike: 0
			}
		}
	},
	"Skies": {
		stories: [32, 34, 35],
		season_rankings: {
			2020: {
				exceptional: 29,
				delicious: 14,
				liked: 13,
				okay: 4,
				dislike: 1
			},
			2019: {
				exceptional: 19,
				delicious: 12,
				liked: 8,
				okay: 1,
				dislike: 1
			}
		}
	},
	"Stones": {
		stories: [36, 37, 38],
		season_rankings: {
			2020: {
				exceptional: 28,
				delicious: 10,
				liked: 15,
				okay: 3,
				dislike: 2
			},
			2019: {
				exceptional: 18,
				delicious: 10,
				liked: 5,
				okay: 1,
				dislike: 2
			}
		}
	},
	"Ruins": {
		stories: [39, 40, 41],
		season_rankings: {
			2020: {
				exceptional: 28,
				delicious: 23,
				liked: 14,
				okay: 3,
				dislike: 1
			},
			2019: {
				exceptional: 16,
				delicious: 18,
				liked: 5,
				okay: 4,
				dislike: 0
			}
		}
	},
	"Sceptres": {
		stories: [42, 43, 44],
		season_rankings: {
			2020: {
				exceptional: 32,
				delicious: 15,
				liked: 14,
				okay: 5,
				dislike: 0
			},
			2019: {
				exceptional: 28,
				delicious: 12,
				liked: 6,
				okay: 1,
				dislike: 0
			}
		}
	},
	"Silver": {
		stories: [45, 47, 48],
		season_rankings: {
			2020: {
				exceptional: 5,
				delicious: 17,
				liked: 23,
				okay: 7,
				dislike: 5
			},
			2019: {
				exceptional: 3,
				delicious: 12,
				liked: 21,
				okay: 13,
				dislike: 1
			}
		}
	},
	"Adorations": {
		stories: [49, 50, 51],
		season_rankings: {
			2020: {
				exceptional: 23,
				delicious: 22,
				liked: 13,
				okay: 8,
				dislike: 2
			},
			2019: {
				exceptional: 24,
				delicious: 19,
				liked: 12,
				okay: 4,
				dislike: 1
			}
		}
	},
	"Embers": {
		stories: [52, 53, 54],
		season_rankings: {
			2020: {
				exceptional: 29,
				delicious: 20,
				liked: 16,
				okay: 5,
				dislike: 1
			},
			2019: {
				exceptional: 24,
				delicious: 27,
				liked: 9,
				okay: 1,
				dislike: 0
			}
		}
	},
	"Celebrations": {
		stories: [55, 56, 57],
		season_rankings: {
			2020: {
				exceptional: 11,
				delicious: 30,
				liked: 23,
				okay: 10,
				dislike: 4
			}
		}
	},
	"Hobbies": {
		stories: [58, 60, 61],
		season_rankings: {
			2020: {
				exceptional: 23,
				delicious: 26,
				liked: 29,
				okay: 3,
				dislike: 3
			}
		}
	},
	"Explorations": {
		stories: [62, 63, 64],
		season_rankings: {
			2020: {
				exceptional: 14,
				delicious: 29,
				liked: 32,
				okay: 14,
				dislike: 7
			}
		}
	},
	"Bargains": {
		stories: [65, 66, 67],
		season_rankings: {
			2020: {
				exceptional: 14,
				delicious: 36,
				liked: 37,
				okay: 14,
				dislike: 4
			}
		}
	},
	"Propinquity": {
		stories: [68, 69, 70]
	},
	"Animals": {
		stories: [71, 73, 74]
	},
	"Endeavour": {
		stories: [75, 76, 77]
	}
};