class Tooltip {
	constructor(data, target) {
		this._data_obj = data;
		this._data = undefined;
		this.tooltip = null;
		this.visible = false;
		this.target = target;
		if (this.target) {
			const show_fn = (e) => {
				if (!this.visible) {
					this.build();
					this.translate(0, 8);
				}
			};
			const hide_fn = (e) => {
				if (this.visible) {
					this.hide();
				}
			};
			this.target.addEventListener('mouseenter', show_fn);
			this.target.addEventListener('focus', show_fn);
			this.target.addEventListener('mouseleave', hide_fn);
			this.target.addEventListener('blur', hide_fn);
		}
	}
	get data() {
		if (undefined == this._data) {
			if (typeof this._data_obj == 'string') {
				this._data = document.createTextNode(this._data_obj);
			} else if (this._data_obj instanceof String) {
				this._data = document.createTextNode(this._data_obj).valueOf();
			} else if (this._data_obj instanceof Node) {
				this._data = this._data_obj;
			} else {
				this._data = document.createTextNode(this._data_obj).toString();
			}
		}
		return this._data;
	}
	build() {
		if (null == this.tooltip) {
			const content_desc = document.createElement('div');
			content_desc.classList.add('tooltip-description');
			content_desc.append(this.data.cloneNode(true));
			const content_inner = document.createElement('div');
			content_inner.classList.add('tooltip-content-inner');
			content_inner.appendChild(content_desc);
			const content_div = document.createElement('div');
			content_div.classList.add('tooltip-content')
			content_div.appendChild(content_inner);
			const box = document.createElement('div');
			box.classList.add('tooltip-box');
			box.tabIndex = -1;
			box.appendChild(content_div);
			this.tooltip = document.createElement('div');
			this.tooltip.classList.add('tooltip');
			this.tooltip.id = `tooltip-${Tooltip.nextId++}`;
			this.tooltip.appendChild(box);
		}
		document.body.appendChild(this.tooltip);
		if (this.target) {
			this.target.setAttribute('aria-describedby', this.tooltip.id);
		} else {
			this.tooltip.classList.add('static');
		}
		this.visible = true;
		return this.tooltip;
	}
	hide() {
		this.visible = false;
		if (this.target) {
			this.target.removeAttribute('aria-describedby');
		}
		document.body.removeChild(this.tooltip);
	}
	translate(x_offset, y_offset) {
		if (this.target) {
			const target_rect = this.target.getBoundingClientRect();
			const tt_rect = this.tooltip.getBoundingClientRect();
			let direction = 'bottom';
			if ((window.innerHeight < target_rect.bottom + y_offset + tt_rect.height) && (target_rect.top > y_offset + tt_rect.height)) {
				direction = 'top';
			}
			if (direction == 'bottom') {
				y_offset += window.scrollY + target_rect.bottom;
				this.tooltip.classList.remove('top');
				this.tooltip.classList.add('bottom');
			} else if (direction = 'top') {
				y_offset = (window.scrollY + target_rect.top - y_offset) - window.innerHeight;
				this.tooltip.classList.remove('bottom');
				this.tooltip.classList.add('top');
			}
			const tt_width = tt_rect.width;
			const x_center = target_rect.left + target_rect.width / 2 + window.scrollX;
			x_offset += x_center - tt_width / 2;
		}
		this.tooltip.style.transform = `translate(${x_offset.toFixed(3)}px, ${y_offset.toFixed(3)}px)`;
	}
	
	static nextId = 0;
}
